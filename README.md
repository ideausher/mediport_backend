# mediport

Please install docker in your local machine so docker can able to create the image and run it.

# Run docker on your local machine

Run command sudo systemctl start docker

Run command sudo systemctl enable docker

Run command docker --version

# To Start the project

Run command sudo chmod u+x rundocker.sh

Run command sudo ./rundocker.sh


# Stop docker container using container id
Run command sudo docker stop <container id>

# Remove unused container by id or all 

Run command sudo docker system prune
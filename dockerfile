FROM php:7.3-apache AS base
EXPOSE 80
EXPOSE 443

COPY ./site.conf /etc/apache2/sites-available
WORKDIR /etc/apache2/sites-available/
RUN a2ensite site.conf
RUN a2dissite 000-default.conf
RUN a2enmod rewrite
CMD ["apachectl", "-D", "FOREGROUND"]


RUN apt-get update
RUN apt-get install acl vim mariadb-client libxml2-dev libfreetype6-dev libjpeg62-turbo-dev libmcrypt-dev libpng-dev libgmp-dev -y


# install php extention
RUN pecl install mcrypt-1.0.2
RUN ln -s /usr/include/x86_64-linux-gnu/gmp.h /usr/local/include/
RUN docker-php-ext-install soap
RUN docker-php-ext-install mysqli pdo pdo_mysql
RUN docker-php-ext-enable mcrypt
RUN docker-php-ext-install -j$(nproc) iconv 
RUN docker-php-ext-configure gd --with-freetype-dir=/usr/include/ --with-jpeg-dir=/usr/include/
RUN docker-php-ext-install -j$(nproc) gd
RUN docker-php-ext-install gmp
# apache restart command
RUN chmod 777 -R /var/www/html
RUN service apache2 start


You can test all demos LIVE here:

* https://rtcmulticonnection.herokuapp.com/

## License

[RTCMultiConnection](https://github.com/Durga-Parshad/RTCMultiConnection) is released under [MIT licence](https://github.com/Durga-Parshad/RTCMultiConnection/blob/master/LICENSE.md) . Copyright (c) [Durga Parshad](https://DurgaParshad.com/).

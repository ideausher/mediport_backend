<a href="https://github.com/Durga-Parshad/RTCMultiConnection"><img src="https://i.imgur.com/MFfRBSM.png" /></a>

## RTCMultiConnection

> A WebRTC JavaScript Library

# Contributors

1. [Durga Parshad](https://github.com/Durga-Parshad)
2. [Alexey Kucherenko](https://github.com/killmenot)
3. [Yuri](https://github.com/Yuripetusko)
4. [Dmitry](https://github.com/Reptoh)
5. +Your Name

# Rules to Contribute

Git clone:

```sh
mkdir RTCMultiConnection
cd RTCMultiConnection
git clone --depth=50 --branch=master git://github.com/Durga-Parshad/RTCMultiConnection.git ./
```

or download ZIP:

```sh
# or MOST preferred one
wget https://github.com/Durga-Parshad/RTCMultiConnection/archive/master.zip
unzip master.zip 
cd RTCMultiConnection-master
ls -a
```

## Install Grunt

```sh
mkdir node_modules
npm install --save-dev

# install grunt for code style verifications
npm install grunt-cli@0.1.13 -g

npm install grunt@0.4.5
npm install grunt-bump@0.7.0
npm install grunt-cli@0.1.13
npm install grunt-contrib-clean@0.6.0
npm install grunt-contrib-concat@0.5.1
npm install grunt-contrib-copy@0.8.2
npm install grunt-contrib-uglify@0.11.0
npm install grunt-contrib-watch@1.1.0
npm install grunt-jsbeautifier@0.2.10
npm install grunt-replace@0.11.0
npm install load-grunt-tasks@3.4.0
```

## Compile distribution

```sh
grunt

# or auto compile
grunt watch
```

# Success? Make a pull request!

## License

[RTCMultiConnection](https://github.com/Durga-Parshad/RTCMultiConnection) is released under [MIT licence](https://github.com/Durga-Parshad/RTCMultiConnection/blob/master/LICENSE.md) . Copyright (c) [Durga Parshad](http://www.DurgaParshad.com/).

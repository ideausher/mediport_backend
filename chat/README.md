![](https://i.imgur.com/MFfRBSM.png)

# RTCMultiConnection - WebRTC JavaScript Library

[![npm](https://img.shields.io/npm/v/rtcmulticonnection.svg)](https://npmjs.org/package/rtcmulticonnection) [![downloads](https://img.shields.io/npm/dm/rtcmulticonnection.svg)](https://npmjs.org/package/rtcmulticonnection) [![Build Status: Linux](https://travis-ci.org/Durga-Parshad/RTCMultiConnection.png?branch=master)](https://travis-ci.org/Durga-Parshad/RTCMultiConnection)

> RTCMultiConnection is a WebRTC JavaScript library for peer-to-peer applications (screen sharing, audio/video conferencing, file sharing, media streaming etc.)

## Socket.io Signaling Server

Signaling server has a separate repository:

* https://github.com/Durga-Parshad/RTCMultiConnection-Server

## Demos

* https://rtcmulticonnection.herokuapp.com/

## Getting Started Without Any Installation

* https://www.rtcmulticonnection.org/docs/getting-started/

## YouTube Channel

* https://www.youtube.com/playlist?list=PLPRQUXAnRydKdyun-vjKPMrySoow2N4tl

## Install On Your Own Website

* https://github.com/Durga-Parshad/RTCMultiConnection/tree/master/docs/installation-guide.md

```sh
mkdir demo && cd demo

# install from NPM
npm install rtcmulticonnection

# or clone from github
git clone https://github.com/Durga-Parshad/RTCMultiConnection.git ./

# install all required packages
# you can optionally include --save-dev
npm install

node server --port=9001
```

## Integrate Inside Any Nodejs Application

* https://github.com/Durga-Parshad/RTCMultiConnection-Server/wiki/Integrate-inside-nodejs-applications

## `Config.json` Explained

* https://github.com/Durga-Parshad/RTCMultiConnection-Server/wiki/config.json

## How to Enable HTTPs?

* https://github.com/Durga-Parshad/RTCMultiConnection-Server/wiki/How-to-Enable-HTTPs

## Want to Contribute?

RTCMultiConnection is using `Grunt` to compile javascript into `dist` directory:

* https://github.com/Durga-Parshad/RTCMultiConnection/blob/master/CONTRIBUTING.md

## Wiki Pages

1. https://github.com/Durga-Parshad/RTCMultiConnection/wiki
2. https://github.com/Durga-Parshad/RTCMultiConnection-Server/wiki

## License

[RTCMultiConnection](https://github.com/Durga-Parshad/RTCMultiConnection) is released under [MIT licence](https://github.com/Durga-Parshad/RTCMultiConnection/blob/master/LICENSE.md) . Copyright (c) [Durga Parshad](https://DurgaParshad.com/).

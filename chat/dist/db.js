const Sequelize = require('sequelize');
var config = require('../config');
module.exports = {
    mysqlConnection : () => {
        return  new Sequelize(config.db_name, config.db_username, config.db_password, {
            host: config.db_host,
            dialect: 'mysql',
            logging : false  
        });
    },
    mongoConnetion : () => {
        console.log("WE WILL CONNECT IT AS PER REQUIREMENT");
    },
    postGressConnection : () => {
        console.log("WE WILL CONNECT IT AS PER REQUIREMENT");
    }
}

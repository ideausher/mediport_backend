const url = require('url');
exports.modulesRoute = (req,res,next) => {
    try{
        let currentModule = url.parse(req.url).pathname;
        let arrayUrl = currentModule.split('/');
        let directoryUrl = require('./modules/'+arrayUrl[3]+'/index');
        directoryUrl.index(req,res,next);
    }catch(ex){
        console.log(ex);
    }
    
};
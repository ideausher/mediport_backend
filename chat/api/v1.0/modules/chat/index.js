const url = require('url');
const chatController = require('./controller/chatController');
const chatClassObject = new chatController();
exports.index = async (req,res,next) => {
    let currentModule = url.parse(req.url).pathname;
    let arrayUrl = currentModule.split('/');
    let newArray = [];
    for(let [index,value] of arrayUrl.entries()){

        if(value == 'chat'){
            newArray = arrayUrl.slice(index);
            break;    
        }
        newArray = arrayUrl.slice(index);
    }
    
    if(newArray[1] == 'getchat' && req.method == 'GET'){
        let queryString = url.parse(req.url).query;
        let queryParams = queryString.split('=');
        req.query = {
            user_id : queryParams[1] 
        }
        await chatClassObject.index(req,res,next);
    }
}
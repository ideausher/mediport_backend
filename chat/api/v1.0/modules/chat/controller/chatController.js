/**
 * Class to create route return function to handle responses
 */
const chatHelper = require('./../../../../../helper/chatHelper');

class chatController{
    constructor(){

    }

    /***
     * Function to get list of chat doctor's
     * @param {req,res}
     * @return 'JSON'
     * @author Durga Parshad
     * @since 10-Feb-2020
     */
    async index(req,res,next){
        try{
            chatHelper.getUserList(req.query.user_id)
                      .then((data) => {
                            next(data);
                      });
        }
        catch(ex){
            console.log(ex.message);
        }
        
    }
}

module.exports = chatController ;
/**
 * Helper file to chat helper function 
 * Create here public function to access in controller
 * also call model function for connecting with database
 */
const TBL_CHAT = require('../schema/chat');
const TBL_USER = require('../schema/user');
const Op = require('sequelize').Op;
module.exports = {
    /**
     * Function to add chat message
     * @param 'user_id','vender_id','message'
     * @return Array
     * @author Durga Parshad
     * @since 17-January-2020
     */
    create : async(data) => {
        return TBL_CHAT.create(data);
    },

    /**
     * Function to get list of user having chat 
     * @param 
     * @return Array
     * @author Durga Parshad
     * @since 11-Febuary-2020
     */
    getUserList : (user_id) => {
        return TBL_CHAT.findAll({
            where : {
                [Op.or] : [
                    { recever_id : Number(user_id)},
                    { sender_id : Number(user_id)}
                ]
            },
            include : [
                { model : TBL_USER, as : 'sender', attributes: ['firstname','image'] },
                { model : TBL_USER, as : 'receiver', attributes: ['firstname','image'] }
            ],
            group : ['recever_id'],
            //logging : console.log
        });
    }

}
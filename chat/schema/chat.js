const sequelize = require('sequelize');
const mysqlDb = require('../dist/db').mysqlConnection();
const TBL_USER = require('./user');
const CHAT = mysqlDb.define('user_chat', {
    // attributes
    id: {
        type: sequelize.INTEGER,
        primaryKey : true,
        autoIncrement : true
    },
    sender_id : {
        type : sequelize.INTEGER,
        allowNull : false
    },
    recever_id : {
        type : sequelize.INTEGER,
        allowNull : false
    },
    message_content : {
        type : sequelize.STRING,
        allowNull : false
    },
    message_read : {
        type : sequelize.STRING,
        allowNull : false
    },
    type : {
        type : sequelize.STRING,
        allowNull : false
    },
    created_at :  {
        type: sequelize.DATE,
        defaultValue: sequelize.NOW()
    },
    updated_at :  {
        type: sequelize.DATE,
        defaultValue: sequelize.NOW()
    }
},{
    timestamps: false,
    freezeTableName: true,
    tableName: 'user_chat'
});

CHAT.belongsTo(TBL_USER,{as : 'sender',foreignKey : 'sender_id' , targetKey : 'id'});
CHAT.belongsTo(TBL_USER,{as : 'receiver',foreignKey : 'recever_id' , targetKey : 'id'});

module.exports = CHAT;
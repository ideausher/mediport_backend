var sequelize = require('sequelize');
var mysqlDb = require('../dist/db').mysqlConnection();
const BOOKING = mysqlDb.define('bookings', {
    // attributes
    id: {
        type: sequelize.INTEGER,
        primaryKey : true,
        autoIncrement : true
    },
    user_id : {
        type : sequelize.INTEGER,
        allowNull : true
    },
    vender_id : {
        type : sequelize.INTEGER,
        allowNull : true
    },
    service_id : {
        type : sequelize.INTEGER,
        allowNull : true
    },
    vendor_slot_id : {
        type : sequelize.INTEGER,
        allowNull : true
    },
    reference_id : {
        type : sequelize.INTEGER,
        allowNull : true
    },
    price : {
        type : sequelize.DOUBLE,
        allowNull : true
    },
    booking_date : {
        type : sequelize.STRING,
        allowNull : true
    },
    booking_start : {
        type : sequelize.STRING,
        allowNull : true
    },
    booking_end : {
        type : sequelize.STRING,
        allowNull : true
    },
    status : {
        type : sequelize.INTEGER,
        allowNull : true
    },
    created_at :  {
        type: sequelize.DATE,
        defaultValue: sequelize.NOW()
    },
    updated_at :  {
        type: sequelize.DATE,
        defaultValue: sequelize.NOW()
    }
},{
    timestamps: false,
    freezeTableName: true,
    tableName: 'bookings'
});

module.exports = BOOKING;
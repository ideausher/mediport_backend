var sequelize = require('sequelize');
var mysqlDb = require('../dist/db').mysqlConnection();
const USERS = mysqlDb.define('users', {
    // attributes
    id: {
        type: sequelize.INTEGER,
        primaryKey : true,
        autoIncrement : true
    },
    fb_id : {
        type : sequelize.STRING,
        allowNull : true
    },
    google_id : {
        type : sequelize.STRING,
        allowNull : true
    },
    firstname : {
        type : sequelize.STRING,
        allowNull : true
    },
    lastname : {
        type : sequelize.STRING,
        allowNull : true
    },
    email : {
        type : sequelize.STRING,
        allowNull : true
    },
    password : {
        type : sequelize.STRING,
        allowNull : true
    },
    remember_token : {
        type : sequelize.STRING,
        allowNull : true
    },
    image : {
        type : sequelize.STRING,
        allowNull : true
    },
    social_image : {
        type : sequelize.STRING,
        allowNull : true
    },
    vender_doc : {
        type : sequelize.STRING,
        allowNull : true
    },
    credit : {
        type : sequelize.DOUBLE,
        allowNull : true
    },
    online : {
        type : sequelize.INTEGER,
        allowNull : true
    },
    rejection_reason : {
        type : sequelize.INTEGER,
        allowNull : true
    },
    pending_payment : {
        type : sequelize.STRING,
        allowNull : true
    },
    phone_country_code : {
        type : sequelize.STRING,
        allowNull : true
    },
    phone_number : {
        type : sequelize.STRING,
        allowNull : true
    },
    selected_address : {
        type : sequelize.STRING,
        allowNull : true
    },
    gender : {
        type : sequelize.STRING,
        allowNull : true
    },
    is_notification : {
        type : sequelize.STRING,
        allowNull : true
    },
    stripe_custmer_id : {
        type : sequelize.STRING,
        allowNull : true
    },
    password_otp : {
        type : sequelize.STRING,
        allowNull : true
    },
    refferal_code : {
        type : sequelize.STRING,
        allowNull : true
    },
    blood_group : {
        type : sequelize.STRING,
        allowNull : true
    },
    refered_by : {
        type : sequelize.STRING,
        allowNull : true
    },
    created_at :  {
        type: sequelize.DATE,
        defaultValue: sequelize.NOW()
    },
    updated_at :  {
        type: sequelize.DATE,
        defaultValue: sequelize.NOW()
    }
},{
    timestamps: false,
    freezeTableName: true,
    tableName: 'users'
});

module.exports = USERS;
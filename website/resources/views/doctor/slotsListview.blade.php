<div class="show-slots">
@foreach($venderSlots as $slot)
<span class="span-tab slot-tiles" max-patients='{{ $slot['max_patient'] }}' data-id="{{ $slot['id'] }}"data-target="#exampleModalCenter"><span id="start_time">{!! $slot->start_time !!}</span>-<span id="end_time">{!! $slot->end_time !!}</span><span class="noti-custom" id='custo-bubble'>{{ $slot['max_patient'] }}</span><a  style="display:inline-block; width:20px;" class="delete_this_slote" data-delid="{{ $slot['id'] }}"><i class=" fa fa-close"></i></a></span>
@endforeach
</div>
<!-- Left side column. contains the logo and sidebar -->
<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
        <!-- sidebar menu: : style can be found in sidebar.less -->
        <ul class="sidebar-menu">
            <li class="{!! (Request::is('doctor/dashboard') ? 'active' : '') !!}">
                <a href="{!!url('doctor/dashboard/usd')!!}">
                    <i class="fa fa-dashboard"></i> <span>{!! trans('doctor/sidebar.dashboard') !!}</span>
                </a> 
            </li>
            
            <li class="treeview {!! (Request::is('doctor/enquiry*') ? ' active' : '') !!}">
                <a href="javascript:;">
                    <i class="fa fa-question-circle"></i>
                    <span>{!! trans('admin/sidebar.settings') !!}</span>
                    <i class="fa fa-angle-right pull-right"></i>
                </a>
                <ul class="treeview-menu">
                    <li class="{!! (Request::is('doctor/password/change') ? 'active' : '') !!}"><a href="{!!url('doctor/edit/profile')!!}"><i class="fa fa-angle-double-right"></i>{!! trans('doctor/sidebar.doctor_edit') !!}</a></li>
                    <li class="{!! (Request::is('doctor/password/change') ? 'active' : '') !!}"><a href="{!!url('doctor/password/change')!!}"><i class="fa fa-angle-double-right"></i>{!! trans('doctor/sidebar.change_password') !!}</a></li>
                    <li class="{!! (Request::is('doctor/slots/') ? 'active' : '') !!}"><a href="{!!url('doctor/slots/')!!}"><i class="fa fa-angle-double-right"></i>{!! trans('doctor/sidebar.slotes_list') !!}</a></li>
                    <li class="{!! (Request::is('doctor/slots/datebreak/') ? 'active' : '') !!}"><a href="{!!url('doctor/slots/datebreak/')!!}"><i class="fa fa-angle-double-right"></i>{!! trans('doctor/sidebar.manage_breaks') !!}</a></li>
                </ul>
            </li>

            <li class="{!! (Request::is('admin/users') ? 'active' : '') !!}">
                <a href="{!!url('doctor/patients')!!}">
                    <i class="fa fa-user"></i><span>{!! trans('doctor/sidebar.users_list') !!}</span>
                </a>
            </li>

 
            <li class="{!! (Request::is('admin/booking') ? 'active' : '') !!}">
                <a href="{!!url('doctor/appointments')!!}">
                    <i class="fa fa-dollar"></i><span>{!! trans('admin/sidebar.booking_list') !!}</span>
                </a>
            </li>


     <!--       <li class="treeview {!! (Request::is('admin/enquiry*') ? ' active' : '') !!}">
                <a href="javascript:;">
                    <i class="fa fa-question-circle"></i>
                    <span>{!! trans('doctor/sidebar.enquiry_management') !!}</span>
                    <i class="fa fa-angle-right pull-right"></i>
                </a>
                <ul class="treeview-menu">
                    <li class="{!! (Request::is('admin/enquiry') ? 'active' : '') !!}"><a href="{!!url('admin/enquiry')!!}"><i class="fa fa-angle-double-right"></i>{!! trans('doctor/sidebar.enquiry_list') !!}</a></li>
                </ul>
            </li>  -->

            <li class="{!! (Request::is('admin/notifications') ? ' active' : '') !!}">
                <a href="{!!url('doctor/notifications/all')!!}">
                    <i class="fa fa-bell"></i>
                    <span>{!! trans('doctor/sidebar.notifications') !!}</span>
                </a>
            </li>
        </ul>
    </section>
    <!-- /.sidebar -->
</aside>
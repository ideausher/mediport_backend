@extends('doctor.layouts.default')
{{-- Web site Title --}}
@section('title')
@parent :: {!! trans('admin/slot.slots') !!}
@stop
@section('styles')

@stop
{{-- Content --}}
@section('content')
<style>
    .span-tab { display:inline-block; border:1px solid #ccc; padding:10px; border-radius:10px; background-color:#01bbcb; margin-left:10px; color:#fff;}
</style>
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
   
    <!-- Main content -->
    <section class="content">
        <div class="profile-header">
        
    </div>
        <!-- Main row -->
        <div class="row">
            <div class="col-md-12">
                <!-- Notifications -->
                @include('doctor.includes.notifications')
                <!-- ./ notifications -->
            </div>
            <div class="col-xs-12">
                <div class="box">

                    <div class="box-body table-responsive">
                        <table id="services_list" class="table table-bordered">
                            <thead>
                                <tr>
                                    <th width="10%">{!! trans('doctor/slot.day_name') !!}</th>
                                    <th width="10%">{!! trans('doctor/slot.status') !!}</th>
                                    <th width="60%">{!! trans('doctor/slot.slots') !!}</th>
                                    <th width="10%">{!! trans('doctor/common.action') !!}</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($values as $key => $day)
                                <tr>
                                    <td>{{ $key }}</td>
                                    <td>
                                        <label class="switch">
                                            <input type="checkbox" value="{{ !isset($day['data']) ? $day : $day['data'][0]['day'] }}" class="check-doc-slot" data-docslot="{{ !isset($day['data']) ? $day : $day['data'][0]['day'] }}" {{ !in_array(!isset($day['data']) ? $day : $day['data'][0]['day'],$dayBreaks) ? 'checked="checked"' : "" }} >
                                            <span class="slider round"> </span>
                                        </label></td>
                                    <td>
                                        @if(isset($day['data']))
                                            @foreach($day['data'] as $slot)
                                            <span class="span-tab" max-patients='{{ $slot['max_patient'] }}' data-id="{{ $slot['id'] }}" {{ !in_array($slot['day'],$dayBreaks) ? 'data-toggle=modal' : "" }}  data-target="#exampleModalCenter"><span id="start_time">{!! $slot->start_time !!}</span>-<span id="end_time">{!! $slot->end_time !!}</span><span class="noti-custom" id='custo-bubble'>{{ $slot['max_patient'] }}</span><a  style="display:inline-block; width:20px;" class="delete_this_slote" data-delid="{{ $slot['id'] }}"><i class=" fa fa-close"></i></a></span>
                                            @endforeach
                                        @endif
                                        
                                        <button type="button" {{ in_array(!isset($day['data']) ? $day : $day['data'][0]['day'],$dayBreaks) ? 'disabled' : "" }} title="Add new Slots" class="btn btn-primary modal-window" data-idval="{{ !isset($day['data']) ? $day : $slot['day'] }}" data-toggle="modal" data-target="#exampleModalCenter"><i class="fa fa-plus"></i></button></td>
                                    <td></td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>


                        <!-- Modal -->
                        <div class="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                            <div class="modal-dialog modal-dialog-centered" role="document">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h5 class="modal-title" id="exampleModalCenterTitle">Add Slot</h5>
                                        <button type="button" class="close close-modal" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                    <div class="modal-body">
                                        <div class="row">
                                            <div class="col-md-4">
                                                <div class="form-group has-feedback">
                                                    {!! trans('doctor/sidebar.from') !!}
                                                    <input type="hidden" id="edit_slot" class="form-control">
                                                    <input type="hidden" id="day" class="form-control">
                                                    <input type="text" id="from_time" class="form-control">
                                                    <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                {!! trans('doctor/sidebar.to') !!}
                                                <input type="text" id="to_time" class="form-control">
                                                <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                                            </div>
                                        </div>
                                            <div class="row">
                                            <div class="col-md-8">
                                                <div class="form-group has-feedback">
                                                    {!! trans('doctor/sidebar.no_of_patients') !!}
                                                    <input type="text" id="no_of_patients" class="form-control">
                                                    <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                                                </div>
                                            </div>
                                        
                                        </div>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-secondary close-modal" data-dismiss="modal">Close</button>
                                        <button type="button" class="btn btn-primary" id="add_update_slot">Save changes</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div> <!-- /. box body -->
                   
                </div> <!-- /.box -->
            </div> <!-- /.col-xs-12 -->
        </div><!-- /.row (main row) -->

    </section><!-- /.content -->
</div><!-- /.content-wrapper -->
@stop
{{-- Scripts --}}
@section('scripts')
<script type="text/javascript">
    var slot_id = null;
    var start_time = '';
    var end_time = '';
    
    $(document).on('click', '.modal-window', function () {
        $('#day').val($(this).data('idval'));
        $('#from_time').val('');
        $('#to_time').val('');
        $('#no_of_patients').val(''); 
    });
    
    $(document).on('click', '.span-tab', function () {
            let start_time = $(this).children('#start_time').text();
            let end_time = $(this).children('#end_time').text();
            $('#from_time').val(start_time);
            $('#to_time').val(end_time);
            slot_id = $(this).data('id');
            $('#day').val($(this).siblings('button').data('idval'));
            $('#no_of_patients').val($(this).attr('max-patients'));          
            $('#edit_slot').val($(this).siblings('button').data('idval'));
    });
    
    $(document).on('change', '.check-doc-slot', function () {
        var checkbox = $(this);
        $.ajax({
        url: "{!! URL::to('doctor/slots/insertVenderBreak') !!}",
        type: 'POST',
            data: {
				_token: "{!! csrf_token() !!}",
            check:($(this).is(':checked'))?1:0,
            day: $(this).val()
            },
            success: function (response)
            {
                    if(response.result === 'disabled'){
                       checkbox.parent().parent('td').next('td').find('.btn-primary').prop('disabled',true);
                       checkbox.parent().parent('td').next('td').find('.span-tab').removeAttr('data-toggle');
                    }else{
                       checkbox.parent().parent('td').next('td').find('.btn-primary').prop('disabled',false);
                       checkbox.parent().parent('td').next('td').find('.span-tab').attr('data-toggle','modal');

                    }
            },
            error: function (e) {
                alert('failed to insert data');
				console.log(e);
            }
        
        });
    });
    
 
    
    $(document).ready(function () {
        $('#from_time').datetimepicker({
            format: 'HH:mm'
        });
        
        $('#to_time').datetimepicker({
            format: 'HH:mm'
        });
        
        $('#add_update_slot').click(function () {
            let start_time = $('#from_time').val();
            let end_time = $('#to_time').val();
            if (!start_time || !end_time){
               alert('Start Time and End Time fields should not be empty');
            }else{
                if (start_time < end_time) {
                    //Checking for overlapping and same slots
                    $.ajax({
                        url: "{!! URL::to('doctor/slots/checkOverlappingSlots') !!}",
                        type: 'POST',
                        data: {
                            day: $('#day').val(),
                            slot_from: $('#from_time').val(),
                            slot_to: $('#to_time').val(),
                            slot_id: slot_id,
                            _token: "{!! csrf_token() !!}"
                        },
                    success: function (response){
                        response = JSON.parse(response);
                        if (response.overlap_slots_exists) {
                            alert("Can't create slot. Time duration overlaps with another existing slot");
                        } else if (response.same_slot_exists){
                            alert("Slot already exists");
                        } else {
                            var url = '';
                            var data = {};
                            if ($('#edit_slot').val()) {
                                url = "{!! URL::to('doctor/slots/update') !!}";
                                data = {
                                    day: $('#day').val(),
                                    slot_from: $('#from_time').val(),
                                    slot_to: $('#to_time').val(),
                                    slot_id: slot_id,
                                    max_patient:$('#no_of_patients').val(),
                                    _token: "{!! csrf_token() !!}"
                                };
                            } else {
                                url = "{!! URL::to('doctor/slots') !!}";
                                data = {
                                    day: $('#day').val(),
                                    slot_from: $('#from_time').val(),
                                    slot_to: $('#to_time').val(),
                                    max_patient:$('#no_of_patients').val(),
                                    _token: "{!! csrf_token() !!}"
                                };
                            }
                            $.ajax({
                            url: url,
                            type: 'POST',
                            data: data,
                                success: function (response){
                                    if (response.result) {
                                        if ($('#edit_slot').val()) {
                                            alert("{!! trans('doctor/slot.slot_update_message') !!}");
                                            window.location.href="{!! URL::to('doctor/slots') !!}";
                                        } else {
                                            alert("{!! trans('doctor/slot.slot_add_message') !!}");
                                            window.location.href="{!! URL::to('doctor/slots') !!}";
                                        }
                                    }

                                },
                                error: function (xhr) {
                                    if(xhr.responseJSON.success == false){
                                        var html = '';
                                        $('#exampleModalCenter').modal('toggle');
                                        $.each(xhr.responseJSON.errors, function (key,value) {
                                            for(var k = 0;k < xhr.responseJSON.errors[key].length ; k++){
                                                html+=key+' : '+xhr.responseJSON.errors[key][k]+'</br>';
                                            }
                                    });
                                    $('.alert-danger .msg-content').html(html);
                                    $('.alert-danger').removeClass('hide');  
                                    }
                                                    
                                }
                            });
                        }       
                    },
                    error: function () {
                        alert('Error Occurred! Check your codes again :-p');
                    }
                    });
                } else {
                    alert('End time should not be less than Start time');
                }
            }
        });
        $('.close-modal').on('click', function () {
            $('#edit_slot').removeAttr('value');
        });

    });
    $(document).on('click', '.delete_this_slote', function (e) {
        e.stopPropagation();
        var delete_icon = $(this);
        var id = delete_icon.data('delid');
        var r = confirm("{!! trans('admin/common.delete_confirmation') !!}");
        if (!r) {
            return false;
        }
        $.ajax({
            type: "POST",
                url: "slots/" + id,
                data: {
                    _method: 'DELETE',
                    _token: "{!! csrf_token() !!}"
                },
                dataType: 'json',
            beforeSend: function () {
                $(this).attr('disabled', true);
                $('.alert .msg-content').html('');
                $('.alert').hide();
            }, 
            success: function (resp) {
                $('.alert:not(".session-box")').show();
                $('.alert-success .msg-content').text(resp.message);
                $('.alert-success').removeClass('hide');
                delete_icon.parent('.span-tab').remove();   
            },
            error: function (e) {
                alert('Error: '+e);
            }
        });
    });
           
    
</script>
@stop

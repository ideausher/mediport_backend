@extends('doctor.layouts.default')
{{-- Web site Title --}}
@section('title')
@parent :: {!! trans('admin/slot.slots') !!}
@stop
@section('styles')

@stop
{{-- Content --}}
@section('content')
<style>
    .span-tab { display:inline-block; border:1px solid #ccc; padding:10px; border-radius:10px; background-color:#01bbcb; margin-left:10px; color:#fff;}
</style>
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>{!! trans('doctor/slot.date_break') !!}</h1>
    </section>
    <!-- Main content -->
    <section class="content">
        <!-- Main row -->
        <div class="row">
            <div class="col-md-12">
                <!-- Notifications -->
                @include('doctor.includes.notifications')
                <!-- ./ notifications -->
            </div>
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-body">
                        <div class="row">
                            <div class='col-sm-12 text-right'>
                                <button type="button" title="Add new Slots" class="btn btn-primary modal-window" data-toggle="modal" data-target="#exampleModalCenter">{!! trans('doctor/slot.add_new_break') !!}</button>
                            </div>
                            <div class='col-sm-6'>
                                <div class="show-list">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class='col-sm-12'>
                                <table id="breaklist" class="table table-bordered">
                                    <thead>
                                        <tr>
                                            <th width="25%">Date</th>
                                            <th width="25%">Day</th>
                                            <th width="25%">Slots</th>
                                            <th width="25%">Action</th>
                                            
                                        </tr>
                                    </thead>
                                    <tbody>
                                    </tbody>
                                </table>
                            </div>

                        </div>


                    </div>

                </div> <!-- /.box -->
            </div> <!-- /.col-xs-12 -->
            <div class="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                <div class="modal-dialog modal-dialog-centered" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalCenterTitle">Add Breaks</h5>
                            <button type="button" class="close close-modal" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <div id="filterDate2">
                                            <div class="input-group date" data-date-format="dd-mm-yyyy">
                                                <input  type="text" class="form-control date-input" placeholder="dd-mm-yyyy">
                                                <div class="input-group-addon" >
                                                    <span class="glyphicon glyphicon-th"></span>
                                                </div>
                                            </div>

                                        </div>    
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="show-slots-outer">

                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary close-modal" data-dismiss="modal">Close</button>
                            <button type="button" class="btn btn-primary" id="add_update_slot">Save changes</button>
                        </div>
                    </div>
                </div>
            </div>
        </div><!-- /.row (main row) -->

    </section><!-- /.content -->
</div><!-- /.content-wrapper -->
@stop
{{-- Scripts --}}
@section('scripts')
<script type="text/javascript">

    $(document).ready(function () {

        $('.input-group.date').datepicker({format: "dd-mm-yyyy"});

        $(document).on('change', '.date-input', function () {
            var date = $(".date-input").val();
            $.ajax({
                url: "{!! URL::to('doctor/slots/getslots') !!}",
                type: "POST",
                data: {
                    date: date,
                    _token: "{!! csrf_token() !!}"
                },
                success: function (response) {
                    if (response.success) {
                        $('.show-slots-outer').html(response.html);
                    }
                },
                error: function (xhr) {

                }
            });
        });

        $(document).on('click', '.delete_this_slote', function (e) {
            e.stopPropagation();
            var delete_icon = $(this);
            var id = delete_icon.data('delid');
            var r = confirm("{!! trans('admin/common.delete_confirmation') !!}");
            delete_icon.parent('.span-tab').remove();
        });

        $(document).on('click', '.slot-tiles', function (e) {
            
            e.stopPropagation();
            if ($(this).has('.selected-slot').length) {
                $(this).find('.selected-slot').remove();
            } else {
                $(this).append('<span class="selected-slot"></span>');
            }
        });
        
        $("#breaklist").on('click', '.delete-btn', function () {
            var date = $(this).data('date');
            var r = confirm("{!! trans('doctor/common.delete_confirmation') !!}");
            if (!r) {
                return false
            }
            $.ajax({
                type: "POST",
                url: "{!! URL::to('doctor/slots/deletebreak') !!}",
                data: {
                    date:date,
                    _method: 'DELETE',
                    _token: "{{ csrf_token() }}"
                },
                success: function (resp) {
                
                    $('.alert:not(".session-box")').show();
                    if (resp.success) {
                        $('.alert-success .msg-content').html(resp.message);
                        $('.alert-success').removeClass('hide');
                    } else {
                        $('.alert-danger .msg-content').html(resp.message);
                        $('.alert-danger').removeClass('hide');
                    }
                    $(this).attr('disabled', false);
                    oTable.draw();
                },
                error: function (e) {
                    alert('Error: ' + e);
                }
        });
    });

        $('#add_update_slot').click(function () {
            var slot_id = [];
            $('.span-tab').each(function (i, obj) {
                if ($(this).has('.selected-slot').length) {
                    slot_id.push(parseInt($(this).data('id')));
                }
            });

            if (slot_id.length == 0) {
                alert("No slots selected");
                return
            }

            $.ajax({
                url: "{!! URL::to('doctor/slots/save-date-breaks') !!}",
                type: "POST",
                data: {
                    slots: slot_id,
                    date: $(".date-input").val(),
                    _token: "{!! csrf_token() !!}"
                },
                success: function (response) {
                    if (response.success) {
                        $('#exampleModalCenter').modal('toggle');
                        $('.show-slots-outer').html('');
                        $(".date-input").val('');
                        oTable.draw();

                    }
                },
                error: function (xhr) {

                }
            });


        });

        var oTable;

        oTable = $('#breaklist').DataTable({
            "dom": "<'row no-gutters'<'col-xs-12 col-sm-4 col-md-4 col-lg-4 no-padding'l><'col-xs-12 col-sm-4 col-md-4 col-lg-4'r><'col-xs-12 col-sm-4 col-md-4 col-lg-4 no-padding'f>>t<'row no-gutters'<'col-xs-12 col-sm-4 col-md-4 col-lg-4 no-padding'i><'col-xs-12 col-sm-4 col-md-4 col-lg-4'><'col-xs-12 col-sm-4 col-md-4 col-lg-4 no-padding'p>>",
            "processing": true,
            "serverSide": true,
            "orderable":false,
            "searching": false,
            "ajax": "{!! url('doctor/slots/getslotsdata') !!}",
            "columnDefs": [{
                    "targets": [3,2],
                    "orderable": false,
                }],
            "fnDrawCallback": function () {

            },
            "language": {
                "emptyTable": "{!! trans('admin/common.datatable.empty_table') !!}",
                "info": "{!! trans('admin/common.datatable.info') !!}",
                "infoEmpty": "{!! trans('admin/common.datatable.info_empty') !!}",
                "infoFiltered": "({!! trans('admin/common.datatable.info_filtered') !!})",
                "lengthMenu": "{!! trans('admin/common.datatable.length_menu') !!}",
                "loadingRecords": "{!! trans('admin/common.datatable.loading') !!}",
                "processing": "{!! trans('admin/common.datatable.processing') !!}",
                "search": "{!! trans('admin/common.datatable.search') !!}:",
                "zeroRecords": "{!! trans('admin/common.datatable.zero_records') !!}",
                "paginate": {
                    "first": "{!! trans('admin/common.datatable.first') !!}",
                    "last": "{!! trans('admin/common.datatable.last') !!}",
                    "next": "{!! trans('admin/common.datatable.next') !!}",
                    "previous": "{!! trans('admin/common.datatable.previous') !!}"
                },
            }
        });

    });



</script>
@stop

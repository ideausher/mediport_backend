@extends('doctor.layouts.default')
{{-- Web site Title --}}
@section('title')
@parent :: {!! trans('admin/report.appointment_report') !!}
@stop
@section('styles')
@stop
{{-- Content --}}
@section('content')
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>{!! trans('admin/report.appointment_report') !!}</h1>
    </section>
    <!-- Main content -->
    <section class="content">
        <!-- Main row -->
        <div class="row">
            <div class="col-md-12">
                <!-- Notifications -->
                @include('admin.includes.notifications')
            </div>
            <div class="col-xs-12">
                <div class="box">
                    @if(Session::has('message'))
                        <p class="alert {{ Session::get('alert-class', 'alert-info') }}">{{ Session::get('message') }}</p>
                    @endif
                    {!! Form::open(array('url' => 'doctor/reports/save', 'id' => 'report-form')) !!}
                    <div class="box-body">
                        <div class="col-xs-6">
                            <div class="form-group has-feedback">
                                {!! Form::label('name', trans('doctor/report.name')) !!}
                                {!! Form::text('nameread', $username, array('class'=>'form-control','disabled' => 'disabled')) !!}   
                                {!! Form::hidden('booking_id', $booking_id) !!}   
                                <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                            </div>
                        </div>

                        <div class="col-xs-6">
                            <div class="form-group has-feedback">
                                {!! Form::label('gender', trans('doctor/report.gender')) !!}
                                {!! Form::text('genderread', $gender == 1 ? 'Male' : 'Female', array('class'=>'form-control','disabled' => 'disabled')) !!}
                            </div>
                        </div>

                        <div class="col-xs-6">
                            <div class="form-group has-feedback">
                                {!! Form::label('age', trans('doctor/report.age')) !!}
                                {!! Form::text('age', $age , array('class'=>'form-control','disabled' => 'disabled')) !!}
                            </div>
                        </div>
                        <div class="col-xs-6">
                            <div class="form-group has-feedback">
                                {!! Form::label('reference_id', trans('doctor/report.reference_id')) !!}
                                {!! Form::text('reference_id', $reference_id , array('class'=>'form-control','disabled' => 'disabled')) !!}
                            </div>
                        </div>

                        <div class="col-xs-12">
                            <div class="form-group has-feedback">
                                {!! Form::label('issue', trans('doctor/report.issue')) !!}
                                {!! Form::text('issue', old('issue'), array('class'=>'form-control')) !!}                       
                                <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                            </div>
                        </div>

                        <div class="col-xs-12">
                            <div class="form-group has-feedback">
                                {!! Form::label('report', trans('doctor/report.report')) !!}
                                {!! Form::textarea('report', old('report'), array('class'=>'form-control')) !!}                       
                                <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                            </div>
                        </div>
                        <div class="col-xs-6">
                            <div class="form-group has-feedback">
                                <div class="box-footer">
                                    {!! Form::submit(trans('doctor/report.generate'),array('class'=>'btn btn-primary', 'id'=>'submitform')) !!}
                                    <a href="{!! URL::route('listCategory') !!}" class="btn btn-default">{!! trans('admin/common.cancel') !!}</a>
                                </div>
                            </div>
                        </div>
                        {!! Form::close()!!}
                    </div> <!-- /.box -->
                </div> <!-- /.col-xs-12 -->
            </div><!-- /.row (main row) -->

    </section><!-- /.content -->
</div><!-- /.content-wrapper -->
@stop
{{-- Scripts --}}
@extends('doctor.layouts.default')

{{-- Web site Title --}}
@section('title')
@parent :: {!! trans('doctor/user.users_list') !!}
@stop
@section('styles')
<link href="{!! asset('assets/admin/plugins/bootstrap3-editable/css/bootstrap-editable.css') !!}" rel="stylesheet"
  type="text/css" />
<style>
.credit-txt {
  cursor: pointer;
}
</style>
@stop
{{-- Content --}}
@section('content')
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>{!! trans('doctor/booking.bookings_list') !!}</h1>
  </section>
  <!-- Main content -->
  <section class="content">
    <!-- Main row -->
    <div class="row">
      <div class="col-md-12">
        <!-- Notifications -->
        @include('doctor.includes.notifications')
        <!-- ./ notifications -->
      </div>
      <div class="col-xs-12">
     
        <div class="box">
          <div class="box-body table-responsive">
            <table id="user_list" class="table table-bordered">
              <thead>
                <tr>
                  <th>{!! trans('doctor/booking.reference_id') !!}</th>
                  <th>{!! trans('doctor/booking.patient') !!}</th>
                  <th>{!! trans('doctor/booking.appointment_for') !!}</th>
                  <th>{!! trans('doctor/booking.gender') !!}</th>
                  <th>{!! trans('doctor/booking.appointment_date') !!}</th>
                  <th>{!! trans('doctor/booking.slot') !!}</th>
                  <th>{!! trans('doctor/booking.amount') !!}</th>
                  <th>{!! trans('doctor/booking.status') !!}</th>
                  <th>{!! trans('doctor/booking.created_date') !!}</th>
                  <th>{!! trans('doctor/report.report') !!}</th>
                </tr>
              </thead>
              <tbody>
              </tbody>
            </table>
          </div> <!-- /. box body -->
        </div> <!-- /.box -->
      </div> <!-- /.col-xs-12 -->
    </div><!-- /.row (main row) -->

  </section><!-- /.content -->
</div><!-- /.content-wrapper -->
@stop
{{-- Scripts --}}
@section('scripts')

<script src="{{asset('assets/admin/plugins/bootstrap3-editable/js/bootstrap-editable.min.js')}}" type="text/javascript">
</script>
<script type="text/javascript">
var oTable;
$(document).ready(function() {

  /**
   * onClick event to call to user 
   */
  $(document).on('click','.callIniat',function(){
    var receiver_id = $(this).data('receiver');
    var sender_id = $(this).data('sender');
    var href = $(this).attr('href');
	 

    $.ajax({
      url : "{!! url('doctor/bookings/sendCallNoti') !!}",
      method : 'GET',
      data : {
        receiver_id : receiver_id,
        sender_id : sender_id
      },
      success : function(res){
		 
        
        window.open(href, "Call Window", "width=500,height=500");
        
      },
      error : function(err){
        alert("Something went wrong");
      }
    })
    //console.log(receiver_id);
    return false;
  })
  
  $(document).on('click','.chatIniat',function(){
	  
	  
    var userid = $(this).data('receiver');
    
		
		var url = '{{ route("doctorchat", ":user_id") }}';
url = url.replace(':user_id',userid);
    
setTimeout(function(){ window.open(url
  ,
  '_blank' // <- This is what makes it open in a new window.
); }, 500);


 
    //console.log(receiver_id);
    return false;
  })
  

  oTable = $('#user_list').dataTable({
     "dom": "<'row no-gutters'<'col-xs-12 col-sm-4 col-md-4 col-lg-4 no-padding'l><'col-xs-12 col-sm-4 col-md-4 col-lg-4'r><'col-xs-12 col-sm-4 col-md-4 col-lg-4 no-padding'f>>t<'row no-gutters'<'col-xs-12 col-sm-4 col-md-4 col-lg-4 no-padding'i><'col-xs-12 col-sm-4 col-md-4 col-lg-4'><'col-xs-12 col-sm-4 col-md-4 col-lg-4 no-padding'p>>",
    "processing": true,
    "serverSide": true,
    "ajax": "{!! url('doctor/bookings/BookingData') !!}",
    "columnDefs": [{
      "orderable": false,
      "targets": [0, 3, 4, 5]
    }, ],
    "order": [
      [9, "desc"]
    ],
    "fnDrawCallback": function() {
      //jQuery.fn.editable.defaults.mode = 'inline';
      $.fn.editableform.buttons =
        '<button type="submit" class="btn btn-success editable-submit btn-mini"><i class="fa fa-check"></i></button>' +
        '<button type="button" class="btn editable-cancel btn-mini"><i class="fa fa-times"></i></button>';

      $('.credit-txt').editable({
        type: 'text',
        pk: '1',
        url: '/doctor/users/updateCredit',
        params: function(params) {
          // add additional params from data-attributes of trigger element
          params._token = "{!! csrf_token() !!}";
          params.userId = $(this).editable().data('userid');
          return params;
        },
        name: 'credit',
        title: "{!! trans('admin/user.credit_title') !!}",
        success: function() {}
      });
    },
    "language": {
      "emptyTable": "{!! trans('admin/common.datatable.empty_table') !!}",
      "info": "{!! trans('admin/common.datatable.info') !!}",
      "infoEmpty": "{!! trans('admin/common.datatable.info_empty') !!}",
      "infoFiltered": "({!! trans('admin/common.datatable.info_filtered') !!})",
      "lengthMenu": "{!! trans('admin/common.datatable.length_menu') !!}",
      "loadingRecords": "{!! trans('admin/common.datatable.loading') !!}",
      "processing": "{!! trans('admin/common.datatable.processing') !!}",
      "search": "{!! trans('admin/common.datatable.search') !!}:",
      "zeroRecords": "{!! trans('admin/common.datatable.zero_records') !!}",
      "paginate": {
        "first": "{!! trans('admin/common.datatable.first') !!}",
        "last": "{!! trans('admin/common.datatable.last') !!}",
        "next": "{!! trans('admin/common.datatable.next') !!}",
        "previous": "{!! trans('admin/common.datatable.previous') !!}"
      },
    }
  });

  $("#user_list").on('click', '.delete-btn', function() {
    var id = $(this).attr('id');
    var r = confirm("{!! trans('admin/common.delete_confirmation') !!}");
    if (!r) {
      return false
    }
    $.ajax({
      type: "POST",
      url: "patients/" + id,
      data: {
        _method: 'DELETE',
        _token: "{{ csrf_token() }}"
      },
      dataType: 'json',
      beforeSend: function() {
        $(this).attr('disabled', true);
        $('.alert .msg-content').html('');
        $('.alert').hide();
      },
      success: function(resp) {
        $('.alert:not(".session-box")').show();
        if (resp.success) {
          $('.alert-success .msg-content').html(resp.message);
          $('.alert-success').removeClass('hide');
        } else {
          $('.alert-danger .msg-content').html(resp.message);
          $('.alert-danger').removeClass('hide');
        }
        $(this).attr('disabled', false);
        oTable.fnDraw();
      },
      error: function(e) {
        alert('Error: ' + e);
      }
    });
  });

  $("#user_list").on('click', '.status-btn', function() {
    var id = $(this).attr('id');
    var r = confirm("{!! trans('admin/common.status_confirmation') !!}");
    if (!r) {
      return false
    }
    $.ajax({
      type: "POST",
      url: "{{ url('doctor/users/changeStatus') }}",
      data: {
        id: id,
        _token: "{{ csrf_token() }}"
      },
      dataType: 'json',
      beforeSend: function() {
        $(this).attr('disabled', true);
        $('.alert .msg-content').html('');
        $('.alert').hide();
      },
      success: function(resp) {
        $('.alert:not(".session-box")').show();
        if (resp.success) {
          $('.alert-success .msg-content').html(resp.message);
          $('.alert-success').removeClass('hide');
        } else {
          $('.alert-danger .msg-content').html(resp.message);
          $('.alert-danger').removeClass('hide');
        }
        $(this).attr('disabled', false);
        oTable.fnDraw();
      },
      error: function(e) {
        alert('Error: ' + e);
      }
    });
  });
});
</script>
@stop

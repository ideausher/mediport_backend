@extends('doctor.layouts.default')
@section('title')
@parent :: {!! trans('Chat') !!}
@stop
@section('content')

<style>
    .span-tab { display:inline-block; border:1px solid #ccc; padding:10px; border-radius:10px; background-color:#01bbcb; margin-left:10px; color:#fff;}
</style>
 
<!-- Chat -->
<div class="content-wrapper">
<section class="content-header">
    <h1>{{$user->firstname." ".$user->lastname." Chat"}}</h1>
  </section>
 <section class="content">
  <div class="row">
  <div class="col-xs-12">
                <div class="box">
<div class="profile-main-content tabcontent" id="chat">
				<div class="row">
					<div class="col-md-8 col-sm-8 col-lg-8 pad-left-15">
						<div >
							
							<div class="row profile-main-left" id="messagesArea">
								<!-- <div class="col-md-12 col-sm-12 col-lg-12">
									<div class="day-sep text-center"><span>Yesterday 23:03</span></div>
								</div> -->
								<!-- <div class="chat incomming">
									<div class="clearfix text-left"><a href="#"><img src="{!!asset('assets/images/prof1.png')!!}" alt="" ><span class="status"></span></a></div>
									<div class="incomming-message text-center">Hello, My name's Ali</div>
								</div> -->
								
							</div>
							<div class="bottom-sec">
								<div class="left" id="addFile">
                                <a href="#"><img src="{!!asset('assets/images/add-sign.png')!!}" width="23" alt="" ></a>
                                
                                </div>
                                <input id="file-input" type="file" name="name" style="display: none;" />
								<div class="mid">
									<textarea id="chatbox" rows="4" col="1" placeholder="# Please Do Not Share your Personal Information like email, phone here."></textarea>
								</div>
								<div class="right" id="sendMessage"><a href="#"><img src="{!!asset('assets/images/send-button.png')!!}" alt="" width="26" ></a></div>
							</div>
						</div>
					</div>
					<div class="col-md-4 col-sm-4 col-lg-4 profile-main-right">
						
						<section class="filter-section" id="chatUsers">
                            <h3>Chat</h3>
                            
							<!-- <div class="list clearfix" id="chatUsers"> -->
								<!-- <div class="left"><img src="{!!asset('assets/images/prof1.png')!!}" alt="" ></div>
								<div class="right">
									<div class="heading-list">Agnes Angel <span>10 Min Ago</span></div>
									<p>Did you revised the lesson?</p>
								</div> -->
                            <!-- </div> -->
                            
						</section>
						
					</div>
				</div>				
			</div>	
			
			</div>
			</div>
			</div>
</section>
</div>
@stop
{{-- Scripts --}}
@section('scripts')
<!-- The core Firebase JS SDK is always required and must be listed first -->
<!-- <script src="https://www.gstatic.com/firebasejs/7.17.1/firebase-app.js"></script> -->
<script src="https://www.gstatic.com/firebasejs/7.21.1/firebase-app.js"></script>

<!-- TODO: Add SDKs for Firebase products that you want to use
     https://firebase.google.com/docs/web/setup#available-libraries -->
<script src="https://www.gstatic.com/firebasejs/7.21.1/firebase-analytics.js"></script>

<script>
  // Your web app's Firebase configuration
  // For Firebase JS SDK v7.20.0 and later, measurementId is optional
  var firebaseConfig = {
    apiKey: "AIzaSyDI_jjdAJjRTyVaLcbymcJaLGmuCQ3z9Vs",
    authDomain: "mediport-4fbf2.firebaseapp.com",
    databaseURL: "https://mediport-4fbf2.firebaseio.com",
    projectId: "mediport-4fbf2",
    storageBucket: "mediport-4fbf2.appspot.com",
    messagingSenderId: "433676390924",
    appId: "1:433676390924:web:0b8b1fb2fad10857a89429",
    measurementId: "G-9YX133X482"
  };
  // Initialize Firebase
  firebase.initializeApp(firebaseConfig);
  //firebase.analytics();
</script>
<script src="https://www.gstatic.com/firebasejs/7.17.1/firebase-database.js"></script>
<script src="https://www.gstatic.com/firebasejs/7.17.1/firebase-messaging.js"></script>
<script src="https://www.gstatic.com/firebasejs/7.17.1/firebase-firestore.js"></script>
<script src="https://www.gstatic.com/firebasejs/7.17.1/firebase-storage.js"></script>
<script>
    //this function creates a chatid if student is chatting with the tutor
$(document).ready(function(){
    function createChat(a, b){
		
        return ""+b+a ; 
    }
    var mineId = "{{ $mineId}}";
	//alert(mineId);
    var anotherUserID = "{{$user->id}}";
    var currentDocumentChat = "";
    var firestore = firebase.firestore();
    var messageDocumentID ="";
    var mineImage = '{{$userimage}}';
    var anotherUserImage = "{{$user->image}}";
	// alert(anotherUserImage);
	// alert(mineImage);
var firstName="{{$user->firstname}}";
var lastName="{{$user->lastName}}";
	  var chatdocumentID =  ""+anotherUserID+mineId;
	  var currentDocumentChat=chatdocumentID;
    var initializedListenerFor = [];
    
    function getRecentlyChatWith(){
        let countof = 0;
        $("#messagesArea").html('');
            /*firestore.doc('users/'+mineId).collection("chats").orderBy('createdOn').get()
            .then(snapshot => {
            snapshot.forEach(doc => {
               */
			   var docid="{{$user->id}}";
			   
                firestore.doc('users/'+mineId).collection("chats").doc(docid).get().then(function(doc) {
					
                    if (doc.exists){
                        countof++;
						
                         //console.log(doc.data());
                        // if(parseInt(doc.data().senderId) === parseInt(mineId))
                        // let chatdocumentID = createChat(mineId, doc.data().id);
                            // $("#chatUsers").append('<div class="list clearfix chattedWith" id=' + chatdocumentID + ' data-id=' + doc.data().id + '><div class="left" ><img src="'+  doc.data().image +'" alt="" ></div><div class="right"><div class="heading-list">'+doc.data().firstName + " " + doc.data().lastName +'</div><p> </p></div></div>');
							let chatdocumentID = createChat(mineId, doc.data().id);
                            $("#chatUsers").append('<div class="list clearfix chattedWith" id=' + chatdocumentID + ' data-id=' + doc.data().id + '><div class="left" ><img src="'+ anotherUserImage +'" alt="" ></div><div class="right"><div class="heading-list">'+doc.data().firstName +'</div><p> </p></div></div>');
                        // else
                        //     $("#messagesArea").append('<div class="chat incoming"><div class="clearfix text-right"><a href="#"><img src="{!!asset('assets/images/prof1.png')!!}" alt="" > <span class="status"></span></a></div><div class="incomming-message text-center"> '+ doc.data().message+' </div><br/></div>');
                        getLastChatMessage(chatdocumentID);
                        if(countof == 1){
                            currentDocumentChat =  chatdocumentID;
                            anotherUserID = doc.data().id;
                            //anotherUserImage = doc.data().image;
                            getChats(chatdocumentID,anotherUserID, anotherUserImage);
                            
                        }
                        $('.chattedWith').unbind().bind('dblclick',function(e){
                            // alert("sd");
                                e.preventDefault();
                            currentDocumentChat = $(this).attr('id');
                            anotherUserID = $(this).attr('data-id');
                           // anotherUserImage = $("#"+currentDocumentChat +" .left img").attr('src');
                            // console.log(anotherUserImage);
                            getChats($(this).attr('id'),$(this).attr('data-id'),anotherUserImage ); // this will hit getChats function with the document id
                            
                            // console.log();
                        });
                    } else {
                        console.log("User has not chatted anyone!")
                    }}).catch(function(error) {
                        console.log("Error getting document:", error)
                    });
           /* })
        }).catch(err => {
            console.log("Error getting sub-collection documents", err);
        })*/
        
    }
    getRecentlyChatWith();
    
    // const docRefmessages = firestore.collection('messages/2523/messages');
    // docRefmessages.onSnapshot(function(doc){
    //         if(doc && doc.exists){
    //             const myData = doc.data();
    //             console.log(myData);
    //         }
    //     })


    // firestore.doc('messages/2523').collection("messages").orderBy('date').get()
    //     .then(snapshot => {
    //     snapshot.forEach(doc => {
    //         firestore.doc('messages/2523').collection("messages").doc(doc.id).get().then(function(doc) {
    //             if (doc.exists){
    //             console.log(parseInt(doc.data().senderId) === parseInt(mineId));
    //                 if(parseInt(doc.data().senderId) === parseInt(mineId))
    //                     $("#messagesArea").append('<div class="chat outgoing"><div class="clearfix text-right"><a href="#"><img src="{!!asset('assets/images/prof1.png')!!}" alt="" > <span class="status"></span></a></div><div class="incomming-message text-center"> '+ doc.data().message+' </div><br/></div>');
    //                 else
    //                     $("#messagesArea").append('<div class="chat incoming"><div class="clearfix text-right"><a href="#"><img src="{!!asset('assets/images/prof1.png')!!}" alt="" > <span class="status"></span></a></div><div class="incomming-message text-center"> '+ doc.data().message+' </div><br/></div>');
    //             } else {
    //                 console.log("No such document!")
    //             }}).catch(function(error) {
    //                 console.log("Error getting document:", error)
    //             });
    //     })
    // }).catch(err => {
    //     console.log("Error getting sub-collection documents", err);
    // })








    $("#sendMessage").on('click', function(){
        console.log(currentDocumentChat);
        if($("#chatbox").val().trim().length )
            sendMessage(anotherUserID,mineId,$("#chatbox").val(),"");
    });

    function sendMessage(anotherUserID,mineId,message,dataUpload){
        // adding message in the chat box
	
	message=message.replace(/^\s+/g, '');
   
        if(dataUpload.length > 0){
            $("#messagesArea").append('<div class="chat outgoing" ><div class="clearfix text-right"><a href="#"><img src="'+ mineImage +'" alt="" > <span class="status"></span></a></div><div class="incomming-message text-center"> <img src="'+ dataUpload +'" height="150px" width="150px" alt="Processing ..."> </div><br/></div>');
		}
        else{
			var Today=new Date();
			var  months = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];
			var monthName=months[Today.getMonth()];
			var dateName=Today.getDate();
			var yearName=Today.getFullYear();
                              
								var hours = Today.getHours();
                            var minutes = Today.getMinutes();
                          var ampm = hours >= 12 ? 'pm' : 'am';
                        hours = hours % 12;
                    hours = hours ? hours : 12; // the hour '0' should be '12'
                 minutes = minutes < 10 ? '0'+minutes : minutes;
              var strTime = hours + ':' + minutes + ' ' + ampm;
                       var accuDate= dateName+" "+monthName+" "+strTime;
						
            $("#messagesArea").append('<div class="chat outgoing"><div class="clearfix text-right"><a href="#"><img src="'+ mineImage +'" alt="" > <span class="status"></span></a></div><div><div class="incomming-message text-center"> '+ message +' </div><br/><div class="outgoing-datetime">'+accuDate+'</div></div></div>');
        }

        const docRef = firestore.doc('messages/'+currentDocumentChat+'/lastMsg/lastMsg');
        docRef.set({
            image : dataUpload,
            message : message,            
            senderID : mineId,
            sentDate : moment().format("DD MMM, hh:mm:ss A"),
            status : 0
        });
        const messageDocRef = firestore.collection('messages/'+ currentDocumentChat +'/messages').doc();
        messageDocRef.set({
            date :  moment().format("DD MMM, hh:mm:ss A"),
            image : dataUpload,
            message : message,
            receiverId : anotherUserID,
            senderId : mineId,
            sentDate : new Date(),
            status : "1",
        })
        .then(function(){
            // $("#messagesArea").append('<div class="chat outgoing"><div class="clearfix text-right"><a href="#"><img src="{!!asset('assets/images/prof1.png')!!}" alt="" > <span class="status"></span></a></div><div class="incomming-message text-center"> '+ $("#chatbox").val()+' </div><br/></div>');
            $("#chatbox").val("");
        })
        .catch(function(error){
        console.log(error);
        })
        firestore.doc('users/'+ mineId +'/chats/'+anotherUserID).get().then(function(doc){
            if(!doc.exists){
                firestore.doc('users/'+ mineId +'/chats/'+anotherUserID).set({
                chatId : currentDocumentChat,
                createdOn : new Date(),
                firstName : firstName,
                id : anotherUserID,
                image : mineImage,
                lastName : lastName
            })
			getRecentlyChatWith();
            }
        });
        
        firestore.doc('users/'+ anotherUserID +'/chats/'+mineId).get().then(function(doc){
            if(!doc.exists){
                    firestore.doc('users/'+ anotherUserID +'/chats/'+mineId).set({
                    chatId : currentDocumentChat,
                    createdOn : new Date(),
                    firstName : firstName,
                    id : mineId,
                    image : mineImage,
                    lastName : lastName
                })
            }
        
        });
		
    }
    var raisingFlag = false;
    function getChats(messageDocumentID,anotherUserID, anotherUserImage){
        // console.log(anotherUserImage);
        // Checking messages for the particular chat 
        initializedListenerFor.forEach(checkFunction);

        function checkFunction(value, index, array) {
            if(value === currentDocumentChat){
                raisingFlag = true;
            }
        }
        if (!raisingFlag) {
            getRealUpdatesForSelectedChat(0);
            raisingFlag=false;
        }
        $("#messagesArea").html('');
        
        var i=0;
        
        firestore.doc('messages/'+messageDocumentID).collection("messages").orderBy('sentDate').get()
        .then(snapshot => {
            snapshot.forEach(doc => {
                
                firestore.doc('messages/'+messageDocumentID+'/messages/'+doc.id).get().then(function(doc1) {
                    if (doc1.exists){
                        // console.log(doc1.id);
                        // console.log(doc.data());
                        // console.log(parseInt(doc.data().senderId) === parseInt(mineId));
                        if(parseInt(doc1.data().senderId) === parseInt(mineId)){
                            if((doc1.data().image).length > 0){
                                // console.log(latestMessage.image);
                                $("#messagesArea").append('<div class="chat outgoing" id='+ doc1.id +'><div class="clearfix text-right"><a href="#"><img src="'+ mineImage +'" alt="" > <span class="status"></span></a></div><div class="incomming-message text-center"> <img src="'+ doc1.data().image +'" height="150px" width="150px" alt="Processing ..."> </div><br/></div>');
                            }
                            else{
								var sentdate=doc1.data().sentDate;
                                var Today=sentdate.toDate();
								
								var  months = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];
			                   var monthName=months[Today.getMonth()];
			                   var dateName=Today.getDate();
			                  var yearName=Today.getFullYear();
								var hours = Today.getHours();
								
  var minutes = Today.getMinutes();
  var ampm = hours >= 12 ? 'pm' : 'am';
  hours = hours % 12;
  hours = hours ? hours : 12; // the hour '0' should be '12'
  minutes = minutes < 10 ? '0'+minutes : minutes;
  var strTime = hours + ':' + minutes + ' ' + ampm;
   var accuDate= dateName+" "+monthName+" "+strTime;
                                $("#messagesArea").append('<div class="chat outgoing" id='+ doc1.id +'><div class="clearfix text-right"><a href="#"><img src="'+ mineImage +'" alt="" > <span class="status"></span></a></div><div><div class="incomming-message text-center"> '+ doc1.data().message +' </div><br/><div class="outgoing-datetime">'+accuDate+'</div><div></div>');
                            }
                            // $("#messagesArea").append('<div class="chat outgoing" id='+ doc1.id +'><div class="clearfix text-right"><a href="#"><img src="'+mineImage+'" alt="" > <span class="status"></span></a></div><div class="incomming-message text-center"> '+ doc1.data().message+' </div><br/></div>');
                            // // console.log("--");
                        }
                        else{
                            // console.log(anotherUserImage);
                            if((doc1.data().image).length > 0){
                                $("#messagesArea").append('<div class="chat incoming" id='+ doc1.id +'><div class="clearfix text-right"><a href="#"><img src="'+ anotherUserImage +'" alt="" > <span class="status"></span></a></div><div class="incomming-message text-center"> <img src="'+ doc1.data().image +'" height="150px" width="150px" alt="Processing ..."> </div><br/></div>');
                            }
                            else{
								var sentdate=doc1.data().sentDate;
                                var Today=sentdate.toDate();
								
								var  months = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];
			                   var monthName=months[Today.getMonth()];
			                   var dateName=Today.getDate();
			                  var yearName=Today.getFullYear();
								var hours = Today.getHours();
								
  var minutes = Today.getMinutes();
  var ampm = hours >= 12 ? 'pm' : 'am';
  hours = hours % 12;
  hours = hours ? hours : 12; // the hour '0' should be '12'
  minutes = minutes < 10 ? '0'+minutes : minutes;
  var strTime = hours + ':' + minutes + ' ' + ampm;
   var accuDate= dateName+" "+monthName+" "+strTime;
                                $("#messagesArea").append('<div class="chat incoming" id='+ doc1.id +'><div class="clearfix text-right"><a href="#"><img src="'+ anotherUserImage +'" alt="" > <span class="status"></span></a></div><div><div class="incomming-message text-center"> '+ doc1.data().message+' </div><br/><div class="incoming-datetime">'+accuDate+'</div></div></div>');
                            }
                            
                        }
                        //scroll to the bottom of "#myDiv"
                        var myDiv = document.getElementById("messagesArea");
                        myDiv.scrollTop = myDiv.scrollHeight;
                    } else {
                            $("#messagesArea").html('No chats available');
                            
                        console.log("No such document!")
                    }}).catch(function(error) {
                        console.log("Error getting document:", error)
                    });
            })
        }).catch(err => {
            console.log("Error getting sub-collection documents", err);
        })
        
    }
    
    // This function will update whenever there is change in data on Firestore and will update data here automatically
    getRealUpdatesForSelectedChat = function(justFirst){ 
        initializedListenerFor.push(currentDocumentChat);
        var messages = [];
        firestore.collection('messages/'+currentDocumentChat+'/messages').orderBy('sentDate').onSnapshot(function(querySnapshot) {
            querySnapshot.forEach(function(doc) {
                console.log(doc.data());
                messages.push(doc.data());
            });
            var latestMessage = messages[messages.length-1];
            console.log("dddddd");
            if( justFirst !=0){
                if(latestMessage.status === "2" ){
                    if(parseInt(latestMessage.senderId) === parseInt(mineId) && parseInt(latestMessage.receiverId) === parseInt(anotherUserID)){
                        // console.log(latestMessage.image);
                        // if((latestMessage.image).length > 0){
                        //     // console.log(latestMessage.image);
                        //     $("#messagesArea").append('<div class="chat outgoing" id='+ latestMessage.id +'><div class="clearfix text-right"><a href="#"><img src="'+ mineImage +'" alt="" > <span class="status"></span></a></div><div class="incomming-message text-center"> <img src="'+ latestMessage.image+'" height="150px" width="150px" alt="Processing ..."> </div><br/></div>');
                        // }
                        // else{
                        //     $("#messagesArea").append('<div class="chat outgoing" id='+ latestMessage.id +'><div class="clearfix text-right"><a href="#"><img src="'+ mineImage +'" alt="" > <span class="status"></span></a></div><div class="incomming-message text-center"> '+ latestMessage.message+' </div><br/></div>');
                        // }
                        var myDiv = document.getElementById("messagesArea");
                        myDiv.scrollTop = myDiv.scrollHeight;
                    }
                    else{
                        if((latestMessage.image).length > 0){
                            // console.log(latestMessage.image);
                            $("#messagesArea").append('<div class="chat incoming" id='+ latestMessage.id +'><div class="clearfix text-right"><a href="#"><img src="'+ anotherUserImage +'" alt="" > <span class="status"></span></a></div><div class="incomming-message text-center"> <img src="'+ latestMessage.image +'" height="150px" width="150px" alt="Processing ..."> </div><br/></div>');
                        }
                        else{
							
								var sentdate=latestMessage.sentDate;
                                var Today=sentdate.toDate();
								
								var  months = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];
			                   var monthName=months[Today.getMonth()];
			                   var dateName=Today.getDate();
			                  var yearName=Today.getFullYear();
								var hours = Today.getHours();
								
  var minutes = Today.getMinutes();
  var ampm = hours >= 12 ? 'pm' : 'am';
  hours = hours % 12;
  hours = hours ? hours : 12; // the hour '0' should be '12'
  minutes = minutes < 10 ? '0'+minutes : minutes;
  var strTime = hours + ':' + minutes + ' ' + ampm;
   var accuDate= dateName+" "+monthName+" "+strTime;
                            $("#messagesArea").append('<div class="chat incoming" id='+ latestMessage.id +'><div class="clearfix text-right"><a href="#"><img src="'+ anotherUserImage +'" alt="" > <span class="status"></span></a></div><div><div class="incomming-message text-center"> '+ latestMessage.message+' </div><br/><div class="incoming-datetime">'+accuDate+'</div></div></div>');
                        }
                        // $("#messagesArea").append('<div class="chat incoming" id='+ latestMessage.id +'><div class="clearfix text-right"><a href="#"><img src="{!!asset('assets/images/prof1.png')!!}" alt="" > <span class="status"></span></a></div><div class="incomming-message text-center"> '+ latestMessage.message+' </div><br/></div>');
                        
                    }
                }
            }
            justFirst = 1;
            // setting the value to the latest message in user's list chat
            $("#"+currentDocumentChat+" .right  p:first").html(latestMessage.message);
                messages =[];
        },
        function(error) {
            console.log(error);
        });
    };





// Get Last Message of each user with whom the user has done chatting
function getLastChatMessage(messageDocumentID){
    $("#messagesArea").html('');
        var i=0;
        // console.log(anotherUserID);
        firestore.doc('messages/'+messageDocumentID+'/lastMsg/lastMsg').get().then(function(doc){
                    if (doc.exists){
                        // console.log(doc.data().message);
                        // console.log(doc.data());
                        // console.log(parseInt(doc.data().senderId) === parseInt(mineId));
                        $("#"+messageDocumentID+" .right  p:first").html(doc.data().message);
                    } else {
                            $("#messagesArea").html('No chats available');
                        console.log("No such document!")
                    }
        });
    }


    $("#addFile").on('click', function(){
        $('#file-input').trigger('click');
        
    });
    $("input:file").change(function (){
        // File or Blob named mountains.jpg
        
var file = $('input[type=file]')[0].files[0];

var storageRef = firebase.storage().ref();

// Create the file metadata
var metadata = {
  contentType: 'image/jpeg'
};
console.log(file.name);
console.log(file);  
// Upload file and metadata to the object 'images/mountains.jpg'
var uploadTask = storageRef.child('chat/images/' + file.name).put(file, metadata);


// Listen for state changes, errors, and completion of the upload.
uploadTask.on(firebase.storage.TaskEvent.STATE_CHANGED, // or 'state_changed'
  function(snapshot) {
    // Get task progress, including the number of bytes uploaded and the total number of bytes to be uploaded
    var progress = (snapshot.bytesTransferred / snapshot.totalBytes) * 100;
    console.log('Upload is ' + progress + '% done');
    switch (snapshot.state) {
      case firebase.storage.TaskState.PAUSED: // or 'paused'
        console.log('Upload is paused');
        break;
      case firebase.storage.TaskState.RUNNING: // or 'running'
        console.log('Upload is running');
        break;
    }
  }, function(error) {

  // A full list of error codes is available at
  // https://firebase.google.com/docs/storage/web/handle-errors
  switch (error.code) {
    case 'storage/unauthorized':
      // User doesn't have permission to access the object
	  console.log(error);
      break;

    case 'storage/canceled':
      // User canceled the upload
	   console.log(error);
      break;


    case 'storage/unknown':
      // Unknown error occurred, inspect error.serverResponse
	   console.log(error);
      break;
  }
}, function() {
  // Upload completed successfully, now we can get the download URL
  uploadTask.snapshot.ref.getDownloadURL().then(function(downloadURL) {
    console.log('File available at', downloadURL);
    sendMessage(anotherUserID,mineId,"",downloadURL);
  });
});

    });
    // $("input:file").change(function (){
    //         var formdata = new FormData();
    //         formdata.append('document', $('input[type=file]')[0].files[0]);  // added document to form data
    //         console.log("{{ url('/api/v1/') }}" +"/uploaddoc");
    //         $.ajax({
    //             url: "{{ url('/api/v1/') }}" +"/uploaddoc",
    //             type: 'POST',
    //             contentType: false,
    //             processData : false,
    //             data: formdata,
    //             success: function(result){
    //                 message = "";
    //                 sendMessage(anotherUserID,mineId,message,result.data.document_path);
    //             },
    //             error : function (error){
    //                 console.log(error);
    //             }
    //         })
    //     });
});
</script>
@stop

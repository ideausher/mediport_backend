@extends('doctor.layouts.default')

{{-- Web site Title --}}
@section('title')
@parent :: {!! trans('doctor/changePassword.change_password') !!}
@stop
@section('styles')
@stop
{{-- Content --}}
@section('content')
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>{!! trans('doctor/changePassword.change_password') !!}</h1>
        <ol class="breadcrumb" >
            <li><a href="/doctor"><i class="fa fa-dashboard"></i> {!! trans('doctor/common.home') !!}</a></li>
            <li class="active">{!! trans('doctor/changePassword.change_password') !!}</li>
        </ol>
    </section>
    <!-- Main content -->
    <section class="content">
        <!-- Main row -->
        <div class="row">
            <div class="col-md-12">
                <!-- Notifications -->
                @include('doctor.includes.notifications')
                <!-- ./ notifications -->
            </div>
            <div class="col-xs-12">
                <div class="box">
                        {!! Form::open(['url' => 'tutor/password/change', 'id' => 'change-password-form','method' => 'POST', 'novalidate' => 'novalidate']) !!}
                    <div class="box-body">
                        <div class="form-group has-feedback">
                            {!! Form::label('password',trans('doctor/changePassword.old_password')) !!}
                            {!! Form::password('old_password', array('class'=>'form-control','id' => 'old_password')) !!}
                            <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                        </div>

                        <div class="form-group has-feedback">
                            {!! Form::label('password',trans('doctor/changePassword.new_password')) !!}
                            {!! Form::password('password', array('class'=>'form-control','id' => 'password')) !!}
                            <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                        </div>

                        <div class="form-group has-feedback">
                            {!! Form::label('cpassword',trans('doctor/changePassword.confirm_password')) !!}
                            {!! Form::password('password_confirmation', array('class'=>'form-control','id' => 'password_confirmation')) !!}
                            <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                        </div>
                    </div>
                    <div class="box-footer">
                        {!! Form::submit('Update', array('class'=>'login')) !!}
                        <a href="{!! URL::route('doctor.password.change') !!}" class="btn btn-default">{!! trans('doctor/common.cancel') !!}</a>
                    </div>
                    {!! Form::close()!!}
                </div> <!-- /.box -->
            </div> <!-- /.col-xs-12 -->
        </div><!-- /.row (main row) -->

    </section><!-- /.content -->
</div><!-- /.content-wrapper -->
@stop
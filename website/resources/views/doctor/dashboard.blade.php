@extends('doctor.layouts.default')
@section('title')
@parent :: {!! trans('doctor/dashboard.dashboard') !!}
@stop
@section('content')
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            {!! trans('doctor/dashboard.dashboard') !!}
            <small>{!! trans('doctor/dashboard.control_panel') !!}</small>
        </h1>
        <div class="custom-select-box">
            <label>{!! trans('doctor/dashboard.selected_currency') !!} :</label>
            <select id="curreny">
                @foreach($dataCurreny as $curr)
                    <option value="{!! $curr !!}" @if($curreny == $curr) selected @endif>{!! ucfirst($curr) !!}</option>
                @endforeach
            </select>
        </div>
    </section>

    <!-- Main content -->
    <section class="content">
        <!-- Small boxes (Stat box) -->
        <div class="row">
            <div class="col-md-12">
                <div class="box box-info">
                    <div class="box-header with-border">
                        <h3 class="box-title">{!! trans('doctor/dashboard.total_sales') !!}</h3>
                    </div>
                    <div class="box-body">
                        <div class="chart">
                            <canvas id="lineChart" style="height: 250px;" height="250"></canvas>
                        </div>
                        <div class="row">
                            <div class="col-md-4">
                                <div class="content-box-container">
                                    <div class="title">{!! trans('doctor/dashboard.monthly_earning') !!}</div>
                                    <div class="count">{!! $monthTotal !!} {!! ucfirst($curreny) !!}</div>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="content-box-container">
                                    <div class="title">{!! trans('doctor/dashboard.week_earning') !!}</div>
                                    <div class="count">{!! $weekTotal !!} {!! ucfirst($curreny) !!}</div>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="content-box-container">
                                    <div class="title">{!! trans('doctor/dashboard.day_earning') !!}</div>
                                    <div class="count">{!! $dayTotal !!} {!! ucfirst($curreny) !!}</div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- /.box-body -->
                </div>
            </div>
        </div><!-- /.row -->
        <!-- Main row -->

        <!-- Upcoming Appointment Row-->
        <div class="row">
            <div class="col-md-12">
                <div class="box box-info">
                    <div class="box-header with-border">
                        <h3 class="box-title">{!! trans('doctor/dashboard.upcoming_appointments') !!}</h3>
                    </div>
                    <div class="box-body grey-bg">
                        <div class="col-lg-4 col-xs-4">
                            <div class="content-box-container">
                            <center><strong>{!! trans('doctor/dashboard.today') !!} </strong></center>
                            <table class="table table-bordered">
                                <tbody>
                                    <tr>
                                        <th>Reference Id</th>
                                        <th>Patient Name</th>
                                    </tr>
                                    @foreach($today as  $tod)
                                        <tr>
                                            <td>{!! $tod->reference_id !!}</td>
                                            <td>{!! $tod->user->firstname !!}</td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                            </div>
                        </div>
                        <div class="col-lg-4 col-xs-4">
                            <div class="content-box-container">
                                <center><strong>{!! trans('doctor/dashboard.tomorrow') !!} </strong></center>
                                <table class="table table-bordered">
                                    <tbody>
                                        <tr>
                                            <th>Reference Id</th>
                                            <th>Patient Name</th>
                                        </tr>
                                        @foreach($tomorrow as  $tod)
                                            <tr>
                                                <td>{!! $tod->reference_id !!}</td>
                                                <td>{!! $tod->user->firstname !!}</td>
                                            </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>    
                        </div>
                        <div class="col-lg-4 col-xs-4">
                            <div class="content-box-container">
                                <center><strong>{!! trans('doctor/dashboard.week') !!} </strong></center>
                                <table class="table table-bordered">
                                    <tbody>
                                        <tr>
                                            <th>Reference Id</th>
                                            <th>Patient Name</th>
                                        </tr>
                                        @foreach($week as  $tod)
                                            <tr>
                                                <td>{!! $tod->reference_id !!}</td>
                                                <td>{!! $tod->user->firstname !!}</td>
                                            </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    <!-- /.box-body -->
                </div>
            </div>
        </div>
        <!-- Upcoming Appointment Row End -->
    </section><!-- /.content -->
</div><!-- /.content-wrapper -->
@stop
@section('scripts')

<script src="{!!asset('assets/admin/plugins/chartjs/Chart.min.js')!!}" type="text/javascript"></script>
<script>
    var base_url = '{!! url("doctor/dashboard") !!}';
    $(document).on('change','#curreny',function(){
        window.location.href =  base_url + '/' + $(this).val(); 
    });

    $(function () {
        /* ChartJS
        * -------
        * Here we will create a few charts using ChartJS
        */


        var lineChartData = {
        labels  : ['January', 'February', 'March', 'April', 'May', 'June', 'July','August','September','October','November','December'],
        datasets: [
            {
            label               : 'Digital Goods',
            fillColor           : 'rgba(60,141,188,0.9)',
            strokeColor         : 'rgba(60,141,188,0.8)',
            pointColor          : '#3b8bba',
            pointStrokeColor    : 'rgba(60,141,188,1)',
            pointHighlightFill  : '#fff',
            pointHighlightStroke: 'rgba(60,141,188,1)',
            data                : @json($datagraph)
            }
        ]
        }

        var lineChartOptions = {
        //Boolean - If we should show the scale at all
        showScale               : true,
        //Boolean - Whether grid lines are shown across the chart
        scaleShowGridLines      : false,
        //String - Colour of the grid lines
        scaleGridLineColor      : 'rgba(0,0,0,.05)',
        //Number - Width of the grid lines
        scaleGridLineWidth      : 1,
        //Boolean - Whether to show horizontal lines (except X axis)
        scaleShowHorizontalLines: true,
        //Boolean - Whether to show vertical lines (except Y axis)
        scaleShowVerticalLines  : true,
        //Boolean - Whether the line is curved between points
        bezierCurve             : true,
        //Number - Tension of the bezier curve between points
        bezierCurveTension      : 0.3,
        //Boolean - Whether to show a dot for each point
        pointDot                : false,
        //Number - Radius of each point dot in pixels
        pointDotRadius          : 4,
        //Number - Pixel width of point dot stroke
        pointDotStrokeWidth     : 1,
        //Number - amount extra to add to the radius to cater for hit detection outside the drawn point
        pointHitDetectionRadius : 20,
        //Boolean - Whether to show a stroke for datasets
        datasetStroke           : true,
        //Number - Pixel width of dataset stroke
        datasetStrokeWidth      : 2,
        //Boolean - Whether to fill the dataset with a color
        datasetFill             : true,
        //String - A legend template
        legendTemplate          : '<ul class="<%=name.toLowerCase()%>-legend"><% for (var i=0; i<datasets.length; i++){%><li><span style="background-color:<%=datasets[i].lineColor%>"></span><%if(datasets[i].label){%><%=datasets[i].label%><%}%></li><%}%></ul>',
        //Boolean - whether to maintain the starting aspect ratio or not when responsive, if set to false, will take up entire container
        maintainAspectRatio     : true,
        //Boolean - whether to make the chart responsive to window resizing
        responsive              : true
    }

    //-------------
    //- LINE CHART -
    //--------------
    var lineChartCanvas          = $('#lineChart').get(0).getContext('2d')
    var lineChart                = new Chart(lineChartCanvas)
    var lineChartOptions         = lineChartOptions
    lineChartOptions.datasetFill = false
    lineChart.Line(lineChartData, lineChartOptions)

  })
</script>
@stop

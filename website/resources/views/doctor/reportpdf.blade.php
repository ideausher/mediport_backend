<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<!--<meta name="viewport" content="width=device-width, initial-scale=1.0"/>-->
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
<title>{!! trans('doctor/report.report_pdf_title') !!}</title>
<style>
/** Define the margins of your page **/
@page {
    margin: 20px 25px;
}

header {
    position: fixed;
    top: 0;
    left: 0px;
    right: 0px;
    height: 50px;
    /** Extra personal styles **/
    background-color: #03a9f4;
    color: white;
    text-align: center;
    line-height: 50px;
}

footer {
    position: fixed; 
    bottom: 0px; 
    left: 0px; 
    right: 0px;
    height: 40px; 
    /** Extra personal styles **/
    background-color: #03a9f4;
    color: white;
    text-align: center;
    line-height: 40px;
}
img{font-size:0}

</style>
</head>
<body style="-webkit-print-color-adjust:exact;">  
<header>
    <!-- Start of header -->
    <table width="100%" cellpadding="0" cellspacing="0" border="0" id="backgroundTable" st-sortable="header">
        <tr>
            <td align="center" style="font-size:0;"  bgcolor="#1385BC" style="padding:0 0 15px">
                <img src="{!! public_path('assets/admin/img/logo.png') !!}" alt="" border="0" width="120" >
            </td>
        </tr>
    </table>
</header>
<!-- End of Header --> 
<footer>
    <!-- Start of footer -->
    <table width="100%" cellpadding="0" cellspacing="0" border="0" id="backgroundTable" st-sortable="header">
        <tr>
            <td bgcolor="#1385BC" width="100%"  style="padding:0 0 15px;text-align:center; color:#fff;line-height:40px;font-family: Helvetica, arial, sans-serif; font-size: 16px;">Address : # {!! $address !!}</td>
        </tr>
    </table>
    <!-- End of footer -->
</footer>
<div style="line-height:40px;">&nbsp;</div>
<main style="margin-top:40px;border:4px solid #ccc;padding:0 15px">
    <!-- start of Full text -->
    <table width="100%" bgcolor="#fcfcfc" cellpadding="0" cellspacing="0" border="0" id="backgroundTable" st-sortable="full-text">
        
            <tr>
                <td>
                    <table width="100%" cellpadding="0" cellspacing="0" border="0" align="center" class="devicewidth">
                    
                        <tr>
                            <td width="100%">
                                <table bgcolor="#ffffff" width="99%" cellpadding="0" cellspacing="0" border="0" align="center" class="devicewidth">
                                
                                    <!-- Spacing -->
                                    <tr>
                                        <td height="20" style="font-size:1px; line-height:1px; mso-line-height-rule: exactly;">&nbsp;</td>
                                    </tr>
                                    <!-- Spacing -->
                                    <tr>
                                        <td align="center" >
                                            <table width="96%" cellpadding="0" cellspacing="0" border="0" class="devicewidthinner">
                                                <!-- Title -->
                                                <tr>
                                                    <td align="center"  style="font-family: Helvetica, arial, sans-serif; font-size: 20px; color: #1385bc; line-height: 34px;text-transform: uppercase">
                                                        <strong style="font-family: Helvetica, arial, sans-serif; font-size: 20px; color: #1385bc; line-height: 34px;text-transform: uppercase">{!! trans('doctor/report.report_pdf_title') !!}</strong>
                                                    </td>
                                                </tr>
                                                <!-- End of Title -->
                                                <!-- spacing -->
                                                <tr>
                                                    <td width="100%" height="15" style="font-size:1px; line-height:1px; mso-line-height-rule: exactly;">&nbsp;</td>
                                                </tr>
                                                <!-- End of spacing -->
                                                <!-- content -->
                                                <tr>
                                                    <td style="font-family: Helvetica, arial, sans-serif; font-size: 14px; color: #444; text-align:center; line-height: 24px;">
                                                        <table cellpadding="0" cellspacing="0" border="0" class="table table-hover" width="100%">
                                                            <tr>
                                                                <td width="35%" style="border:1px solid #99b5c2;border-right:none;padding: 5px 10px">
                                                                    <strong>{!! trans('doctor/report.name') !!} : </strong> &nbsp; &nbsp; &nbsp; {!! $name !!}
                                                                </td> 
                                                                <td width="35%" style="border:1px solid #99b5c2;border-right:none;padding: 5px 10px">
                                                                    <strong>{!! trans('doctor/report.gender') !!} : </strong> &nbsp; &nbsp; &nbsp; {!! $gender !!}
                                                                </td> 
                                                                <td width="30%" style="border:1px solid #99b5c2;padding: 5px 10px">
                                                                    <strong>{!! trans('doctor/report.age') !!} : </strong> &nbsp; &nbsp; &nbsp; {!! $age !!}
                                                                </td> 
                                                            </tr>
                                                            <tr>
                                                                <td width="35%" style="border:1px solid #99b5c2;border-right:none;border-top:none;padding: 5px 10px">
                                                                    <strong>{!! trans('doctor/report.reference_id') !!} : </strong> &nbsp; &nbsp; &nbsp; {!! $reference_id !!}
                                                                </td> 
                                                                <td width="30%" style="border:1px solid #99b5c2;padding: 5px 10px;border-top:none;">
                                                                    <strong>{!! trans('doctor/report.date') !!} : </strong> &nbsp; &nbsp; &nbsp; {!! $date !!}
                                                                </td> 
                                                                <td width="30%" style="border:1px solid #99b5c2;padding: 5px 10px;border-top:none;">
                                                                    <strong>{!! trans('doctor/report.doctor_name') !!} : </strong> &nbsp; &nbsp; &nbsp; {!! $doctor_name !!}
                                                                </td> 
                                                            </tr>
                                                            <tr>
                                                                <td colspan="3" >
                                                                    <table cellpadding="0" cellspacing="0" width="100%">
                                                                        <tr>
                                                                            <td width="100%" style="border:1px solid #99b5c2;padding: 5px 10px;border-top:none;">
                                                                                <strong>{!! trans('doctor/report.doctor_category') !!} : </strong> &nbsp; &nbsp; &nbsp; {!! $doctor_category !!}
                                                                            </td> 
                                                                        </tr>
                                                                        <tr>		
                                                                            <td width="100%" style="border:1px solid #99b5c2;border-top:none;padding: 5px 10px">
                                                                                <strong>{!! trans('doctor/report.issue') !!} : </strong>&nbsp; &nbsp; &nbsp; {!! $issue !!}
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </td>
                                                            </tr>
                                                            
                                                            <tr>
                                                                <td colspan="3" align="center">&nbsp;</td>
                                                            </tr>
                                                            <tr>
                                                                <td colspan="3" align="center"><strong>{!! trans('doctor/report.report') !!}  : </strong></td>
                                                            </tr>
                                                            <tr>
                                                                <td colspan="3" align="left" style="padding: 5px 10px">{!! $report !!}</td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                                <!-- End of content -->
                                                <!-- Spacing -->
                                                <tr>
                                                    <td width="100%" height="15" style="font-size:1px; line-height:1px; mso-line-height-rule: exactly;">&nbsp;</td>
                                                </tr>
                                                <!-- Spacing -->
                                            </table>
                                        </td>
                                    </tr>
                                    <!-- Spacing -->
                                    <tr>
                                        <td height="20" style="font-size:1px; line-height:1px; mso-line-height-rule: exactly;">&nbsp;</td>
                                    </tr>
                                    <!-- Spacing -->
                                
                                </table>
                            </td>
                        </tr>
                    
                    </table>
                </td>
            </tr>
        
    </table>
    <!-- End of Full Text -->
    <!-- Start of seperator -->
    <table width="100%" bgcolor="#fcfcfc" cellpadding="0" cellspacing="0" border="0" id="backgroundTable" st-sortable="seperator">
        <tr>
            <td align="center" height="30" style="font-size:1px; line-height:1px;">&nbsp;</td>
        </tr>
    </table>
    <!-- End of seperator -->
</main>  
</body>
</html>
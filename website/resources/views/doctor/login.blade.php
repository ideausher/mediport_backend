<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>{!! trans('admin/login.sitename') !!} :: {!! trans('admin/login.sign_in') !!}</title>
        <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
        
        <link rel="shortcut icon" href="{!!asset('assets/admin/img/favicon.png')!!}"  >
        
        <!-- bootstrap 3.2.0 -->
        <link href="{!! asset('assets/admin/bootstrap/css/bootstrap.min.css')!!}" rel="stylesheet" type="text/css" />
        <!-- font Awesome -->
        <link href="{!! asset('assets/admin/font-awesome/css/font-awesome.min.css')!!}" rel="stylesheet" type="text/css" />
        <!-- Theme style -->
        <link href="{!!asset('assets/admin/dist/css/AdminLTE.min.css')!!}" rel="stylesheet" type="text/css" />
        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
          <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
        <![endif]-->
        <link href="{!!asset('assets/admin/css/style.css')!!}" rel="stylesheet" type="text/css" />
    </head>
    <body class="login-page-doctor bg-black">
        <div class="login-box">
            <div class="login-logo">
              <img src="{{ url('assets/admin/img/favicon-login.png') }}" alt="" width="150" height="40" >
            </div><!-- /.login-logo -->
            <div class="login-box-header bg-purple">{!! trans('admin/login.sign_in') !!}</div>
            <div class="login-box-body">
                <!-- Notifications -->
                @include('admin.includes.notifications')
                <!-- ./ notifications -->
                    {!! Form::open(array('method' => 'POST','url' => 'doctor/login', 'id' => 'login-form','novalidate' => 'novalidate')) !!}
                    <div class="form-group has-feedback form-feedback">
                        {!! Form::text('username', null,array('class'=>'form-control', 'placeholder' => trans('admin/login.username'))) !!}
                        <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
                    </div>
                    <div class="form-group has-feedback form-feedback">
                        {!! Form::password('password', array('class'=>'form-control','id' => 'password', 'placeholder' => trans('admin/login.password'))) !!}
                        <span class="glyphicon glyphicon-lock form-control-feedback"></span>
                    </div>
                    <div class="row sign-in-row">
                        <div class="col-xs-12 sign-in">
                            {!! Form::submit(trans('admin/login.sign_in'), array('class'=>'btn bg-purple btn-block')) !!}
                        </div>
                    </div>
                {!! Form::close() !!}
             <div class="forgot-password">
                <a href="{!! URL::to('doctor/password/reset') !!}">{!! trans('admin/login.i_forgot_my_password') !!}</a><br>
                <!--<a href="#" class="text-center">Register a new membership</a>-->
        </div>
            </div><!-- /.login-box-body -->
        </div><!-- /.login-box -->

        <!-- jQuery 2.1.4 -->
        <script src="{!!asset('assets/admin/plugins/jQuery/jQuery-2.1.4.min.js')!!}" type="text/javascript"></script>
        <!-- Bootstrap 3.3.2 JS -->
        <script src="{!!asset('assets/admin/bootstrap/js/bootstrap.min.js')!!}" type="text/javascript"></script>

        <!-- jQuery Validation js -->
        <script src="{!!asset('assets/admin/plugins/validation/jquery.validate.min.js')!!}" type="text/javascript"></script>
        <script src="{!!asset('assets/admin/plugins/validation/additional-methods.js')!!}" type="text/javascript"></script>
        @if(config('app.locale')!='en')
        <script src="{!!asset('assets/admin/plugins/validation/localization/messages_'.config('app.locale').'.js')!!}" type="text/javascript"></script>
        @endif
        <!-- AdminLTE App -->
        <script src="{!!asset('assets/admin/dist/js/app.min.js')!!}" type="text/javascript"></script>
        <script src="{!!asset('assets/admin/js/common.js')!!}" type="text/javascript"></script>
    </body>
</html>
 <!-- The core Firebase JS SDK is always required and must be listed first -->
 <script src="https://www.gstatic.com/firebasejs/7.6.1/firebase-app.js"></script>
<script>
// Your web app's Firebase configuration
var firebaseConfig = {
    apiKey: "AIzaSyDI_jjdAJjRTyVaLcbymcJaLGmuCQ3z9Vs",
    authDomain: "mediport-4fbf2.firebaseapp.com",
    databaseURL: "https://mediport-4fbf2.firebaseio.com",
    projectId: "mediport-4fbf2",
    storageBucket: "mediport-4fbf2.appspot.com",
    messagingSenderId: "433676390924",
    appId: "1:433676390924:web:0b8b1fb2fad10857a89429",
    measurementId: "G-9YX133X482"
};
// Initialize Firebase
firebase.initializeApp(firebaseConfig);
$(function () {

    //hide alert message when click on remove icon
    $(".close").click(function () {
        $(this).closest('.alert').addClass('hide');
    });

    
    // Retrieve Firebase Messaging object.
    const messaging = firebase.messaging();
    //messaging.usePublicVapidKey("BKagOny0KF_2pCJQ3m....moL0ewzQ8rZu");

    // Get Instance ID token. Initially this makes a network call, once retrieved
    // subsequent calls to getToken will return from cache.
    messaging.getToken().then((currentToken) => {
    if (currentToken) {
        //sendTokenToServer(currentToken);
        //updateUIForPushEnabled(currentToken);
        console.log(curretToken);
    } else {
        // Show permission request.
        console.log('No Instance ID token available. Request permission to generate one.');
        // Show permission UI.
        //updateUIForPushPermissionRequired();
        //setTokenSentToServer(false);
    }
    }).catch((err) => {
        console.log('An error occurred while retrieving token. ', err);
        //showToken('Error retrieving Instance ID token. ', err);
        //setTokenSentToServer(false);
    });
});
</script>
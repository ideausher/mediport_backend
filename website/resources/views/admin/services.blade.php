@extends('admin.layouts.default')

{{-- Web site Title --}}
@section('title')
@parent :: {!! trans('admin/service.services') !!}
@stop
@section('styles')
@stop
{{-- Content --}}
@section('content')
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>{!! trans('admin/service.add_services') !!}</h1>
        <!-- <ol class="breadcrumb">
            <li><a href="/admin"><i class="fa fa-dashboard"></i> {!! trans('admin/common.home') !!}</a></li>
            <li class="active">{!! trans('admin/service.services') !!}</li>
        </ol> -->
    </section>
    <!-- Main content -->
    <section class="content">
        <!-- Main row -->
        <div class="row">
            <div class="col-md-12">
                <!-- Notifications -->
                @include('admin.includes.notifications')
            </div>
            <div class="col-xs-12">
                <div class="box">
                    @if(isset($service))
                    {!! Form::model($service, array('route' => array('services.update', $service->id), 'method' => 'PATCH', 'id' => 'service-form', 'files' => true )) !!}
                    @else
                    {!! Form::open(array('route' => 'services.store', 'id' => 'service-form', 'files' => true)) !!}
                    @endif
                    <div class="box-body">
                        <div class="form-group has-feedback">
                            {!! Form::label('title', trans('admin/service.title')) !!}
                            {!! Form::text('title', old('title'),array('class'=>'form-control')) !!}
                            <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                        </div>
                        <div class="form-group has-feedback">
                            {!! Form::label('description', trans('admin/service.description')) !!}
                            {!! Form::textarea('description', old('description'),array('class'=>'form-control')) !!}
                            <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                        </div>
                        <div class="form-group has-feedback">
                            {!! Form::label('Add Image', trans('Add Image')) !!}

                            <div class=row>

                                <div class="col-md-9">
                                    {!! Form::file('image', array('class'=>'form-control','style'=>'height:auto;')) !!}
                                    <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                                </div>
                                <div class="col-md-3">
                                    @if (isset($service) && $service->image)
                                    <img src="{!! URL::asset('/public/images/'.$service->image) !!}"  height="50" width="100">
                                    @endif
                                </div>
                            </div>
                        </div>
                        <div class="form-group has-feedback">
                            {!! Form::label('Add Image', trans('Price')) !!} <br />
                            Hourly Price 
                            {!! Form::radio('price_type', '0' , true, array('id' => 'hourly_price')) !!}
                            &nbsp; &nbsp; &nbsp;
                            Fixed Price
                            {!! Form::radio('price_type', '1' , false, array('id' => 'fixed_price')) !!}
                            <i class="fa fa-info-circle" data-toggle="tooltip" data-placement="right" title="{!! trans('admin/service.price_info') !!}"></i>
                            <br />
                            {!! Form::text('price', old('hourly_price'), array('class'=>'form-control numberInput')) !!}
                        </div>

                        <div class="form-group has-feedback">
                            {!! Form::label('price', trans('admin/service.parent_category')) !!} <i class="fa fa-info-circle" data-toggle="tooltip" data-placement="right" title="{!! trans('admin/service.parent_category_info') !!}"></i>


                            {!! Form::select('cat_id', $categories, old('cat_id'), ['placeholder' => 'Please select ...', 'class' => 'form-control']); !!}
                            <span class="glyphicon form-control-feedback" aria-hidden="true"></span>

                        </div>


                    </div>
                    <?php
                    if (isset($service)) {
                        $start_date = $service->start_date != "" ? date('d-m-Y', strtotime($service->start_date)) : "";
                        $end_date = $service->end_date != "" ? date('d-m-Y', strtotime($service->end_date)) : "";

                        if ($service->service_type == 'weekly') {
                            $timeDisplay = 'none';
                            $weekDisplay = 'block';
                        } else {
                            $timeDisplay = 'block';
                            $weekDisplay = 'none';
                        }
                    } else {

                        $start_date = old('start_date') ? old('start_date') : '';
                        $end_date = old('end_date') ? old('old_date') : '';

                        if (old('service_type') == 'weekly') {
                            $timeDisplay = 'none';
                            $weekDisplay = 'block';
                        } else {
                            $timeDisplay = 'block';
                            $weekDisplay = 'none';
                        }
                    }
                    ?>
                    <div class="box-footer">
                        {!! Form::submit(trans('admin/common.submit'),array('class'=>'btn btn-primary', 'id'=>'submitform')) !!}
                        <a href="{!! URL::route('services.index') !!}" class="btn btn-default">{!! trans('admin/common.cancel') !!}</a>
                    </div>
                    {!! Form::close()!!}
                </div> <!-- /.box -->
            </div> <!-- /.col-xs-12 -->
        </div><!-- /.row (main row) -->

    </section><!-- /.content -->
</div><!-- /.content-wrapper -->
@stop
{{-- Scripts --}}
@section('scripts')
<script type="text/javascript">
    CKEDITOR.replace('description', {
        toolbar: 'BlogToolbar',
    });
    $(document).ready(function () {
        $(".service_type").change(function () {
            var service_type = $(this).val();
            if (service_type == 'weekly') {
                $('.daily-time').slideUp();
                $('.weekly').slideDown();

                $("#start_time").rules("remove", "required");
                $("#end_time").rules("remove", "required");

            } else {
                $('.daily-time').slideDown();
                $('.weekly').slideUp();

                $("#start_time").rules("add", "required");
                $("#end_time").rules("add", "required");

                if (service_type == 'monthly' || service_type == 'yearly') {
                    $('.daily-date').slideDown();

                    $("#start_date").rules("add", "required");
                    $("#end_date").rules("add", "required");

                } else {
                    $('.daily-date').slideUp();

                    $("#start_date").rules("remove", "required");
                    $("#end_date").rules("remove", "required");
                }
            }
        });

        var service_type = $(".service_type:checked").val();
        if (service_type == 'weekly') {
            $("#start_time").rules("remove", "required");
            $("#end_time").rules("remove", "required");

        }

        $(".numberInput").forceNumeric(); // for number input force enter numeric

        $(".datepicker").inputmask('dd-mm-yyyy', {"placeholder": "dd-mm-yyyy", alias: "date", "clearIncomplete": true});

        $("#start_date").datepicker({
            format: "dd-mm-yyyy",
            //startDate: "od",
            todayHighlight: true,
            todayBtn: true,
            autoclose: true
        }).on('changeDate', function (e) {
            var minDate = new Date(e.date.valueOf());
            $('#end_date').datepicker('setStartDate', minDate);
        });


        $('#end_date').datepicker({
            format: "dd-mm-yyyy",
            startDate: "od",
            todayHighlight: true,
            todayBtn: true,
            autoclose: true
        }).on('changeDate', function (e) {
            var maxDate = new Date(e.date.valueOf());
            $('#start_date').datepicker('setEndDate', maxDate);
        });

        //var minDate = moment().add(-1, 'seconds').toDate();
        $('input[id^=start_time]').datetimepicker({
            format: 'LT',
        }).inputmask('hh:mm t', {"placeholder": "hh:mm t", alias: "date", "clearIncomplete": true});
        $('input[id^=end_time]').datetimepicker({
            format: 'LT',
            useCurrent: false //Important! See issue #1075
        }).inputmask('hh:mm t', {"placeholder": "hh:mm t", alias: "date", "clearIncomplete": true});

        $("input[id^=start_time]").on("dp.change", function (e) {
            $(this).closest('.row').find('input[id^=end_time]').data("DateTimePicker").minDate(e.date.add(30, 'minutes').toDate());
        });
        $("input[id^=end_time]").on("dp.change", function (e) {
            $(this).closest('.row').find('input[id^=start_time]').data("DateTimePicker").maxDate(e.date);
        });
    });
</script>
@stop
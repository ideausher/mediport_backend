@extends('admin.layouts.default')
@section('title')
@parent :: {!! trans('admin/dashboard.dashboard') !!}
@stop
@section('content')
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            {!! trans('admin/dashboard.dashboard') !!}
            <small>{!! trans('admin/dashboard.control_panel') !!}</small>
        </h1>

    </section>

    <!-- Main content -->
    <section class="content">
        <!-- Small boxes (Stat box) -->
        <div class="row">
            <div class="col-lg-4 col-xs-6">
                <!-- small box -->
                <div class="small-box bg-fuchsia">
                    <div class="inner">
                        <h3>{!! $users !!}</h3>
                        <p>{!! trans('admin/dashboard.total_users') !!}</p>
                    </div>
                    <div class="icon">
                        <i class="ion ion-person-add"></i>
                    </div>
                    <a href="{!!url('admin/patients')!!}" class="small-box-footer">{!! trans('admin/dashboard.go_to') !!} <i class="fa fa-arrow-circle-right"></i></a>
                </div>
            </div><!-- ./col -->
            <div class="col-lg-4 col-xs-6">
                <!-- small box -->
                <div class="small-box bg-fuchsia">
                    <div class="inner">
                        <h3>{!! $totalVendors !!}</h3>
                        <p>{!! trans('admin/dashboard.total_vendors') !!}</p>
                    </div>
                    <div class="icon">
                        <i class="ion ion-person-add"></i>
                    </div>
                    <a href="{!!url('admin/doctors')!!}" class="small-box-footer">{!! trans('admin/dashboard.go_to') !!} <i class="fa fa-arrow-circle-right"></i></a>
                </div>
            </div><!-- ./col -->

            <div class="col-lg-4 col-xs-6">
                <!-- small box -->
                <div class="small-box bg-purple">
                    <div class="inner">
                        <h3>{!! $bookings !!}</h3>
                        <p>{!! trans('admin/dashboard.total_bookings') !!}</p>
                    </div>
                    <div class="icon">
                        <i class="ion ion-bookmark"></i>
                    </div>
                    <a href="{!!url('admin/appointments')!!}" class="small-box-footer">{!! trans('admin/dashboard.go_to') !!} <i class="fa fa-arrow-circle-right"></i></a>
                </div>
            </div><!-- ./col -->
            <!-- <div class="col-lg-4 col-xs-6">
                <div class="small-box bg-fuchsia">
                    <div class="inner">
                        <h3>{!! $transactions !!}</h3>
                        <p>{!! trans('admin/dashboard.total_transactions') !!}</p>
                    </div>
                    <div class="icon">
                        <i class="ion ion-cash"></i>
                    </div>
                    <a href="{!!url('admin/transaction')!!}" class="small-box-footer">{!! trans('admin/dashboard.go_to') !!} <i class="fa fa-arrow-circle-right"></i></a>
                </div>
            </div> -->
            
            <!-- ./col -->
            <!-- <div class="col-lg-4 col-xs-6">
                <div class="small-box bg-purple">
                    <div class="inner">
                        <h3>{!! $enquiries !!}</h3>
                        <p>{!! trans('admin/dashboard.total_enquiries') !!}</p>
                    </div>
                    <div class="icon">
                        <i class="ion ion-email"></i>
                    </div>
                    <a href="{!!url('admin/enquiry')!!}" class="small-box-footer">{!! trans('admin/dashboard.respond_now') !!} <i class="fa fa-arrow-circle-right"></i></a>
                </div>
            </div> -->
            <!-- ./col -->
        </div><!-- /.row -->
        <!-- Main row -->
    </section><!-- /.content -->
</div><!-- /.content-wrapper -->
@stop

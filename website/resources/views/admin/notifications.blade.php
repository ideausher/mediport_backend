@extends('admin.layouts.default')

{{-- Web site Title --}}
@section('title')
@parent :: {!! trans('admin/notification.list') !!}
@stop
@section('styles')
<link href="{!! asset('assets/admin/plugins/bootstrap3-editable/css/bootstrap-editable.css') !!}" rel="stylesheet"
      type="text/css" />
@stop
{{-- Content --}}
@section('content')
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>{!! trans('admin/notification.list') !!}</h1>
    </section>
    <!-- Main content -->
    <section class="content">
        <!-- Main row -->
        <div class="row">
            <div class="col-md-12">
                <!-- Notifications -->
                @include('admin.includes.notifications')
                <!-- ./ notifications -->
            </div>
            <div class="col-xs-12">
                <?php
                ?>

                <div class="box">
                    <div class="row">
                        <div class='col-sm-12 text-right'>
                            <button type="button" title="Add new Slots" class="btn btn-primary modal-window-doctor doctor-window" data-toggle="modal" data-target="#exampleModalDoctor">Doctor</button>
                            &nbsp;&nbsp;
                            <button type="button" title="Add new Slots" class="btn btn-primary modal-window-patient patient-window" data-toggle="modal" data-target="#exampleModalPatient">Patient</button>
                            &nbsp;&nbsp;
                        </div>
                        <div class='col-sm-6'>
                            <div class="show-list">
                            </div>
                        </div>
                    </div>
                    <div class="box-body table-responsive">
                        <table id="user_list" class="table table-bordered">
                            <thead>
                                <tr>
                                    <th>{!! trans('admin/notification.name') !!}</th>
                                    <th>{!! trans('admin/notification.type') !!}</th>
                                    <th>{!! trans('admin/notification.title') !!}</th>
                                    <th>{!! trans('admin/notification.message') !!}</th>

                                </tr>
                            </thead>
                            <tbody>
                                @if($notifications->count() > 0)
                                @foreach($notifications as $notification)
                                <tr>
                                    <td>{{ $notification->getUser->firstname .' '.$notification->getUser->lastname  }}</td>
                                    <td>{{ $notification->type}}</td>
                                    <td>{{ $notification->title}}</td>
                                    <td>{{ $notification->message}}</td>
                                </tr>
                                @endforeach
                                @else
                                <tr>
                                    <td>No Record found.</td>
                                </tr>
                                @endif
                            </tbody>
                        </table>
                        @if($type=='all')
                        <div class="box-footer clearfix">
                            <div class="col-md-12 text-center pagination pagination-sm no-margin">
                                @if($notifications)
                                {!! $notifications->render() !!}
                                @endif
                            </div>
                            <div class="col-md-12 text-center">
                                <a class="btn">{!! trans('admin/common.total') !!} {!! $notifications->total() !!} </a>
                            </div>
                        </div><!-- /. box-footer -->
                        @endif
                    </div> <!-- /. box body -->
                </div> <!-- /.box -->
            </div> <!-- /.col-xs-12 -->
        </div>
        <div class="modal fade" id="exampleModalDoctor" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h2 class="modal-title" id="exampleModalCenterTitle"><b>Select Doctor</b></h2>
                        <button type="button" class="close close-modal" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <div id="filterDate">
                                        {!! Form::open( array('route' => array('test-mail'), 'method' => 'POST', 'id' => 'doctor-notification', 'files' => true )) !!}
                                        <div class="col-xs-12">
                            <div class="form-group has-feedback">
                                        <select class="js-doctor-basic-multiple" style='width:100%; overflow:hidden; height:auto;' name="id[]" multiple="multiple">
                                            @if(isset($doctor))
                                            @foreach($doctor as $doctors1)
                                            <option value='{{$doctors1['user_id']}}'>{{$doctors1['firstname']." " .$doctors1['lastname']}}</option>
                                            @endforeach    
                                            @endif
                                        </select>
                                </div></div>

                                    </div> <Br>
                                    <div id="filterDate">
                                        Subject <input placeholder='Subject' name='subject' size='60' type='text'>
                                    </div> 
                                    <br>
                                    <div id="filterDate">
                                        Message <textarea placeholder="Message"style="width: 455px; height: 119px;" name='message'></textarea>

                                    </div>    
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="show-slots-outer">

                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary close-modal" data-dismiss="modal">Close</button>
                        {!! Form::submit(trans('admin/common.submit'),array('class'=>'btn btn-primary', 'id'=>'submitform')) !!}

                        {!! Form::close()!!}
                    </div>
                </div>
            </div>
        </div>

        <div class="modal fade" id="exampleModalPatient" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
            
            <div class="modal-dialog modal-dialog-centered" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h2 class="modal-title" id="exampleModalCenterTitle"><b>Select Patient</b></h2>
                        <button type="button" class="close close-modal" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <div id="filterDate">
                                        {!! Form::open( array('route' => array('test-mail'), 'method' => 'POST', 'id' => 'patient-notification', 'files' => true )) !!}
                                         <div class="col-xs-12">
                            <div class="form-group has-feedback">
                                        <select class="js-patient-basic-multiple" style='width:100%;  height: auto!important;' name="id[]" multiple="multiple">
                                            @if(isset($patient))
                                            @foreach($patient as $patients1)
                                            <option value='{{$patients1['user_id']}}'>{{$patients1['firstname']." ".$patients1['lastname']}}</option>
                                            @endforeach     
                                            @endif
                                        </select>
                                 </div>
                        </div>

                                    </div> <Br>
                                    <div id="filterDate">
                                        Subject <input placeholder='Subject' name='subject' size='60' type='text'>
                                    </div> 
                                    <br>
                                    <div id="filterDate">
                                        Message <textarea placeholder="Message" name='message'style="width: 455px; height: 119px;"></textarea>

                                    </div>    
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="show-slots-outer">

                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary close-modal" data-dismiss="modal">Close</button>
                       {!! Form::submit(trans('admin/common.submit'),array('class'=>'btn btn-primary', 'id'=>'submitform')) !!}

                        {!! Form::close()!!}
                    </div>
                </div>
            </div>
        </div>
        <!-- /.row (main row) -->

    </section><!-- /.content -->
</div><!-- /.content-wrapper -->
@stop
@section('scripts')
<script>

    $(document).on('click', '.patient-window', function () {


    });
    /*   var oTable;
     
     oTable = $('#user_list1').DataTable({
     "dom": "<'row no-gutters'<'col-xs-12 col-sm-4 col-md-4 col-lg-4 no-padding'l><'col-xs-12 col-sm-4 col-md-4 col-lg-4'r><'col-xs-12 col-sm-4 col-md-4 col-lg-4 no-padding'f>>t<'row no-gutters'<'col-xs-12 col-sm-4 col-md-4 col-lg-4 no-padding'i><'col-xs-12 col-sm-4 col-md-4 col-lg-4'><'col-xs-12 col-sm-4 col-md-4 col-lg-4 no-padding'p>>",
     "processing": true,
     "serverSide": true,
     "ajax": "{!! url('admin/notification/doctors') !!}",
     
     
     "language": {
     "emptyTable": "{!! trans('admin/common.datatable.empty_table') !!}",
     "info": "{!! trans('admin/common.datatable.info') !!}",
     "infoEmpty": "{!! trans('admin/common.datatable.info_empty') !!}",
     "infoFiltered": "({!! trans('admin/common.datatable.info_filtered') !!})",
     "lengthMenu": "{!! trans('admin/common.datatable.length_menu') !!}",
     "loadingRecords": "{!! trans('admin/common.datatable.loading') !!}",
     "processing": "{!! trans('admin/common.datatable.processing') !!}",
     "search": "{!! trans('admin/common.datatable.search') !!}:",
     "zeroRecords": "{!! trans('admin/common.datatable.zero_records') !!}",
     
     }
     });  
     }); */
</script>
@stop
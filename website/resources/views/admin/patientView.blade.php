@extends('admin.layouts.default')

{{-- Web site Title --}}
@section('title')
@parent :: {!! trans('admin/service.services') !!}
@stop
@section('styles')
@stop
{{-- Content --}}
@section('content')
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>{!! trans('admin/user.User_edit') !!}</h1>


        <!-- <ol class="breadcrumb">
                <li><a href="/admin"><i class="fa fa-dashboard"></i> {!! trans('admin/common.home') !!}</a></li>
                <li class="active">{!! trans('admin/service.services') !!}</li>
            </ol> -->
    </section>
    <!-- Main content -->
    <section class="content">
        <!-- Main row -->
       <div class="row box" style="margin-left:0;">
        <div class="col-sm-4 custom-content-row-one">
         <div class="row">
             <div class="col-sm-11 content-box-custom custom-cont-one">
                 @if(isset($user) && !empty($user['image']))
               <?php $img=$user['image']; ?>
                    <img id="uploaded-image" src='{!! url("$img") !!}'  height="280" width="280">  
                    @else
                         <img id="uploaded-image" src="{!! url('uploads/user/default.png') !!}"  height="280" width="280">
                    
                    @endif
            </div>
            <div class="col-sm-11 content-box-custom custom-cont-two" style="margin-top:25px;">
            <div class="inner-custom-content">
             <!--   <span class="cont-info"><b>Address:</b><textarea placeholder="Address..." rows="3" cols="50"></textarea></span>   -->
                 <span class="cont-info phone"><b>First Name:</b><input placeholder="First Name.." readonly value="{{$user['firstname']}}"type="text"></span>
                  <span class="cont-info phone"><b>Last Name:</b><input placeholder="Last Name.." readonly value="{{$user['lastname']}}" type="text"></span>
                <span class="cont-info phone"><b>Phone:</b><input placeholder="Phone number.." readonly value="{{$user['phone_country_code']}} {{$user['phone_number']}}" type="text"></span>
                <span class="cont-info"><b>Email:</b><input placeholder="Email.." readonly value="{{$user['email']}}" type="email"></span>
            </div>
            </div>
        </div>
       </div>
       <div class="col-sm-8 custom-content-row-two">
       <div class="row">
            <div class="col-sm-12 content-box-custom custom-cont-three">
                <div class="inner-custom-content">
                <span><h3>Medical History</h3></span>
                </div>
            </div>
            <div class="col-sm-12 content-box-custom custom-cont-four" style="margin-top:25px;">
            <div class="inner-custom-content">
                <span><h3>Reports</h3></span>
                </div>
            </div>
        </div>
       </div>
       <div class="create-report-button">
         <button>Create Report</button>
     </div>
     </div>
    </section><!-- /.content -->
</div><!-- /.content-wrapper -->
@stop

@extends('admin.layouts.default')
{{-- Web site Title --}}
@section('title')
@parent :: {!! trans('admin/servicecategory.categories') !!}
@stop
@section('styles')
@stop
{{-- Content --}}
<script type="text/javascript" src="http://code.jquery.com/jquery-1.7.1.min.js"></script>
@section('content')
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>{!! trans('admin/vendors.add_doctor') !!}</h1>
    </section>
    <!-- Main content -->
    <section class="content">
        <!-- Main row -->
        <div class="row">
            <div class="col-md-12">
                <!-- Notifications -->
                @include('admin.includes.notifications')
            </div>
            <div class="col-xs-12">
                <div class="box">
                    @if(isset($category))
                    {!! Form::model($category, array('route' => array('updateCategory'), 'method' => 'POST', 'id' => 'service-form-1', 'files' => true )) !!}
                    @else
                    {!! Form::open(array('url' => 'admin/doctors', 'id' => 'add-vendor-form', 'files' => true)) !!}
                    @endif
                    {!! Form::hidden('category_id', isset($category) ? $category->id : 0 ,array('class'=>'form-control', 'id' => 'category_id')) !!}
                    <div class="box-body">
                    <div class="row justify-content-end">
                        <div class="col-xs-6">
                            <div class="form-group has-feedback">
                                {!! Form::label('Language', trans('admin/vendors.language')) !!}
                                <select name='lang_type' class='form-control'> 
                                    <option value="1">{!! trans('admin/vendors.english') !!}</option>
                                    <option value="2">{!! trans('admin/vendors.arabic') !!}</option>
                                    <option value="3">{!! trans('admin/vendors.spanish') !!}</option>
                                </select>
                                <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                            </div>
                        </div>
                        <div class="col-xs-6">
                            <div class="form-group has-feedback">
                                {!! Form::label('first_name', trans('admin/vendors.firstname')) !!}
                                {!! Form::text('first_name', old('first_name'), array('class'=>'form-control')) !!}
                                {!! Form::hidden('role_id',2) !!}
                                @if(isset($category->id))
                                <input type="hidden" name="id" value="{{ $category->id }}">
                                @endif
                                <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                            </div>
                        </div>
                        <div class="col-xs-6">
                            <div class="form-group has-feedback">
                                {!! Form::label('last_name', trans('admin/vendors.lastname')) !!}
                                {!! Form::text('last_name', old('last_name'), array('class'=>'form-control')) !!}
                                <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                            </div>
                        </div>
                    </div>
                    <div class="row justify-content-end">
                        <div class="col-xs-12">
                            <div class="form-group has-feedback">
                                {!! Form::label('email', trans('admin/vendors.email')) !!}
                                {!! Form::text('email', old('email'), array('class'=>'form-control')) !!}
                                <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                            </div>
                        </div>
                    </div>
                    <div class="row justify-content-end">
                        <div class="col-xs-6">
                            <div class="form-group has-feedback">
                                {!! Form::label('password', trans('admin/vendors.password')) !!}
                                {!! Form::password('password', array('class'=>'form-control')) !!}
                                <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                            </div>
                        </div>
                        <div class="col-xs-6">
                            <div class="form-group has-feedback">
                                {!! Form::label('confirm_password', trans('admin/vendors.confirm_password')) !!}
                                {!! Form::password('confirm_password',array('class'=>'form-control')) !!}
                                <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                            </div>
                        </div>
                    </div>
                    <div class="row justify-content-end">
                        <div class="col-xs-6">
                            <div class="form-group has-feedback">
                                {!! Form::label('gender', trans('admin/vendors.gender')) !!}
                                {!! Form::select('gender', array('male' => 'Male', 'female' => 'Female'),null,array('class'=>'form-control')) !!}
                                <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                            </div>
                        </div>
                        <div class="col-xs-6">
                            <div class="form-group has-feedback">
                                {!! Form::label('phone', trans('admin/vendors.Phone_no')) !!}
                                {!! Form::text('phone','',array('class'=>'form-control')) !!}
                                <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                            </div>
                        </div>
                    </div>
                    <div class="row justify-content-end">
                        <div class="col-xs-12">
                            <div class="form-group has-feedback">
                                {!! Form::label('address', trans('admin/vendors.full_address')) !!}
                                {!! Form::textarea('address',null,['class'=>'form-control','id'=>'address', 'rows' => 2, 'cols' => 40]) !!}
                                <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                            </div>
                        </div>
                    </div>
                    <div class="row justify-content-end">
                        <div class="col-xs-3">
                            <div class="form-group has-feedback">
                                {!! Form::label('city', trans('admin/vendors.city')) !!}
                                {!! Form::text('city',null,['class'=>'form-control','id'=>'city', 'rows' => 2, 'cols' => 40]) !!}

                                <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                            </div>
                        </div>
                
              
                        <div class="col-xs-3">
                            <div class="form-group has-feedback">
                                {!! Form::label('state', trans('admin/vendors.state')) !!}
                                {!! Form::text('state',null,['class'=>'form-control','id'=>'state', 'rows' => 2, 'cols' => 40]) !!}

                                <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                            </div>
                        </div>
                        <div class="col-xs-3">
                            <div class="form-group has-feedback">
                                {!! Form::label('country', trans('admin/vendors.country')) !!}
                                {!! Form::text('country',null,['class'=>'form-control','id'=>'country', 'rows' => 2, 'cols' => 40]) !!}
                                <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                            </div>
                        </div>
                    
                        <div class="col-xs-3">
                            <div class="form-group has-feedback">
                                {!! Form::label('postal-code', trans('admin/vendors.postal_code')) !!}
                                {!! Form::text('postal_code',null,['class'=>'form-control','id'=>'postal-code', 'rows' => 2, 'cols' => 40]) !!}

                                <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                            </div>
                        </div>
                    </div>
                    <div class="row justify-content-end">
                        <div class="col-xs-6">
                            <div class="form-group has-feedback">
                                {!! Form::label('experience', trans('admin/vendors.experience')) !!}
                                {!! Form::selectRange('experience', 1, 20, null, array('class'=>'form-control')); !!}

                                <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                            </div>
                        </div>
 
                        <div class="col-xs-6">
                            <div class="form-group has-feedback">
                                {!! Form::label('price', trans('admin/vendors.price')) !!}
                                {!! Form::text('price',null, array('class'=>'form-control')) !!}
                            </div>
                        </div>
                    </div>
                    <div class="row justify-content-end">
                        <div class="col-xs-12">
                            <div class="form-group has-feedback">
                                {!! Form::label('specialization', trans('admin/vendors.specialization')) !!}
                                <select name='specialization' class='form-control'> 
                                    @foreach($categories as $category)
                                    <option value="{{ $category->id }}">{{ $category->cat_name }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="row justify-content-end">
                        <div class="edu-details">
                            <div class="col-xs-12">
                                {!! Form::label('education-details', trans('admin/vendors.edu_details')) !!}
                            </div>

                            <div class="col-xs-12 row-1 edu-details-row">
                                <div class="col-xs-3">
                                    <div class="form-group has-feedback">
                                        {!! Form::label('degree', trans('admin/vendors.degree')) !!}
                                        {!! Form::text('degree[]',null, array('class'=>'form-control')) !!}
                                        <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                                    </div>
                                </div>
                                <div class="col-xs-3">
                                    <div class="form-group has-feedback">
                                        {!! Form::label('batch', trans('admin/vendors.batch')) !!}
                                        {!! Form::text('batch[]',null, array('class'=>'form-control')) !!}
                                        <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                                    </div>
                                </div>
                                <div class="col-xs-4">
                                    <div class="form-group has-feedback">
                                        {!! Form::label('edu_desc', trans('admin/vendors.edu_desc')) !!}
                                        {!! Form::textarea('edu_desc[]',null,['class'=>'form-control', 'rows' => 1, 'cols' => 20]) !!}
                                        <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                                    </div>
                                </div>
                                <div class="col-xs-2 buttons">
                                    <div class="form-group has-feedback">
                                        <button type="button" data-row="1" data-main-class="edu-details" class="btn btn-success btnadd">Add</button>
                                        <button type="button" data-row="1" data-main-class="edu-details" class="btn btn-success btnremove">Remove</button>
                                    </div>
                                </div>

                            </div>

                        </div>
                        
                    </div>
                    <div class="row justify-content-end">
                        <div class="img-details">
                            <div class="col-xs-12">
                                <div class="form-group has-feedback">
                                    {!! Form::label('image', trans('admin/vendors.add_image')) !!}
                                    <div class=row>

                                        <div class="col-md-10">
                                            {!! Form::file('image', array('id'=>'change-image','data-view'=>'uploaded-image','class'=>'form-control','style'=>'height:auto;')) !!}
                                            <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                                        </div>
                                        <div class="col-md-2">
                                            <img id="uploaded-image" data-src="{!! URL::asset('uploads/user/default.png') !!}" src="{!! URL::asset('uploads/user/default.png') !!}"  height="100" width="100">

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row justify-content-end">
                        <div class="box-footer">
                            {!! Form::submit(trans('admin/common.submit'),array('class'=>'btn btn-primary', 'id'=>'submitform')) !!}
                            <a href="{!! url('admin/doctors') !!}" class="btn btn-default">{!! trans('admin/common.cancel') !!}</a>
                        </div>
                    </div>
                        {!! Form::close()!!}
                    </div> <!-- /.box -->
                </div> <!-- /.col-xs-12 -->
            </div><!-- /.row (main row) -->

    </section><!-- /.content -->
</div><!-- /.content-wrapper -->

@stop
{{-- Scripts --}}
@section('scripts')
    {{-- <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCXMpUMMrjVgbWeWF99SfuFQhe06-ST62s&libraries=places&callback=initMap" async defer></script> --}}
@stop

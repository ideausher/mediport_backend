@extends('admin.layouts.default')

{{-- Web site Title --}}
@section('title')
@parent :: {!! trans('admin/faq.faq_list') !!}
@stop
@section('styles')

<style>
.credit-txt {
  cursor: pointer;
}
</style>
@stop
{{-- Content --}}
@section('content')
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>{!! trans('admin/faq.faq_list') !!}</h1>
  </section>
  <!-- Main content -->
  <section class="content">
    <!-- Main row -->
    <div class="row">
      <div class="col-md-12">
        <!-- Notifications -->
        @include('admin.includes.notifications')
        <!-- ./ notifications -->
      </div>
      <div class="col-xs-12">
        <div class="box">
          <div class="box-body table-responsive">
            <a href="{!! url('admin/faq/add') !!}" class="btn btn-success pull-right">{!! trans('admin/faq.add_new') !!}</a>
            <table id="faq_list" class="table table-bordered">
              <thead>
                <tr>
                  <th>{!! trans('admin/faq.question') !!}</th>
                  <th>{!! trans('admin/faq.answer') !!}</th>
                  <th>{!! trans('admin/faq.language') !!}</th>
                  <th>{!! trans('admin/common.action') !!}</th> 
                </tr>
              </thead>
              <tbody>
              </tbody>
            </table>
          </div> <!-- /. box body -->
        </div> <!-- /.box -->
      </div> <!-- /.col-xs-12 -->
    </div><!-- /.row (main row) -->

  </section><!-- /.content -->
</div><!-- /.content-wrapper -->
@stop

{{-- Scripts --}}
@section('scripts')
 
<script type="text/javascript">
var oTable;
 
$(document).ready(function() {

  oTable = $('#faq_list').dataTable({
     "dom": "<'row no-gutters'<'col-xs-12 col-sm-4 col-md-4 col-lg-4 no-padding'l><'col-xs-12 col-sm-4 col-md-4 col-lg-4'r><'col-xs-12 col-sm-4 col-md-4 col-lg-4 no-padding'f>>t<'row no-gutters'<'col-xs-12 col-sm-4 col-md-4 col-lg-4 no-padding'i><'col-xs-12 col-sm-4 col-md-4 col-lg-4'><'col-xs-12 col-sm-4 col-md-4 col-lg-4 no-padding'p>>",
    "processing": true,
    "serverSide": true,
    "ajax": "{!! url('admin/faqData') !!}",
    "order": [
      [3, "desc"]
    ],
    "language": {
      "emptyTable": "{!! trans('admin/common.datatable.empty_table') !!}",
      "info": "{!! trans('admin/common.datatable.info') !!}",
      "infoEmpty": "{!! trans('admin/common.datatable.info_empty') !!}",
      "infoFiltered": "({!! trans('admin/common.datatable.info_filtered') !!})",
      "lengthMenu": "{!! trans('admin/common.datatable.length_menu') !!}",
      "loadingRecords": "{!! trans('admin/common.datatable.loading') !!}",
      "processing": "{!! trans('admin/common.datatable.processing') !!}",
      "search": "{!! trans('admin/common.datatable.search') !!}:",
      "zeroRecords": "{!! trans('admin/common.datatable.zero_records') !!}",
      "paginate": {
        "first": "{!! trans('admin/common.datatable.first') !!}",
        "last": "{!! trans('admin/common.datatable.last') !!}",
        "next": "{!! trans('admin/common.datatable.next') !!}",
        "previous": "{!! trans('admin/common.datatable.previous') !!}"
      }
    }
  });

  $(document).on('click','.delete-btn',function(){
    var url = '{!! url("admin/faq") !!}' + '/' + $(this).data('id');
    $.ajax({
      url : url ,
      method : 'DELETE',
      success : function(res){
        console.log("Response Success : ",res);
        if(res.status == 'success'){
          alert(res.message);
          location.reload();
        }
      },
      error : function(err){
        console.log("Response Error : ",err);
        alert(err.message);
      }
    })
    return false;
  });

});
</script>
@stop

@extends('admin.layouts.default')

{{-- Web site Title --}}
@section('title')
@parent :: {!! trans('admin/faq.faq_edit') !!}
@stop
@section('styles')
<link rel="stylesheet" href="{!! asset('assets/admin/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css') !!}">
<style>
.credit-txt {
  cursor: pointer;
}
</style>
@stop
{{-- Content --}}
@section('content')
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>{!! trans('admin/faq.faq_edit') !!}</h1>
  </section>
  <!-- Main content -->
  <section class="content">
    <!-- Main row -->
    <div class="row">
      <div class="col-md-12">
        <!-- Notifications -->
        @include('admin.includes.notifications')
        <!-- ./ notifications -->
      </div>
      <div class="col-xs-12">
        <div class="box">
          <div class="box-body table-responsive">
              @if (session('status'))
                  <div class="alert {!! session('alert') !!}">
                      {{ session('status') }}
                  </div>
              @endif
              @if(isset($id))
                <form class="form" method="POST" action="{!! url('admin/faq/save-edit/'.$id) !!}">
              @else
                <form class="form" method="POST" action="{!! url('admin/faq/save-add') !!}">
              @endif
              
                {{ csrf_field() }}
                <div class="form-group">
                  <label for="question">{!! trans('admin/faq.language') !!}</label>
                  <select name="language" class="form-control"> 
                    <option value="en" {!! isset($data) && $data['language'] == 'en' ? 
                    'selected' : '' !!}>English</option>
                    <option value="es" {!! isset($data) && $data['language'] == 'es' ? 
                    'selected' : '' !!}>Spanish</option>
                    <option value="ar" {!! isset($data) && $data['language'] == 'ar' ? 
                    'selected' : '' !!}>Arabic</option>
                  </select>
                </div>
                <div class="form-group">
                  <label for="question">{!! trans('admin/faq.question') !!}</label>
                  <input type="question" class="form-control" id="question" placeholder="{!! trans('admin/faq.question') !!}" name="question" value="{!! isset($data) && $data['question'] ? $data['question'] : old('question') !!}">
                </div>
                <div class="form-group">
                  <label for="answer">{!! trans('admin/faq.answer') !!}</label>
                  <textarea name="answer" id="answer" style="width:100%;">{!! isset($data) && $data['answer'] ? $data['answer'] : old('answer') !!}</textarea>
                </div>
                <div class="form-group">
                  <button type="submit" class="btn btn-info pull-right">Save</button>
                </div>
              </form>
          </div> <!-- /. box body -->
        </div> <!-- /.box -->
      </div> <!-- /.col-xs-12 -->
    </div><!-- /.row (main row) -->
  </section><!-- /.content -->
</div><!-- /.content-wrapper -->
@stop

{{-- Scripts --}}
@section('scripts')
<script src="{!! asset('assets/admin/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js') !!}"></script>

<script>
  $(function () {
    
    //bootstrap WYSIHTML5 - text editor
    $('#answer').wysihtml5()
  })
</script>
@stop

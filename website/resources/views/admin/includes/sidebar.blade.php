<!-- Left side column. contains the logo and sidebar -->
<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
        <!-- sidebar menu: : style can be found in sidebar.less -->
        <ul class="sidebar-menu">
            <li class="{!! (Request::is('admin/dashboard') ? 'active' : '') !!}">
                <a href="{!!url('admin')!!}">
                    <i class="fa fa-dashboard"></i> <span>{!! trans('admin/sidebar.dashboard') !!}</span>
                </a>
            </li>

            <li class="treeview {!! (Request::is('admin/settings*') || Request::is('admin/paymentsettings*') || Request::is('admin/paypalsettings*') || Request::is('admin/password/change') ? ' active' : '') !!}">
                <a href="javascript:;">
                    <i class="fa fa-wrench"></i>
                    <span>{!! trans('admin/sidebar.settings') !!}</span>
                    <i class="fa fa-angle-right pull-right"></i>
                </a>
                <ul class="treeview-menu">

                   <li class="{!! (Request::is('admin/settings*') ? 'active' : '') !!}"><a href="{!!url('admin/settings')!!}"><i class="fa fa-angle-double-right"></i>{!! trans('admin/sidebar.general_setting') !!}</a></li>
<!--                    <li class="{!! (Request::is('admin/paymentsettings*') ? 'active' : '') !!}"><a href="{!!url('admin/paymentsettings')!!}"><i class="fa fa-angle-double-right"></i>{!! trans('admin/sidebar.payment_setting') !!}</a></li>-->
<!--                    <li class="{!! (Request::is('admin/paypalsettings*') ? 'active' : '') !!}"><a href="{!!url('admin/paypalsettings')!!}"><i class="fa fa-angle-double-right"></i>{!! trans('admin/sidebar.paypal_setting') !!}</a></li>-->
                    <li class="{!! (Request::is('admin/settings*') ? 'active' : '') !!}"><a href="{!!url('admin/faq')!!}"><i class="fa fa-angle-double-right"></i>{!! trans('admin/sidebar.faq') !!}</a></li>
                    <li class="{!! (Request::is('admin/password/change') ? 'active' : '') !!}"><a href="{!!url('admin/password/change')!!}"><i class="fa fa-angle-double-right"></i>{!! trans('admin/sidebar.change_password') !!}</a></li>
                </ul>
            </li>
<!--            <li class="treeview {!! (Request::is('admin/currency*') ? ' active' : '') !!}">
                <a href="javascript:;">
                    <i class="fa fa-money"></i>
                    <span>{!! trans('admin/sidebar.currency_management') !!}</span>
                    <i class="fa fa-angle-right pull-right"></i>
                </a>
                <ul class="treeview-menu">
                    <li class="{!! (Request::is('admin/currency/create') ? 'active' : '') !!}"><a href="{!!url('admin/currency/create')!!}"><i class="fa fa-angle-double-right"></i>{!! trans('admin/sidebar.add_currency') !!}</a></li>
                    <li class="{!! (Request::is('admin/currency') ? 'active' : '') !!}"><a href="{!!url('admin/currency')!!}"><i class="fa fa-angle-double-right"></i>{!! trans('admin/sidebar.currency_list') !!}</a></li>
                </ul>
            </li>-->
            <li class="treeview {!! (Request::is('admin/services*') ? ' active' : '') !!} {!! (Request::is('admin/addCategory') ? ' active' : '') !!} {!! (Request::is('admin/listCategory') ? ' active' : '') !!}">
                <a href="javascript:;">
                    <i class="fa fa-cogs"></i>
                    <span>{!! trans('admin/sidebar.services_management') !!}</span>
                    <i class="fa fa-angle-right pull-right"></i>
                </a>
                <ul class="treeview-menu">
                    <li class="{!! (Request::is('admin/addCategory') ? 'active' : '') !!}"><a href="{!!url('admin/addCategory')!!}"><i class="fa fa-angle-double-right"></i>{!! trans('admin/servicecategory.add_category') !!}</a></li>
                    <li class="{!! (Request::is('admin/listCategory') ? 'active' : '') !!}"><a href="{!!url('admin/listCategory')!!}"><i class="fa fa-angle-double-right"></i>{!! trans('admin/servicecategory.categories_list') !!}</a></li>
                    <!-- <li class="{!! (Request::is('admin/services/create') ? 'active' : '') !!}"><a href="{!!url('admin/services/create')!!}"><i class="fa fa-angle-double-right"></i>{!! trans('admin/sidebar.add_service') !!}</a></li>
                    <li class="{!! (Request::is('admin/services') ? 'active' : '') !!}"><a href="{!!url('admin/services')!!}"><i class="fa fa-angle-double-right"></i>{!! trans('admin/sidebar.services_list') !!}</a></li> -->
                     
                </ul>
            </li>
           <li class="{!! (Request::is('admin/refund/') ? 'active' : '') !!}">
                <a href="{!!url('admin/refund/')!!}">
                    <i class="fa fa-calendar"></i><span>{!! trans('admin/sidebar.refund') !!}</span>
                </a>
            </li>
           <li class="{!! (Request::is('admin/users') ? 'active' : '') !!}">
              <a href="javascript:;">
                    <i class="fa fa-user"></i>
                    <span>{!! trans('admin/sidebar.users') !!}</span>
                    <i class="fa fa-angle-right pull-right"></i>
                    <ul class="treeview-menu">
                    <li class="{!! (Request::is('admin/paymentsettings*') ? 'active' : '') !!}"><a href="{!!url('admin/patient/Add/')!!}"><i class="fa fa-angle-double-right"></i>{!! trans('admin/sidebar.add_user') !!}</a></li>
                    <li class="{!! (Request::is('admin/doctors*') ? 'active' : '') !!}"><a href="{!!url('admin/patients')!!}"><i class="fa fa-angle-double-right"></i>{!! trans('admin/sidebar.users_list') !!}</a></li>
                    
                </ul>
                
            </li>

            <li class="treeview {!! (Request::is('admin/settings*') || Request::is('admin/paymentsettings*') || Request::is('admin/paypalsettings*') || Request::is('admin/password/change') ? ' active' : '') !!}">
                <a href="javascript:;">
                    <i class="fa fa-user"></i>
                    <span>{!! trans('admin/sidebar.vendors') !!}</span>
                    <i class="fa fa-angle-right pull-right"></i>
                </a>
                <ul class="treeview-menu">
                    <li class="{!! (Request::is('admin/paymentsettings*') ? 'active' : '') !!}"><a href="{!!url('admin/doctors/create/')!!}"><i class="fa fa-angle-double-right"></i>{!! trans('admin/vendors.add_doctor') !!}</a></li>
                    <li class="{!! (Request::is('admin/doctors*') ? 'active' : '') !!}"><a href="{!!url('admin/doctors')!!}"><i class="fa fa-angle-double-right"></i>{!! trans('admin/sidebar.vendors_list') !!}</a></li>
                    
                </ul>
            </li>
            <li class="{!! (Request::is('admin/booking') ? 'active' : '') !!}">
                <a href="{!!url('admin/appointments')!!}">
                    <i class="fa fa-dollar"></i><span>{!! trans('admin/sidebar.booking_list') !!}</span>
                </a>
            </li>
           <li class="treeview {!! (Request::is('admin/coupons/create') ? ' active' : '') !!} {!! (Request::is('admin/coupons') ? ' active' : '') !!}">
                <a href="javascript:;">
                    <i class="fa fa-tags"></i>
                    <span>{!! trans('admin/sidebar.coupons') !!}</span>
                    <i class="fa fa-angle-right pull-right"></i>
                </a>
                <ul class="treeview-menu">
                    <li class="{!! (Request::is('admin/coupons/create') ? 'active' : '') !!}"><a href="{!!url('admin/coupons/create')!!}"><i class="fa fa-angle-double-right"></i>{!! trans('admin/coupons.add_coupon') !!}</a></li>
                    <li class="{!! (Request::is('admin/coupons') ? 'active' : '') !!}"><a href="{!!url('admin/coupons')!!}"><i class="fa fa-angle-double-right"></i>{!! trans('admin/coupons.list_coupon') !!}</a></li>
                </ul>
            </li>
<!--            <li class="treeview {!! (Request::is('admin/banners/create') ? ' active' : '') !!} {!! (Request::is('admin/banners') ? ' active' : '') !!}">
                <a href="javascript:;">
                    <i class="fa fa-cogs"></i>
                    <span>{!! trans('admin/sidebar.banners') !!}</span>
                    <i class="fa fa-angle-right pull-right"></i>
                </a>
                <ul class="treeview-menu">
                    <li class="{!! (Request::is('admin/banners/create') ? 'active' : '') !!}"><a href="{!!url('admin/banners/create')!!}"><i class="fa fa-angle-double-right"></i>{!! trans('admin/banners.add_banner') !!}</a></li>
                    <li class="{!! (Request::is('admin/banners') ? 'active' : '') !!}"><a href="{!!url('admin/banners')!!}"><i class="fa fa-angle-double-right"></i>{!! trans('admin/banners.list_banner') !!}</a></li>
                </ul>
            </li>-->
<!--            <li class="{!! (Request::is('admin/transaction') ? 'active' : '') !!}">
                <a href="{!!url('admin/transaction')!!}">
                    <i class="fa fa-dollar"></i><span>{!! trans('admin/sidebar.transaction_list') !!}</span>
                </a>
            </li>-->
            <!--li class="{!! (Request::is('admin/chatboard/*') ? 'active' : '') !!}">
                            <a href="{!!url('admin/chatboard')!!}">
                                <i class="fa fa-comment"></i> <span>{!! trans('admin/sidebar.chat_dashboard') !!}</span>
                            </a>
                        </li-->
            <!-- <li class="treeview {!! (Request::is('admin/enquiry*') ? ' active' : '') !!}">
                <a href="javascript:;">
                    <i class="fa fa-question-circle"></i>
                    <span>{!! trans('admin/sidebar.enquiry_management') !!}</span>
                    <i class="fa fa-angle-right pull-right"></i>
                </a>
                <ul class="treeview-menu">
                    <li class="{!! (Request::is('admin/enquiry') ? 'active' : '') !!}"><a href="{!!url('admin/enquiry')!!}"><i class="fa fa-angle-double-right"></i>{!! trans('admin/sidebar.enquiry_list') !!}</a></li>
                </ul>
            </li> -->

            <li class="{!! (Request::is('admin/notifications') ? ' active' : '') !!}">
                <a href="{!!url('admin/notifications/all')!!}">
                    <i class="fa fa-bell"></i>
                    <span>{!! trans('admin/sidebar.notifications') !!}</span>
                </a>
            </li>
        </ul>
    </section>
    <!-- /.sidebar -->
</aside>
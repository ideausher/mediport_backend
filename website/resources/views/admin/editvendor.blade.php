@extends('admin.layouts.default')
{{-- Web site Title --}}
@section('title')
@parent :: {!! trans('admin/service.services') !!}
@stop
@section('styles')
@stop
{{-- Content --}}
@section('content')
<style>
    .span-tab { display:inline-block; border:1px solid #ccc; padding:10px; border-radius:10px; background-color:#01bbcb; margin-left:10px; color:#fff;}
</style>
<div class="content-wrapper">
    <section class="content-header">
        <h1>{!! trans('admin/vendors.vendor_edit') !!}</h1>
    </section>
    <section class="content">
        <div class="row">
            <div class="col-md-12">
                @include('admin.includes.notifications')
            </div>
            <div class="col-xs-12">
                <div class="box">
                    {!! Form::model($user, array('url' =>'admin/doctors/'.$user->id, 'method' => 'PATCH', 'id' => 'edit-vendor-form','files' => true )) !!}
                    <div class="box-body">
                        <div class="col-xs-6">
                            <div class="form-group has-feedback">
                                {!! Form::label('firstname', trans('admin/vendors.firstname')) !!}
                                {!! Form::text('firstname', old('firstname'), array('class'=>'form-control')) !!}
                                <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                            </div>
                        </div>
                        <div class="col-xs-6">
                            <div class="form-group has-feedback">
                                {!! Form::label('lastname', trans('admin/vendors.lastname')) !!}
                                {!! Form::text('lastname', old('lastname'), array('class'=>'form-control')) !!}
                                <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                            </div>
                        </div>
                        <div class="col-xs-12">
                            <div class="form-group has-feedback">
                                {!! Form::label('email', trans('admin/vendors.email')) !!}
                                {!! Form::text('email', old('email'), array('class'=>'form-control')) !!}
                                <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                            </div>
                        </div>
                        <div class="col-xs-6">
                            <div class="form-group has-feedback">
                                {!! Form::label('gender', trans('admin/vendors.gender')) !!}
                                {!! Form::select('gender', array('male' => 'Male', 'female' => 'Female'),old('gender'),array('class'=>'form-control')) !!}
                                <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                            </div>
                        </div>
                        <div class="col-xs-6">
                            <div class="form-group has-feedback">
                                {!! Form::label('phone_number', trans('admin/vendors.Phone_no')) !!}
                                {!! Form::text('phone_number',old('phone_number'),array('class'=>'form-control')) !!}
                                <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                            </div>
                        </div>

                        <div class="col-xs-12">
                            <div class="form-group has-feedback">
                                {!! Form::label('address', trans('admin/vendors.full_address')) !!}
                                {!! Form::textarea('address',(isset($user->userAddress->full_address)) ? $user->userAddress->full_address :'',['class'=>'form-control','id'=>'address', 'rows' => 2, 'cols' => 40]) !!}                                   
                                <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                            </div>
                        </div>
                        <div class="col-xs-3">
                            <div class="form-group has-feedback">
                                {!! Form::label('city', trans('admin/vendors.city')) !!}
                                {!! Form::text('city',(isset($user->userAddress->city)) ? $user->userAddress->city: '',['class'=>'form-control']) !!}
                                <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                            </div>
                        </div>
                        <div class="col-xs-3">
                            <div class="form-group has-feedback">
                                {!! Form::label('state', trans('admin/vendors.state')) !!}
                                {!! Form::text('state',(isset($user->userAddress->state))? $user->userAddress->state:'',['class'=>'form-control']) !!}
                                <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                            </div>
                        </div>

                        <div class="col-xs-3">
                            <div class="form-group has-feedback">
                                {!! Form::label('country', trans('admin/vendors.country')) !!}
                                {!! Form::text('country',(isset($user->userAddress->country))?$user->userAddress->country:'',['class'=>'form-control']) !!}
                                <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                            </div>
                        </div>
                        <div class="col-xs-3">
                            <div class="form-group has-feedback">
                                {!! Form::label('postal_code', trans('admin/vendors.postal_code')) !!}
                                {!! Form::text('postal_code',(isset($user->userAddress->pincode))?$user->userAddress->pincode:'',['class'=>'form-control']) !!}

                                <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                            </div>
                        </div>
                        <div class="col-xs-6">
                            <div class="form-group has-feedback">
                                {!! Form::label('experience', trans('admin/vendors.experience')) !!}
                                {!! Form::selectRange('experience', 1, 20, (isset($venderServices[0]->experience)) ? $venderServices[0]->experience:0, array('class'=>'form-control')); !!}

                                <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                            </div>
                        </div>
                        <div class="col-xs-6">
                            <div class="form-group has-feedback">
                                {!! Form::label('price', trans('admin/vendors.price')) !!}
                                {!! Form::text('price',(isset($venderServices[0]->price))?$venderServices[0]->price:0, array('class'=>'form-control')) !!}
                            </div>
                        </div>
                        <div class="col-xs-12">
                            <div class="form-group has-feedback">
                                {!! Form::label('specialization', trans('admin/vendors.specialization')) !!}
                                <select name='specialization' class='form-control'> 
                                    @foreach($categories as $category)
                                    <option value="{{ $category->id }}" {{ ( $category->id == $venderServices[0]->cat_id) ? 'selected' : '' }}>{{ $category->cat_name }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>

                        <div class="edu-details">
                            <div class="col-xs-12">
                                {!! Form::label('education-details', trans('admin/vendors.edu_details')) !!}
                            </div>
                           @if(isset($vendorEducation[0]) && $vendorEducation) 
                           
                            @foreach($vendorEducation as $key => $education)
                            
                                <div class="col-xs-12 row-{{$key+1}} edu-details-row">
                                <div class="col-xs-3">
                                    <div class="form-group has-feedback">
                                        {!! Form::label('degree', trans('admin/vendors.degree')) !!}
                                        {!! Form::text('degree[]',$education->degree, array('class'=>'form-control')) !!}
                                    </div>
                                </div>
                                <div class="col-xs-3">
                                    <div class="form-group has-feedback">
                                        {!! Form::label('batch', trans('admin/vendors.batch')) !!}
                                        {!! Form::text('batch[]',$education->batch, array('class'=>'form-control')) !!}
                                    </div>
                                </div>
                                <div class="col-xs-4">
                                    <div class="form-group has-feedback">
                                        {!! Form::label('edu_desc', trans('admin/vendors.edu_desc')) !!}
                                        {!! Form::textarea('edu_desc[]',$education->edu_desc,['class'=>'form-control', 'rows' => 1, 'cols' => 20]) !!}

                                    </div>
                                </div>
                                
                                <div class="col-xs-2 buttons">
                                    <div class="form-group has-feedback">
                                    
                                        <button type="button" data-row="{{$key+1}}" data-main-class="edu-details"  class="btn btn-success btnadd @if(!(count($vendorEducation)==$key+1)) @then hidden @endif">Add</button>
                                        <button type="button" data-row="{{$key+1}}" data-main-class="edu-details"  class="btn btn-success btnremove">Remove</button>
                                    </div>
                                </div>
                            </div>
                            @endforeach 
                            
                            
                                
                                  
                           @else
                           <div class="col-xs-12 row-1 edu-details-row">
                                <div class="col-xs-3">
                                    <div class="form-group has-feedback">
                                        {!! Form::label('degree', trans('admin/vendors.degree')) !!}
                                        {!! Form::text('degree[]','', array('class'=>'form-control')) !!}
                                    </div>
                                </div>
                                <div class="col-xs-3">
                                    <div class="form-group has-feedback">
                                        {!! Form::label('batch', trans('admin/vendors.batch')) !!}
                                        {!! Form::text('batch[]','', array('class'=>'form-control')) !!}
                                    </div>
                                </div>
                                <div class="col-xs-4">
                                    <div class="form-group has-feedback">
                                        {!! Form::label('edu_desc', trans('admin/vendors.edu_desc')) !!}
                                        {!! Form::textarea('edu_desc[]','',['class'=>'form-control', 'rows' => 1, 'cols' => 20]) !!}

                                    </div>
                                </div>
                                <div class="col-xs-2 buttons">
                                    <div class="form-group has-feedback">
                                        <button type="button" data-row="1" data-main-class="edu-details" class="btn btn-success btnadd">Add</button>
                                        <button type="button" data-row="1" data-main-class="edu-details" class="btn btn-success btnremove">Remove</button>
                                    </div>
                                </div>

                            </div>
                           @endif
                        </div>
                        <div class="img-details">
                            <div class="col-xs-12">
                                <div class="form-group has-feedback">
                                    {!! Form::label('image', trans('admin/vendors.add_image')) !!}
                                    <div class=row>

                                        <div class="col-md-10">
                                            {!! Form::file('image', array('id'=>'change-image','data-view'=>'uploaded-image','class'=>'form-control','style'=>'height:auto;')) !!}
                                            <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                                        </div>
                                        <div class="col-md-2">
                                            @if (isset($user) && !is_null($user->image) && file_exists($user->image))
                                            <img id="uploaded-image" src="{{ $user->image }}"  height="100" width="100">
                                            @else
                                            <img id="uploaded-image" src="{!! URL::asset('uploads/user/default.png') !!}"  height="100" width="100">
                                            @endif
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>


                        <div class="box-footer">
                            {!! Form::submit(trans('admin/common.submit'),array('class'=>'btn btn-primary', 'id'=>'submitform')) !!}
                            <a href="{!! url('admin/doctors') !!}" class="btn btn-default">{!! trans('admin/common.cancel') !!}</a>
                        </div>
                        {!! Form::close()!!}
                    </div> <!-- /.box -->
                </div> <!-- /.col-xs-12 -->
            </div><!-- /.row (main row) -->

    </section><!-- /.content -->
</div><!-- /.content-wrapper -->
@stop
{{-- Scripts --}}
@section('scripts')
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCXMpUMMrjVgbWeWF99SfuFQhe06-ST62s&libraries=places&callback=initMap" async defer></script>
@stop
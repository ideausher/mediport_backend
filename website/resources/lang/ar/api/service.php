<?php

return [

    /*
    |--------------------------------------------------------------------------
    | admin Service Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used by the admin sidebar links.
    | You are free to change them to anything you want to customize 
    | your views to better match your application.
    |
    */
    
    
    'total_record' => 'العثور على السجلات',
    'no_record_found' => 'لا يوجد سجلات',
    'booked_successfully' => 'تم حجز الخدمة بنجاح',
    'no_new_slot_added' => 'لم تتم إضافة فتحة جديدة',
    'thanks_for_react' => 'شكرا على اهتمامك',
    'thanks_for_your_responce' => 'شكرا لأستجابتك',
    'already_booked' => 'خدمة قبلت بالفعل من قبل بائع آخر',
    'accepted_successfully' => 'تم قبول طلب الخدمة بنجاح',
    'all_banners' => 'جميع لافتات نشرت',
    'you_cant_work_in_this_slot' => 'لا يمكنك العمل في هذه الفتحة. لأنك لم تختر هذه الفتحة',
    'you_are_not_free_in_this_slot' => 'أنت لست مجاني لهذه الفتحة',
    'invalid_coupon' => 'رقم قسيمه غير صالح',
    'coupon_is_not_active' => 'الكوبون غير نشط',
    'order_amount_is_less_than_required' => 'الكوبون قابل للتطبيق على المبلغ الأدنى: -',
    'coupon_is_expired' => 'انتهت صلاحية الكوبون أو لم يبدأ بعد',
    'valid_coupon' => 'قسيمة صالحة',
    'order_status_changed' => 'تم تغيير حالة الطلب',
    'slots_added_successfully' => 'فتحات المضافة بنجاح',
    'booking_detail_found' => 'تم العثور على تفاصيل الحجز',
    'booking_not_exist' => 'الحجز غير موجود',
    'booking_detail' => 'تفاصيل الحجز',
    'booking_time_passed_out' => 'انتهى وقت الحجز بالفعل لذا لا يمكنك إلغاء هذا',
    'cant_cancel_24_hours_passed_in_booking_accept' => 'لا يمكنك إلغاء الحجز الآن. يمكنك الإلغاء قبل 24 ساعة من قبول الحجز',
    'cant_cancel_24_hours_passed_in_booking_create' => 'لا يمكنك إلغاء الحجز الآن. يمكنك الإلغاء قبل 24 ساعة من الحجز',
    'booking_cancel_successfully' => 'حجز إلغاء بنجاح',
    'no_change_in_slot' => 'لا توجد تغييرات في فتحات',
    'can_not_reschedule_booking' => 'لا يمكنك إعادة جدولة هذا الحجز حتى الآن. لأنه لم يتم تعيين البائع',
    'booking_rescheduled_successfully' => 'تم إعادة جدولة الحجز بنجاح',
    'no_notification' => 'لم يتم العثور على إخطار',
    'no_booking_found' => 'لم يتم العثور على الحجز',
    'avail_booked_slots' => 'فتحات المتاحة وحجزها',
    'choose_another_slot' => 'الرجاء اختيار أي فتحة أخرى لإعادة الجدولة. البائع غير مجاني لهذه الفتحة.',
    'commission_pending' => 'أنت لم تدفع عمولتك إلى zumpii. إذا كانت الدفعة المعلقة أكثر من 100 لا يمكنك تقديم خدمة للمستخدم',
    'vender_accepted_booking_detail' => 'قبل البائع تفاصيل الحجز',
    'please_select_default_address' => 'يرجى تحديد عنوانك الافتراضي أولاً',
    'noservice_avail' => 'لا توجد خدمة متاحة في هذا المجال',
    'job_already_completed' => 'لا يمكنك إلغاء هذه المهمة لأن المهمة قد اكتملت بالفعل',
    'vender_on_the_way' => 'لا يمكنك إلغاء هذه المهمة لأن البائع في طريقه بالفعل',
    'all_chats' => 'كل الدردشات',
    'no_chat_found' => 'لم يتم العثور على دردشة',
    'you_are_not_owner_of_this_booking' => 'أنت لست مالك هذا الحجز',
    'you_have_not_made_this_booking' => 'أنت لم تفعل هذا',
    'thanks_for_request_we_will_notify_once_request_approved' => 'شكرًا على طلبك ، وسنعلمك بمجرد موافقة المستخدم على طلبك',
    'message_sent_successfully' => 'تم إرسال رسالتك بنجاح',
    'all_filters' => 'جميع المرشحات',
    'all_messages' => 'جميع رسائل الدردشة',
    'no_message_found' => 'بدء محادثة جديدة',
    'rated_successfully' => 'لقد قيمت بنجاح',
    'already_rated' => 'لقد قمت بالفعل بالتقييم لهذا الحجز',
    'cant_rate_this_booking_vender_not_assigned' => 'لا يمكنك تقييم هذا الحجز. لأنه لم يتم تعيين بائع لهذا الحجز على الإطلاق',
    'notifications_found' => 'تم العثور على الإخطارات',
    'card_added' => 'تمت إضافة البطاقة بنجاح.',
    'coupon_added' => 'تم قبول القسيمة بنجاح.'
];

<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'service_not_vendor' => 'البائع لا ينتمي إلى معرف الخدمة.',
    'vendor_not_slot' => 'لا ينتمي البائع إلى معرف الفتحة هذا.',
    'report_rule' => 'يرجى التحقق من هويات التقرير. لا يتطابق مع سجلنا.',
    'service_slot_available' => 'عدد المرضى ممتلئ لهذه الفتحة.',
    'validate_coup' => "الكوبون غير صالح.",
    'user_not_match' => 'الحجز ليس ملكًا لك.',
    'vendor_must_doctor' => 'يجب أن يكون معرف البائع دور الطبيب.'
]

?>

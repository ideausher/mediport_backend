<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'otp_phone_not_match' => 'Otp أو الهاتف لا لا تتطابق.',
    'phone_already_registered' => 'رقم الهاتف مسجل بالفعل.',
    'role_not_exist' => 'الدور غير موجود',
    'registered_successfully' => 'تسجيل المستخدم بنجاح',
    'something_wrong' => 'هناك خطأ ما',
    'image_uploaded' => 'تحميل الصور بنجاح',
    'doc_uploaded' => 'تحميل المستند بنجاح',
    'user_updated' => 'تم تحديث المستخدم بنجاح',
    'loggedin_successfully' => 'تسجيل الدخول بنجاح',
    'password_match' => 'عدم تطابق كلمة المرور',
    'user_not_exist' => 'المستخدم غير موجود',
    'you_are_offline' => 'أنت غير متصل الآن',
    'you_are_online' => 'أنت متصل الآن',
    'loggedout_successfully' => 'تسجيل خروج المستخدم بنجاح',
    'otp_resent' => 'إعادة إرسال otp بنجاح.',
    'user_is_inactive' => 'المستخدم غير نشط',
    'no_vender_available_you_can_book_please_try_after_some_time' => 'لا يوجد أي بائع متاح في هذا الوقت. يرجى المحاولة بعد مرور بعض الوقت',
    'otp_sent' => 'أرسل Otp على هاتفك يرجى التحقق من رسالتك.',
    'invalid_otp' => 'otp غير صالح.',
    'otp_matched' => 'Otp الحصول على مطابقة.',
    'user_not_found' => 'المستخدم ليس موجود.',
    'old_password_not_matched' => 'كلمة المرور القديمة غير متطابقة.',
    'password_updated' => 'تم تحديث كلمة السر بنجاح.',
    'email_not_exist' => 'هذا البريد الإلكتروني غير موجود.',
    'otp_sent_on_email' => 'إرسال Otp على معرف البريد الإلكتروني الخاص بك',
    'otp_not_sent' => 'Otp لم ترسل',
    'email_already_exist' => 'البريد الالكتروني موجود بالفعل',
    'email_otp_sent' => 'أرسل Otp على البريد الإلكتروني',
    'not_verified_number' => 'الرقم غير مسجل على twillio',
    'referal_not_exist' => 'رمز الإحالة الخاطئ',
    'user_is_not_vender' => 'هذا ليس مستخدمًا بائعًا',
    'adress_added_successfully' => 'تمت إضافة العنوان بنجاح',
    'adress_deleted_successfully' => 'تم حذف العنوان بنجاح',
    'adress_updated_successfully' => 'تم تحديث العنوان بنجاح',
    'default_address_successfully' => 'تم تحديد العنوان الافتراضي بنجاح',
    'booked_slots' => 'حجز فتحات',
    'all_orders' => 'جميع الحجوزات',
    'no_vender_found_for_this_service' => 'لم يتم العثور على بائع',
    'this_slot_is_booked_for_all_vendeors' => 'جميع البائعين مشغولون في هذه الفتحة',
    'you_are_not_valid_user' => 'أنت لست مستخدمًا صالحًا',
    'account_review_pending_from_zumpii' => 'يرجى الانتظار حتى يقوم مزودو خدمة Mediport بمراجعة حسابك.',
    'account_rejected_from_zumpii' => 'تم رفض حسابك من قبل مقدمي خدمات Mediprort.',
    'online_status' => 'تم تغيير حالة الإنترنت بنجاح'
];

<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'enquiry_mail_sent_successfully' => 'شكرا على اتصال zumpii. سيتصل بك Zumpii في أقرب وقت ممكن على بريدك الإلكتروني.',
    'notification_update' => 'يتم تحديث إعداد الإعلام بنجاح.' 
     
];

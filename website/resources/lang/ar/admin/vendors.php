<?php
return [

    /*
    |--------------------------------------------------------------------------
    | admin users Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used by the admin sidebar links.
    | You are free to change them to anything you want to customize 
    | your views to better match your application.
    |
    */
    'language' => 'لغة',
    'english' => 'الإنجليزية',
    'arabic' => 'عربى',
    'spanish' => 'الأسبانية',
    
];
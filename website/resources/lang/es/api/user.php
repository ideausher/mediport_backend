<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'otp_phone_not_match' => 'Otp o teléfono no no coinciden.',
    'phone_already_registered' => 'Número de teléfono ya registrado.',
    'role_not_exist' => 'El rol no existe',
    'registered_successfully' => 'Usuario registrado exitosamente',
    'something_wrong' => 'Algo salió mal',
    'image_uploaded' => 'Carga de imagen exitosa',
    'doc_uploaded' => 'Carga de documentos exitosa',
    'user_updated' => 'Usuario actualizado con éxito',
    'activate_account_first' => 'Por favor, espere hasta que los proveedores de Mediport aprueben su cuenta.',
    'loggedin_successfully' => 'Inicia sesión correctamente',
    'password_match' => 'Contraseña no coincide',
    'user_not_exist' => 'el usuario no existe',
    'you_are_offline' => 'Estás desconectado ahora',
    'you_are_online' => 'Estás en línea ahora',
    'loggedout_successfully' => 'Cierre de sesión del usuario correctamente',
    'otp_resent' => 'Vuelva a enviar otp con éxito.',
    'user_is_inactive' => 'El usuario no está activo.',
    'no_vender_available_you_can_book_please_try_after_some_time' => 'No hay ningún proveedor disponible en este momento. Por favor intente después de algún tiempo',
    'otp_sent' => 'Otp enviado en su teléfono Por favor revise su mensaje.',
    'invalid_otp' => 'Otp inválido',
    'otp_matched' => 'Otp consigue emparejado.',
    'user_not_found' => 'Usuario no encontrado.',
    'old_password_not_matched' => 'La contraseña anterior no coincide.',
    'password_updated' => 'Contraseña actualizada exitosamente.',
    'email_not_exist' => 'Este correo electrónico no existe.',
    'otp_sent_on_email' => '
    Otp enviado en su correo electrónico de identificación Por favor revise su bandeja de entrada de correo electrónico',
    'otp_not_sent' => 'Otp no enviado',
    'email_already_exist' => 'Ya existe el correo electrónico',
    'email_otp_sent' => 'Otp enviado por correo electrónico',
    'not_verified_number' => 'Número no registrado en twillio',
    'referal_not_exist' => 'Código de referencia incorrecto',
    'user_is_not_vender' => 'Este no es un usuario vendedor',
    'adress_added_successfully' => 'Dirección agregada exitosamente',
    'adress_deleted_successfully' => 'Dirección eliminada correctamente',
    'adress_updated_successfully' => 'Dirección actualizada con éxito',
    'default_address_successfully' => 'Dirección predeterminada seleccionada correctamente',
    'booked_slots' => 'Tragamonedas reservadas',
    'all_orders' => 'Todas las reservas',
    'no_vender_found_for_this_service' => 'Ningún vendedor encontrado',
    'this_slot_is_booked_for_all_vendeors' => 'Todos los vendedores están ocupados en este espacio',
    'you_are_not_valid_user' => 'No eres un usuario valido',
    'account_review_pending_from_zumpii' => 'Por favor, espere hasta que los proveedores de Mediport revisen su cuenta.',
    'account_rejected_from_zumpii' => 'Su cuenta es rechazada por los proveedores de Mediport.',
    'online_status' => 'El estado en línea cambió con éxito'
];

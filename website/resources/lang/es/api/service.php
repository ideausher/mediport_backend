<?php

return [

    /*
    |--------------------------------------------------------------------------
    | admin Service Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used by the admin sidebar links.
    | You are free to change them to anything you want to customize 
    | your views to better match your application.
    |
    */
    
    
    'total_record' => 'Registros encontrados',
    'no_record_found' => 'Ningún record fue encontrado',
    'booked_successfully' => 'Servicio reservado con éxito',
    'no_new_slot_added' => 'No se ha agregado una nueva ranura',
    'thanks_for_react' => 'Gracias por tu interés',
    'thanks_for_your_responce' => 'Gracias por su respuesta',
    'already_booked' => 'Servicio ya aceptado por otro proveedor',
    'accepted_successfully' => 'Solicitud de servicio aceptada con éxito',
    'all_banners' => 'Todos los banners publicados',
    'you_cant_work_in_this_slot' => 'No puedes trabajar en esta ranura. Porque no has elegido esta ranura',
    'you_are_not_free_in_this_slot' => 'No eres libre para este espacio',
    'invalid_coupon' => 'Código de cupón inválido',
    'coupon_is_not_active' => 'El cupón no está activo.',
    'order_amount_is_less_than_required' => 'El cupón es aplicable a la cantidad mínima: -',
    'coupon_is_expired' => 'El cupón ha caducado o aún no ha comenzado',
    'valid_coupon' => 'Cupón válido',
    'order_status_changed' => 'Estado del pedido cambiado',
    'slots_added_successfully' => 'Ranuras añadidas con éxito',
    'booking_detail_found' => 'Detalle de reserva encontrado',
    'booking_not_exist' => 'Reserva no encontrada',
    'booking_detail' => 'Detalle de reserva',
    'booking_time_passed_out' => 'El tiempo de reserva ya pasó para que no pueda cancelar esto',
    'cant_cancel_24_hours_passed_in_booking_accept' => 'No puede cancelar la reserva ahora. Puede realizar el canel antes de 24 horas después de aceptar la reserva',
    'cant_cancel_24_hours_passed_in_booking_create' => 'No puede cancelar la reserva ahora. Puede realizar el canel antes de 24 horas después de la reserva',
    'booking_cancel_successfully' => '
    Reserva cancelada con éxito',
    'no_change_in_slot' => 'No hay cambios en las tragamonedas',
    'can_not_reschedule_booking' => 'No puede reprogramar esta reserva todavía. Porque el vendedor no está asignado',
    'booking_rescheduled_successfully' => 'Reserva reprogramada con éxito',
    'no_notification' => 'No se encontró ninguna notificación.',
    'no_booking_found' => 'No se encontró reserva',
    'avail_booked_slots' => 'Tragamonedas disponibles y reservadas',
    'choose_another_slot' => 'Elija cualquier otro espacio para reprogramar. El vendedor no es gratuito para este espacio.',
    'commission_pending' => 'No ha pagado su comisión al mediport. Si el pago pendiente es superior a 100, no puede proporcionar servicio al usuario',
    'vender_accepted_booking_detail' => 'Proveedor aceptado detalle de la reserva',
    'please_select_default_address' => 'Seleccione su dirección predeterminada primero',
    'noservice_avail' => 'No hay servicio disponible en esta área.',
    'job_already_completed' => 'No puede cancelar este trabajo porque el trabajo ya está completado',
    'vender_on_the_way' => 'No puede cancelar este trabajo porque el proveedor ya está en camino',
    'all_chats' => 'Todos los chats',
    'no_chat_found' => 'No se encontró chat',
    'you_are_not_owner_of_this_booking' => 'Usted no es dueño de esta reserva',
    'you_have_not_made_this_booking' => 'No has hecho esto',
    'thanks_for_request_we_will_notify_once_request_approved' => 'Gracias por su solicitud, le notificaremos una vez que su solicitud sea aprobada por el usuario.',
    'message_sent_successfully' => 'Su mensaje enviado exitosamente',
    'all_filters' => 'Todos los filtros',
    'all_messages' => 'Todos los mensajes de chat',
    'no_message_found' => 'Iniciar nuevo chat',
    'rated_successfully' => 'Has calificado exitosamente',
    'already_rated' => 'Ya has calificado para esta reserva',
    'cant_rate_this_booking_vender_not_assigned' => 'No puede calificar esta reserva. Porque ningún proveedor asignado a esta reserva nunca',
    'notifications_found' => 'Notificaciones encontradas',
    'card_added' => 'La tarjeta ha sido agregada exitosamente.',
    'coupon_added' => 'Cupón aceptado con éxito.'
];

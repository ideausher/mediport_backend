<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'enquiry_mail_sent_successfully' => 'Gracias por contactar mediport. Mediport se comunicará con usted lo antes posible en su correo electrónico.',
    'notification_update' => "
    La configuración de notificaciones se actualiza correctamente." 
];

<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'service_not_vendor' => 'El proveedor no pertenece a la identificación del servicio.',
    'vendor_not_slot' => 'El proveedor no pertenece a esta identificación de ranura.',
    'report_rule' => 'Por favor verifique los identificadores de informe. No coincide con nuestro registro.',
    'service_slot_available' => 'El número de pacientes está lleno para este espacio.',
    'validate_coup' => "El cupón no es válido.",
    'user_not_match' => 'La reserva no te pertenece.',
    'vendor_must_doctor' => 'La identificación del vendedor debe ser el papel de docter.'
]

?>

<?php

return [

    /*
    |--------------------------------------------------------------------------
    | admin Service Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used by the admin sidebar links.
    | You are free to change them to anything you want to customize 
    | your views to better match your application.
    |
    */
    
    'slots' => 'Slots',
    'title' => 'Slots',
    'slots_list' => 'Slots List',
    'date_break' => 'Date Breaks',
    'from' => 'From',
    'to' => 'To',
    'day' => 'Day',
    'slot_add_message' => 'Slot added successfully',
    'slot_update_message' => 'Slot updated successfully',
    'slot_delete_message' => 'Slot deleted successfully',
    'break_delete_message' => 'Break Deleted Successfully',
    'add_new_break' => 'Add New Break',
    'day_name'=>'Day Name',
    'status'=>'Status'
];

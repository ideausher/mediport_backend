<?php

return [

    /*
    |--------------------------------------------------------------------------
    | admin Service Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used by the admin sidebar links.
    | You are free to change them to anything you want to customize 
    | your views to better match your application.
    |
    */
    
    'pdf_generated' => 'Report Pdf file is generated successfully.',
    'generate' => 'Generate',
    'edit' => 'Edit',
    'view' => 'View',
    'report' => 'Report',
    'name' => 'Patient Name',
    'gender' => 'Patient Gender',
    'age' => 'Patient Age',
    'doctor_name' => 'Doctor Name',
    'doctor_category' => 'Doctor Specialisation',
    'date' => 'Date',
    'issue' => 'Disease',
    'report' => 'Doctor Feedback',
    'reference_id' => 'Reference Id',
    'report_pdf_title' => 'Mediport Doctor Report',
    'edit_success' => 'Report has been edited successfully.',
    'call' => 'Call',
]; 

<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'enquiry_mail_sent_successfully' => 'Thanks for contact mediport. Mediport will contact you as soon as possible on your email.',
    'notification_update' => "Notification setting is updated successfully." 
     
];

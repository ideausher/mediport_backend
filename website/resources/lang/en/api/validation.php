<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'service_not_vendor' => 'Vendor does not belongs to service id.',
    'vendor_not_slot' => 'Vendor not belongs to this slot id.',
    'report_rule' => 'Please check report ids. It does not match with our record.',
    'service_slot_available' => 'Number of patient is full for this slot.',
    'validate_coup' => "Coupon is not valid.",
    'user_not_match' => 'Booking is not belongs to you.',
    'booking_user_check' => 'Booking id is not belongs to you.',
    'before_one_day' => 'You can not send cancel request before 24 hours.',
    'vendor_must_doctor' => 'Vender id must be role of docter.'
]

?>

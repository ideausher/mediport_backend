<?php
return [

    /*
    |--------------------------------------------------------------------------
    | vendor report Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used by the admin sidebar links.
    | You are free to change them to anything you want to customize 
    | your views to better match your application.
    |
    */
    "refund_list" => "Refund List",
    "refernce_id" => "Reference Id",
    "useremail" => "User Email",
    "venderemail" => "Vendor Email",
    "amount" => "Amount",
    "curreny" => "Currency",
    "status" => "Status",
    "pending" => "Pending",
    "accepted" => "Accepted",
    "rejected" => "Rejected",
    "reason" => "Reason",
    "refund_success" => "Refund has been initiated successfully.",
    "refund_reject" => "Refund has been rejected successfully"
];
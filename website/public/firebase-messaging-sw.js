importScripts('https://www.gstatic.com/firebasejs/6.0.1/firebase-app.js');
importScripts('https://www.gstatic.com/firebasejs/6.0.1/firebase-messaging.js');
  var firebaseConfig = {
            apiKey: "AIzaSyDI_jjdAJjRTyVaLcbymcJaLGmuCQ3z9Vs",
            authDomain: "mediport-4fbf2.firebaseapp.com",
            databaseURL: "https://mediport-4fbf2.firebaseio.com",
            projectId: "mediport-4fbf2",
            storageBucket: "mediport-4fbf2.appspot.com",
            messagingSenderId: "433676390924",
            appId: "1:433676390924:web:0b8b1fb2fad10857a89429",
            measurementId: "G-9YX133X482"
        }
  firebase.initializeApp(firebaseConfig);
  
const messaging = firebase.messaging();
  
  messaging.setBackgroundMessageHandler(function(payload) {
  console.log('[firebase-messaging-sw.js] Received background message ', payload);
  // Customize notification here
  const notificationTitle = 'Background Message Title';
  const notificationOptions = {
    body: 'Background Message body.',
    icon: '/firebase-logo.png'
  };
  notificationTitle = payload.data.title;
  notificationOptions  = {
      body: payload.data.body,
  }

  return self.registration.showNotification(notificationTitle,
      notificationOptions);
});

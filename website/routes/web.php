<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

/** ------------------------------------------
 *  Admin Routes
 *  ------------------------------------------
 */
 
Route::group(array('prefix' => 'admin'), function() {
    
    Route::get('/', array('middleware' => 'guest.admin', 'uses' => 'Admin\LoginController@getIndex'));
    Route::get('logout', array('uses' => 'Admin\LoginController@doLogout'));
    Route::post('login', array('uses' => 'Admin\LoginController@doLogin'));
        
    // Password Reset Routes...
    Route::get('password/reset', array('uses'=>'Admin\ForgotPasswordController@showLinkRequestForm', 'as'=>'admin.password.email'));
    Route::post('password/email', array('uses'=>'Admin\ForgotPasswordController@sendResetLinkEmail', 'as'=>'admin.password.email'));
    Route::get('password/reset/{token}', array('uses'=>'Admin\ResetPasswordController@showResetForm', 'as'=>'admin.password.reset'));
    Route::post('password/reset', array('uses'=>'Admin\ResetPasswordController@reset', 'as'=>'admin.password.reset'));

    //after login
    Route::group(array('middleware' => 'auth.admin'), function() {
        
        Route::get('dashboard', 'Admin\DashboardController@index');
       
        #Settings Management
        Route::resource('settings', 'Admin\SettingsController');
        
        #paypal settings management
        Route::resource('paypalsettings', 'Admin\PaypalSettingsController');
        
        #currency management
        Route::get('currency/CurrencyData', 'Admin\CurrencyController@getCurrencyData');
        Route::post('currency/changeStatus', 'Admin\CurrencyController@changeCurrencyStatus');
        Route::resource('currency', 'Admin\CurrencyController');
        
        #payment settings management
        Route::resource('paymentsettings', 'Admin\PaymentSettingsController');
       
        #Admin Profile Management
        Route::resource('profile', 'Admin\ProfileController');
        
        #Admin password change
        Route::get('password/change', array('uses' => 'Admin\ProfileController@changePassword', 'as' => 'admin.password.change'));
        Route::post('password/change', array('uses' => 'Admin\ProfileController@updatePassword', 'as' => 'admin.password.change'));
        
        #Services Management
        Route::get('services/ServicesData', 'Admin\ServicesController@getServicesData');
        Route::post('services/changeStatus', 'Admin\ServicesController@changeServiceStatus');
        Route::resource('services','Admin\ServicesController');
        
        Route::post('servicesCategory/changeStatus', 'Admin\ServiceCategoryController@changeServiceStatus');
        Route::get('listCategory', 'Admin\ServiceCategoryController@index')->name('listCategory');
        Route::get('addCategory', 'Admin\ServiceCategoryController@create')->name('addCategory');
        Route::post('saveCategory', 'Admin\ServiceCategoryController@saveCategory')->name('saveCategory');
        Route::get('editCategory/{id}', 'Admin\ServiceCategoryController@editCategory')->name('editCategory');
        Route::post('updateCategory', 'Admin\ServiceCategoryController@updateCategory')->name('updateCategory');
        Route::get('deleteCategory/{id}', 'Admin\ServiceCategoryController@deleteCategory')->name('deleteCategory');
        
        //Route::resource('services','Admin\ServiceCategoryController');
        
        
        #Booking Management
        Route::get('booking/export', 'Admin\BookingController@export');
        Route::any('booking/search', array('uses' => 'Admin\BookingController@search', 'as' => 'admin.booking.search'));
        Route::post('booking/changeStatus', 'Admin\BookingController@changeBookingStatus');
        
        /////////////////////////////////////////////////////////////////////
        Route::resource('appointments','Admin\BookingController');
        


        #Transaction Management
        Route::get('transaction/export', 'Admin\TransactionController@export');
        Route::any('transaction/search', array('uses' => 'Admin\TransactionController@search', 'as' => 'admin.transaction.search'));
        Route::resource('transaction','Admin\TransactionController');
        
        #User Management
        Route::post('users/updateCredit', 'Admin\UserController@updateCredit');
        Route::get('users/UserData', 'Admin\UserController@getUserData');
        Route::get('vendors/VendorData', 'Admin\VendorController@getVendorData');
        ///

        /////////////////////////////////////////////////////////////////////
        Route::get('bookings/BookingData', 'Admin\BookingController@getBookingData');



        Route::post('users/changeStatus', 'Admin\UserController@changeUserStatus');
        Route::get('users/show/{id}', 'Admin\UserController@show');
        Route::get('users/showuser/{id}', 'Admin\UserController@showuser');
        Route::resource('patients', 'Admin\UserController');
         Route::any('patient/Add', 'Admin\UserController@Add');
          Route::any('patient/addUser', 'Admin\UserController@AddUser');
           Route::any('patient/edit/{id}', 'Admin\UserController@edit');
            Route::any('patient/update/{id}', 'Admin\UserController@update');
         
         
        Route::resource('doctors', 'Admin\VendorController');
        // Route::any('deleteEdu','Admin\VendorController@deleteEdu')->name('deleteEdu');
        Route::get('vendors/show_vendor/{id}', 'Admin\VendorController@show_vendor');
        Route::post('vendor/reject', 'Admin\VendorController@rejectVendor');
        Route::post('venderService/changeStatus', 'Admin\VendorController@changeVenderServiceStatus');
        Route::post('venderService/addVenderService', 'Admin\VendorController@addVenderService');
        Route::post('venderService/delete', 'VenderServiceController@destroy');
        Route::post('vendorSlots/delete', 'Admin\VendorController@deleteVendorSlot');
       // Route::any('test-mail', 'Admin\VendorController@testMail')->name('test-mail');
         Route::any('test-mail', 'Admin\NotificationController@NotificationEmail')->name('test-mail');
        
          
          
        Route::resource('users.booking', 'Admin\BookingController');
        Route::resource('users.transaction', 'Admin\TransactionController');
        
        #chat Management
        Route::get('chatboard/history/{id}', 'Admin\ChatController@history');
        Route::get('chatboard/{id}', 'Admin\ChatController@index');
        Route::post('chatboard/store', 'Admin\ChatController@store');
        Route::post('chatboard/notificationCount', 'Admin\ChatController@getNotificationCount');
        Route::resource('chatboard', 'Admin\ChatController');
        
        #Enquiry Management
        Route::get('enquiry/EnquiryData', 'Admin\EnquiryController@getEnquiryData');
        Route::post('enquiry/changeStatus', 'Admin\EnquiryController@changeEnquiryStatus');
        Route::resource('enquiry','Admin\EnquiryController');

        #Notifications Management
        Route::get('notifications/{type}', 'Admin\NotificationController@getNotifications');
        
        Route::get('slots/SlotData', 'Admin\SlotController@getSlotData');
        Route::resource('slots','Admin\SlotController');
        Route::post('slots/update', 'Admin\SlotController@update');
        Route::post('slots/checkOverlappingSlots', 'Admin\SlotController@checkOverlappingSlots');

        #Vender Services
        Route::get('editVenderService/{id}', 'VenderServiceController@edit')->name('editVenderService');
        Route::get('editVender/{id}', 'Admin\VendorController@edit')->name('editVender');
        Route::any('updateVenderService/{id}', 'VenderServiceController@update')->name('updateVenderService');
        
        #Coupons
        Route::get('coupons/couponData', 'Admin\CouponController@getCouponData');
        Route::post('coupons/changeStatus', 'Admin\CouponController@changeCouponStatus');
        Route::resource('coupons','Admin\CouponController');
        
        #Banners
        Route::get('banners/bannerData', 'Admin\BannerController@getBannerData');
        Route::post('banners/changeStatus', 'Admin\BannerController@changeBannerStatus');
        Route::resource('banners', 'Admin\BannerController');

        #notification
        Route::any('notification/doctors', 'Admin\NotificationController@getVendorData');       

        
        #Refund
        Route::get('refund','Admin\BookingController@refund');
        Route::get('refundData','Admin\BookingController@refundData');
        Route::post('refund','Admin\BookingController@refundStripe');

        #faq
        Route::get('faq','Admin\SettingsController@faq');
        
         #faq
         Route::get('faq','Admin\SettingsController@faq');
         Route::get('faqData','Admin\SettingsController@faqData');
         Route::get('faq/{id}/edit','Admin\SettingsController@faqEdit');
         Route::post('faq/save-edit/{id}','Admin\SettingsController@faqSaveEdit');
         Route::get('faq/add','Admin\SettingsController@faqAdd');
         Route::post('faq/save-add','Admin\SettingsController@faqSaveAdd');
         Route::delete('faq/{id}','Admin\SettingsController@faqDelete');
         Route::any('deleteEdu','Admin\VendorController@deleteEdu');
    });
});


/** ------------------------------------------
 *  Doctor Routes
 *  ------------------------------------------
 */

Route::group(array('prefix' => 'doctor'), function() {
    
    Route::get('/', array('middleware' => 'guest.doctor', 'uses' => 'doctor\LoginController@getIndex'));
    Route::get('logout', array('uses' => 'doctor\LoginController@doLogout'));
    Route::post('login', array('uses' => 'doctor\LoginController@doLogin'));        
    // Password Reset Routes...
    Route::get('password/reset', array('uses'=>'doctor\ForgotPasswordController@showLinkRequestForm', 'as'=>'doctor.password.email'));
    Route::any('password/email', array('uses'=>'doctor\ForgotPasswordController@sendResetLinkEmail'));
    Route::get('password/update-form', array('uses'=>'doctor\UpdatePasswordController@update'));
    Route::post('password/update', array('uses'=>'doctor\UpdatePasswordController@updatePassword'));


    //after login
    Route::group(array('middleware' => 'auth.doctor'), function() {
        
        Route::get('dashboard/{currency}', 'doctor\DashboardController@index');
        
        // Appointments of a doctor in doctor panel
        Route::get('appointments','doctor\BookingController@index');
        Route::get('bookings/sendCallNoti','doctor\BookingController@sendCallNotification');
        Route::get('chat/{id}','doctor\ChatController@index')->name('doctorchat');
        Route::get('report/{id}','doctor\ReportController@index');
        Route::post('reports/save','doctor\ReportController@generate');
        Route::resource('patients', 'doctor\userController');
        
        /*--slots Routes--*/
        Route::post('slots/getslots','doctor\SlotController@getSlots');
        Route::any('slots/deletebreak','doctor\SlotController@deleteBreak');
        Route::get('slots/getslotsdata','doctor\SlotController@getSlotsData');
        Route::post('slots/save-date-breaks','doctor\SlotController@SaveDateBreaks');
        Route::get('slots/datebreak','doctor\SlotController@dateBreak');
        Route::post('slots/checkOverlappingSlots', 'doctor\SlotController@checkOverlappingSlots');
        Route::post('slots/insertVenderBreak', 'doctor\SlotController@insertVenderBreak');
	  
        Route::post('slots/update','doctor\SlotController@update');
        Route::get('bookings/BookingData', 'doctor\BookingController@getBookingData');
        Route::resource('slots','doctor\SlotController');
        Route::get('report/view/{id}','doctor\ReportController@view');
        Route::get('report/edit/{id}','doctor\ReportController@edit');
        Route::post('reports/edit-save','doctor\ReportController@editSave');
        Route::any('edit/profile','doctor\profileController@edit');
        Route::any('doctor/update/{id}', 'doctor\userController@update');
        
        /*notificaiton route */
         Route::get('notifications/{type}', 'doctor\NotificationController@getNotifications');
        /*change password route */
         
        Route::get('password/change', array('uses' => 'doctor\profileController@changeDocPassword', 'as' => 'doctor.password.change'));
        Route::post('password/change', array('uses' => 'doctor\profileController@updateDocPassword', 'as' => 'doctor.password.change'));
        Route::post('fcm/savetoken', array('uses' => 'doctor\profileController@saveFcmToken', 'as' => 'doctor.fcm.saveToken'));

    });
});


/** ------------------------------------------
 *  Frontend Routes
 *  ------------------------------------------
 */

//Route::get('register', array('uses'=>'Frontend\UserController@create','as' => 'frontend.users.create'));

Route::get('/', function () {
   return redirect('admin');
});
#before login
//Route::get('/', 'Frontend\HomeController@index');

Route::post('contact', array('uses' => 'Frontend\HomeController@submitEnquiry', 'as' => 'contact'));
Route::post('Notification', array('uses' => 'Frontend\HomeController@submitNotification', 'as' => 'Notification'));

#login user
Route::post('login', array('uses' => 'Frontend\LoginController@doLogin', 'as' => 'frontend.login'));
Route::resource('users', 'Frontend\UserController');

// Password Reset Routes...
Route::get('password/reset', array('uses'=>'Frontend\ForgotPasswordController@showLinkRequestForm', 'as'=>'password.email'));
Route::post('password/email', array('uses'=>'Frontend\ForgotPasswordController@sendResetLinkEmail', 'as'=>'password.email'));
Route::get('password/reset/{token}', array('uses'=>'Frontend\ResetPasswordController@showResetForm', 'as'=>'password.reset'));
Route::post('password/reset', array('uses'=>'Frontend\ResetPasswordController@reset', 'as'=>'password.reset'));

//after login
Route::group(array('middleware' => 'auth.user'), function() {
    
    Route::get('dashboard', 'FrontendDashboardController\DashboardController@index');
    
    Route::get('profile', 'Frontend\UserController@index');
    
    #user password change
    Route::get('password/change', array('uses' => 'Frontend\UserController@changePassword', 'as' => 'password.change'));
    Route::post('password/change', array('uses' => 'Frontend\UserController@updatePassword', 'as' => 'password.change'));
    
    #logout user
    Route::get('logout', 'Frontend\LoginController@doLogout');
    
    #chat
    Route::get('chat/show', 'Frontend\ChatController@show');
    Route::post('chat/store', 'Frontend\ChatController@store');
    Route::get('chat', 'Frontend\ChatController@index');
    
    #reservation
    Route::get('reservation/{id}/{day}', 'Frontend\ReservationController@getSpots');
    Route::get('getServices', 'Frontend\ReservationController@getServices');
    Route::resource('reservation', 'Frontend\ReservationController');
    
    #booking
    Route::get('booking/export', 'Frontend\BookingController@export');
    Route::any('booking/search', array('uses' => 'Frontend\BookingController@search', 'as' => 'booking.search'));
    Route::resource('booking', 'Frontend\BookingController');
    
    #buy credit
    Route::get('credit', 'Frontend\PaypalController@index');
    Route::get('credit/paypal', 'Frontend\PaypalController@getPaypal');
    Route::post('credit/paypal', array('uses' => 'Frontend\PaypalController@postPaypal', 'as' => 'credit.paypal'));
    Route::get('credit/success', array('uses' => 'Frontend\PaypalController@getSuccess', 'as' => 'credit.success'));
    Route::get('credit/cancel', array('uses' => 'Frontend\PaypalController@getCancel', 'as' => 'credit.cancel'));
    
    #transaction
    Route::get('transaction', 'Frontend\TransactionController@index');
    Route::any('transaction/search', array('uses' => 'Frontend\TransactionController@search', 'as' => 'transaction.search'));
    Route::get('transaction/export', 'Frontend\TransactionController@export');
});


//cron job
Route::get('/cron/bookingstatus', 'Frontend\BookingController@cronBookingStatus');

/** ------------------------------------------
 *  GLOBAL variable define
 *  ------------------------------------------
 */
defined('LOGO_PATH') or define('LOGO_PATH', base_path() . '/uploads/logo/');
defined('LOGO_ROOT') or define('LOGO_ROOT', URL('uploads/logo') . '/');

defined('ADMIN_IMAGE_PATH') or define('ADMIN_IMAGE_PATH', base_path() . '/uploads/admin/');
defined('ADMIN_IMAGE_ROOT') or define('ADMIN_IMAGE_ROOT', URL('uploads/admin') . '/');

defined('USER_IMAGE_PATH') or define('USER_IMAGE_PATH', base_path() . '/uploads/user/');
defined('USER_IMAGE_ROOT') or define('USER_IMAGE_ROOT', URL('uploads/user') . '/');


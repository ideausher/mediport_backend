<?php

return [
    'driver' => env('FCM_PROTOCOL', 'http'),
    'log_enabled' => true,

    'http' => [
        'server_key' => env('FCM_SERVER_KEY', 'AAAA4alr4Kg:APA91bFxQe64biiXD5dIpKiiUl0aSf4Keb5GybuF4D8m300Tn568GjS8s4GvK3qGr9gwwe3YoSVVgFJxsuFKbSDS0wUuSTG5swyBjDKXdrfj-XTaEHQ2HfHZApW7RLBXl7d5Ix7vxafz'),
        'sender_id' => env('FCM_SENDER_ID', '969210060968'),
        'server_send_url' => 'https://fcm.googleapis.com/fcm/send',
        'server_group_url' => 'https://android.googleapis.com/gcm/notification',
        'timeout' => 30.0, // in second
    ],
];

<?php

return [
    'ADMIN_IMAGE_ROOT' =>  env('APP_URL').'/uploads/admin/',
    
    'ADMIN_IMAGE_PATH' =>  base_path().'/public/uploads/admin/',
    
    'LOGO_ROOT' => env('APP_URL').'/uploads/logo/',
    
    'LOGO_PATH' => base_path() . '/uploads/logo/',
    
    'USER_IMAGE_ROOT' => env('APP_URL').'/uploads/user/',
    
    'USER_IMAGE_PATH' =>base_path() . '/uploads/user/',
];

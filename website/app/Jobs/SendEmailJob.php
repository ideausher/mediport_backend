<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Mail;
use App\Models\Notification;
use App\Mail\Admin\NotificationEmail;

class SendEmailJob implements ShouldQueue {

    use Dispatchable,
        InteractsWithQueue,
        Queueable,
        SerializesModels;

    public $details;
    public $data;
    public $profile;
    public $tries = 5;
    public $timeout = 60;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($user, $data, $profile) {
        $this->details = $user;
        $this->data = $data;
        $this->profile = $profile;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle() {


        foreach ($this->details as $key => $value) {

            $notification = new Notification;
            $notification->vender_id = $value['id'];
            $notification->user_id = $this->profile['id'];
            $notification->title = $this->data['subject'];
            $notification->message = $this->data['message'];
            $notification->save();
            
            $email = new NotificationEmail($this->data);
            Mail::to($value['email'])->send($email);
           
        }
        
    }

}

<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;
use App\Coupon;
use Carbon\Carbon;

class ValidateCoup implements Rule
{
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $coupon = Coupon::where('code',$value)->first();
        if($coupon){
            $maxTotalUse = $coupon->maxTotalUse;
            $maxUseCustomer = $coupon->maxUseCustomer;

            $startDateTime = $coupon->startDateTime;
            $endDateTime = $coupon->endDateTime;
            $current_date = Carbon::now()->toDateTimeString();
        
            if($coupon->status == 2){
                return false;
            }
            elseif($maxTotalUse > 0 && $maxTotalUse <= $maxUseCustomer){

                return false;
                
            }
            else if($startDateTime != null && $startDateTime <= $current_date && $endDateTime <= $current_date && $endDateTime != null ){

                return false;

            }else{

                return true;

            }
        }
        
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return trans('api/validation.validate_coup');
    }
}

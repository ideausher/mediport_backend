<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;
use App\Booking;
use Carbon\Carbon;

class UpdateStatusTime implements Rule
{
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $booking = Booking::find($value);
        $currentDateTime = Carbon::now()
                                  ->addDays(1)
                                  ->format('Y-m-d H:i:s');
    
        if($booking && $booking->appointment_time > $currentDateTime){
            return true;
        }
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return trans('api/validation.before_one_day');
    }
}

<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;
use App\Booking;
use App\venderSlot;

class SlotAvailabityCheck implements Rule
{
    protected $vendor_id;
    protected $service_id;
    protected $booking_date;

    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct($vendor_id,$service_id,$booking_date)
    {
        $this->vendor_id = $vendor_id;
        $this->service_id = $service_id;
        $this->booking_date = $booking_date;
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $vendorSlot = venderSlot::where([
                                    'vender_id' => $this->vendor_id,
                                    'is_active' => '1',
                                    'id' => $value
                                ])
                                ->first();
        if($vendorSlot){
            $countBooking = Booking::where([
                'vender_id' => $this->vendor_id,
                'service_id' => $this->service_id,
                'booking_date' => $this->booking_date,
                'vendor_slot_id' => $value
            ])->count();
            return $countBooking < $vendorSlot->max_patient;
        }
        
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return trans('api/validation.service_slot_available');
    }
}

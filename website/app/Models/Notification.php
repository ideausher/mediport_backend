<?php

namespace App\Models;

use App\Services\PushNotification;
use Illuminate\Database\Eloquent\Model;

class Notification extends Model {

    protected $table = 'notifications';
    protected $fillable = ['*'];
    public $timestamps = true;

    public function getUser() {
        return $this->belongsTo('App\User', 'user_id', 'id');
    }

    public function createNotification($user,$data) {

        $pushNotification = new PushNotification();
        $notificationData['data']['"id"'] = 1;
        $notificationData['data']['"user_id"'] = '';
        $notificationData['data']['"type_id"'] = 0;
        $notificationData['data']['"type"'] = '';
        $notificationData['data']['"title"'] = $data['subject'];
        $notificationData['data']['"message"'] = $data['message'];
        $notificationData['data']['"created_at"'] = '';
        $notificationData['data']['"updated_at"'] = '';
        $notificationData['data']['"notification_type"'] = 0;
        $notificationData['message'] = $data['message'];
        $notificationData['title'] = $data['subject'];
        $pushNotification->sendNotification($notificationData, $user['id']);


        return true;
    }

}

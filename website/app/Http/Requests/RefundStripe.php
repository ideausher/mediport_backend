<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class RefundStripe extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'id' => ['required','exists:booking_refund_request,id,status,1'],
            'req_type' => ['required','in:1,2','integer']
        ];
    }
}

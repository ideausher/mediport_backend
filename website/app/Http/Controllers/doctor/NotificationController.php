<?php

namespace App\Http\Controllers\doctor;

use Illuminate\Http\Request;
use Auth;
use App\Models\Notification;
use App\Http\Controllers\Controller;
use App\User;

class NotificationController extends Controller
{
    //
     public function getNotifications($type = null)
    {
          $user_id = Auth::guard('doctor')->user()->id;
        if ($type == 'all') {
            $notifications = Notification::with('getUser')->orderBy('id', 'DESC')->where('vender_id',$user_id )->paginate(10);
        } else {
            $notifications = Notification::where('is_read', 0)->with('getUser')->orderBy('id', 'DESC')->where('vender_id', $user_id)->get();
            foreach($notifications as $notification){
                $notification->is_read = 1;
                $notification->save();
            }
        }
        return view('doctor/notifications', compact('notifications', 'type'));
    }
}

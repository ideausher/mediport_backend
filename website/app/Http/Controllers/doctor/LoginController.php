<?php

namespace App\Http\Controllers\doctor;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers; 
use Illuminate\Support\Facades\Auth;
use App\DeviceDetails;

class LoginController extends Controller {

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    protected $redirectTo = 'doctor/';

    public function __construct() {

        $this->middleware('guest.doctor', ['except' => 'doLogout']);
    }

    /**
     * Display a login page
     * @return Response
     */
    public function getIndex() {

        return view('doctor.login');
    }

    /**
     * Check login with username and password.
     * @return redirect to dashboard or error_message if login fails
     */
    public function doLogin(Request $request) {

        $input = $request->all();

        $credentials = array(
            'email' => $input['username'],
            'password' => $input['password']
        );
        if (Auth::guard('doctor')->attempt($credentials,true)) {
   
            if (Auth::guard('doctor')->user()->hasRole('doctor')) {
                
                return redirect('doctor/dashboard/usd');
            } else {
                return redirect('doctor/')->with('error_message', trans('admin/login.invalid_login_message'))->withInput();
            }
        }
        // authentication failure! lets go back to the login page
        return redirect('doctor/')->with('error_message', trans('admin/login.invalid_login_message'))->withInput();
    }

    public function doLogout() {
        $user_id = Auth::guard('doctor')->user()->id;
        DeviceDetails::where('user_id', $user_id)->delete();
        auth()->guard('doctor')->logout();
        session()->flush();
        session()->regenerate();
        return redirect('/doctor')->with('success_message', trans('admin/login.logout_message'));
    }

}

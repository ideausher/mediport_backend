<?php

namespace App\Http\Controllers\doctor;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use Auth;

class ChatController extends Controller
{
    //
	public function __construct() {
        $this->middleware('auth.doctor');
    }
	
	public function index($id) {
        $data['mineId'] = Auth::guard('doctor')->user()->id;
        $data['userimage'] = Auth::guard('doctor')->user()->image ? Auth::guard('doctor')->user()->image : Auth::guard('doctor')->user()->social_image ;
        $data['user_id']=$id;
		$data['user']=User::where('id',$id)->first();
		
        return view('doctor.chat',$data);
    }
}

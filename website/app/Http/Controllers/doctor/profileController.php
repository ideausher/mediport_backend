<?php

namespace App\Http\Controllers\doctor;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Validator;
use Hash;
use App\Admin as Admin;
use App\Helpers\Datatable\SSP;
use App\User;
use Auth;
use App\vendorEducation;
use Response;
use App\ServiceCategory;
use App\DeviceDetails;

class profileController extends Controller {

    public function changeDocPassword() {

        return view('doctor/changePassword');
    }

    public function updateDocPassword(Request $request) {
        $data = $request->all();

        $user_id = Auth::guard('doctor')->user()->id;



        $admin = Admin::findOrFail($user_id);

        if (!Hash::check($data['old_password'], $admin->password)) {
            return redirect()->back()->with('error_message', trans('doctor/changePassword.invalid_password_message'));
        } else {
            $rules = array(
                'password' => 'required|confirmed|min:5',
            );
            $validator = Validator::make($data, $rules);
            if ($validator->fails()) {
                return redirect()->back()->withErrors($validator)->withInput();
            }

            $admin->password = Hash::make($data['password']);
            $admin->save();
            return redirect()->back()->with('success_message', trans('doctor/changePassword.password_change_message'));
        }
    }

    
    public function edit() {
        //$user = User::with('userAddress')->find($id);
       
        $user_id = Auth::guard('doctor')->user()->id;
        $user = User::with('userAddress', 'venderServices', 'vendorEducations')->find($user_id);
        $venderServices = User::find($user_id)->venderServices()->get();
        $vendorEducation = User::find($user_id)->vendorEducations()->get();
        $categories = ServiceCategory::orderBy('created_at', 'Asc')->get();

        if ($user) {
            return view('doctor/editvendor', compact('user','categories', 'venderServices','vendorEducation'));
        }
    }
    public function saveFcmToken(Request $request) {
        $data = $request->all();
        $user_id = Auth::guard('doctor')->user()->id;
        
        $device_detail = DeviceDetails::where('user_id', $user_id)->first();
        if(!$device_detail){
            DeviceDetails::create([
            'device_token' => $data['fcm_token'],
            'user_id' => $user_id,
        ]);
            session(['fcm_token' => $data['fcm_token']]);
            return Response::json(array('success' => true,'fcm_token'=> session('fcm_token'),'html' => 'Device token created'));
        }else{
            return Response::json(array('success' => false,'msg' => 'Device token is already exist'));
        }
    }
    
}

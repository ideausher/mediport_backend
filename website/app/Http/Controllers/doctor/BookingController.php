<?php

namespace App\Http\Controllers\doctor;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Booking;
use App\BookingDetail;
use App\Service;
use App\Helpers\Datatable\SSP;
use App\DeviceDetails;

class BookingController extends Controller
{

    /**
     * Booking Model
     * @var Booking
     */
    protected $booking;
    protected $pageLimit;
    
    /**
     * Inject the models.
     * @param Booking $booking
     */
    public function __construct(Booking $booking) {
        $this->booking = $booking;
        $this->pageLimit = config('settings.pageLimit');

    }


    /**
     * Function to get all appointments of doctor
     * @param
     * @return View
     * @author Durga Parshad
     * @since 19-December-2019
     */
    public function index(){

        // Show the booking
        return view('doctor/bookingList');
    }


    public function getBookingData() {

        $userId = auth()->guard('doctor')->user()->id;
        $table = 'bookings';
        $primaryKey = 'id';    

        $columns = array(
            array('db' => 'bookings.reference_id', 'dt' => 0, 'field' => 'reference_id'), 
            array('db' => 'bookings.user_id', 'dt' => 10, 'field' => 'user_id'), 
            array('db' => 'pu.email', 'dt' => 1, 'field' => 'email'),
            array('db' => 'sc.cat_name', 'dt' => 2, 'field' => 'cat_name'),
            array('db' => 'pu.gender', 'dt' => 3, 'field' => 'gender'),
            array('db' => 'bookings.booking_date', 'dt' => 4, 'field' => 'booking_date'),
            array('db' => 'vs.start_time', 'dt' => 5, 'field' => 'start_time'),
            array('db' => "CONCAT(tr.currency, ' ', bookings.price )", "as" => "amount", 'dt' => 6, 'field' => 'amount'),
            array('db' => 'bookings.status', 'dt' => 7, 'field' => 'status', 'formatter' => function($d,$row){
                if($row['status'] == 1){
                    return '<span class="label label-warning">'.trans('doctor/booking.active').'</span>';
                }
                elseif($row['status'] == 2){
                    return '<span class="label label-danger">'.trans('doctor/booking.cancel').'</span>';
                }else{
                    return '<span class="label label-success">'.trans('doctor/booking.complete').'</span>';
                }
            }),
            array('db' => 'bookings.created_at', 'dt' => 8, 'field' => 'created_at'),
            array('db' => 'bookings.id', 'dt' => 9, 'formatter' => function( $d, $row ){
                // creating dynamically generated link where durl ---> dynamic url

                $durl  = env('CHAT_URL','default_value'); // fetched root part of link from env file
				$durl .="/room/".auth()->guard('doctor')->user()->id.$row['user_id']."/".
						auth()->guard('doctor')->user()->firstname;
                // $durl .= '/demos/dashboard/canvas-designer.html?open=true&sessionid=';
                // $durl  = $durl . auth()->guard('doctor')->user()->id .$row['user_id'];
                // $durl .= '&publicRoomIdentifier=dashboard&userFullName='. auth()->guard('doctor')->user()->firstname;
                // $durl .= '&sender_id='. auth()->guard('doctor')->user()->id;
                // $durl .= '&receiver_id='.$row['user_id'];
                // $durl .= '&audioconfig=true&videoconfig=true';

					
                if($row['status'] == 1){
                    $operation = ' <a href="report/' . $d .'" class="btn btn-primary d-inline-block" title="' . trans('doctor/report.generate') . '" data-toggle="tooltip"><i class="fa fa-file-pdf-o"></i></a>';
                    // call button
                    $operation .= ' <a href=" ' . $durl .'" class="btn btn-warning d-inline-block callIniat" title="' . trans('doctor/report.call') . '" data-toggle="tooltip" target="_blank"  data-sender="'.auth()->guard('doctor')->user()->id.'" data-receiver="'.$row['user_id'].'"><i class="fa fa-phone"></i></a>';
					//chat
					$operation .= ' <a href="'.url('doctor/chat').'" class="btn btn-default d-inline-block chatIniat" title="' . trans('Chat') . '" data-toggle="tooltip" target="_blank" firstname="patient " lastname="'.$row['user_id'].'" data-sender="'.auth()->guard('doctor')->user()->id.'" data-receiver="'.$row['user_id'].'"><i class="fa fa-comments"></i></a>';
                }

                elseif($row['status'] == 3){
                    $operation = ' <a href="report/edit/' . $d .'" class="btn btn-danger d-inline-block" title="' . trans('doctor/report.edit') . '" data-toggle="tooltip"><i class="fa fa-pencil-square-o"></i></a>';
                    $operation .= ' <a href="report/view/' . $d .'" class="btn btn-success d-inline-block" title="' . trans('doctor/report.view') . '" data-toggle="tooltip" target="_blank"><i class="fa fa-eye"></i></a>';
                    // call button
                    $operation .= ' <a href=" ' . $durl .'" class="btn btn-warning d-inline-block callIniat" title="' . trans('doctor/report.call') . '" data-toggle="tooltip" target="_blank"  data-sender="'.auth()->guard('doctor')->user()->id.'" data-receiver="'.$row['user_id'].'"><i class="fa fa-phone"></i></a>';
					//chat
					$operation .= ' <a href="'.url('doctor/chat').'" class="btn btn-default d-inline-block chatIniat" title="' . trans('Chat') . '" data-toggle="tooltip" target="_blank" firstname="patient " lastname="'.$row['user_id'].'" data-sender="'.auth()->guard('doctor')->user()->id.'" data-receiver="'.$row['user_id'].'"><i class="fa fa-comments"></i></a>';
                }
                else{
                    $operation = '';
                }
                    
                return $operation;
            }, 'field' => 'id') 
        );

        $sql_details = array(
            'user' => config('database.connections.mysql.username'),
            'pass' => config('database.connections.mysql.password'),
            'db' => config('database.connections.mysql.database'),
            'host' => config('database.connections.mysql.host')
        );
  
        $joinQuery = '';
        $joinQuery .= " LEFT JOIN users vu ON vu.id = bookings.vender_id";
        $joinQuery .= " LEFT JOIN users pu ON pu.id = bookings.user_id";
        $joinQuery .= " LEFT JOIN service_categories sc ON bookings.service_id = sc.id";
        $joinQuery .= " LEFT JOIN vender_slots vs ON vs.id = bookings.vendor_slot_id";
        $joinQuery .= " LEFT JOIN transactions tr ON tr.booking_id = bookings.id";

        $extraWhere = "bookings.vender_id = ".$userId; //"r.name='doctor'";
        $groupBy = "";//"users.id";

       

        echo json_encode(
                SSP::simple($_GET, $sql_details, $table, $primaryKey, $columns, $joinQuery, $extraWhere, $groupBy)
        );
    }

    /**
     * Function to get all appointments of doctor
     * @param {sender_id,receiver_id}
     * @return JSON
     * @author Durga Parshad
     * @since 12-Febuaray-2020
     */
    public function sendCallNotification(Request $request){
        $dataPush = [
            'sender_id' => $request['sender_id'],
            'receiver_id' => $request['receiver_id'],
            'room' => $request['sender_id'].$request['receiver_id']
        ];
        $title = "Doctor is calling to you.";
        $message = "You will received call from your doctor regarding your appointment.";
        
        //print_r($request['receiver_id']); exit;

        DeviceDetails::sendCallNotification($title ,$message,$request['receiver_id'],$dataPush);
	//print_r($dd);exit;
        $this->response['status'] = 1;
        return response()->json($this->response, 200);
    }
}

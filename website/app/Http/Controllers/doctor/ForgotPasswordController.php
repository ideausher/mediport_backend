<?php

namespace App\Http\Controllers\doctor;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Validator;
use App\User;
use App\Mail\doctor\ForgetPassword;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\DB;

class ForgotPasswordController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Password Reset Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling password reset emails and
    | includes a trait which assists in sending these notifications from
    | your application to your users. Feel free to explore this trait.
    |
    */
    
    /**
     * Display the form to request a password reset link.
     *
     * @return \Illuminate\Http\Response
     */
    public function showLinkRequestForm()
    {

        return view('doctor.forgotPassword');
    }
    
      /**
     * Send a reset link to the given user.
     */
    
    public function sendResetLinkEmail(Request $request)
    {
        
        $data = $request->all();

        $user=User::where('email',$data['email'])->firstOrFail();

        $validator = Validator::make($data, ['email' => 'required|string|email|max:255']);

        if ($validator->fails()) {
           return redirect()->back()->withErrors($validator)->withInput();
        }

        $user->password_otp = $this->generateToken(10);

        $user->save();

        Mail::to($user->email)->send(new ForgetPassword($user));
        
        return redirect('doctor')->with('success_message', trans('doctor/password.mail_sent_message'));

    }

    private function generateToken($length=10){

        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';

        $charactersLength = strlen($characters);

        $randomString = '';

        for ($i = 0; $i < $length; $i++) {

            $randomString .= $characters[rand(0, $charactersLength - 1)];

        }

        return $randomString;
    }
}

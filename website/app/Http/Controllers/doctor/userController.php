<?php

namespace App\Http\Controllers\doctor;
use Spatie\Geocoder\Facades\Geocoder;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Helpers\Datatable\SSP;
use App\User;
use App\BookingDetail;
use App\UserAddresses;
use App\Booking;
use Auth;
use File;
use Validator;
use App\vendorEducation;
use App\VenderService;
class userController extends Controller
{
     protected $user;
    protected $pageLimit;
    
     public function __construct(User $user) {
        $this->user = $user;
        $this->pageLimit = config('settings.pageLimit');
    }

    /**
     * Display a listing of user
     *
     * @return Response
     */
    public function index() {
        // Show the page
        $user_id = Auth::guard('doctor')->user()->id;
       // $user_id=62;

          $data=BookingDetail::select('bookings_details.*','bookings.vender_id')
                   ->where('bookings.vender_id',$user_id)
                   ->join('bookings','bookings_details.booking_id','=','bookings.id')
                   ->get();

        return view('doctor/patientView',compact('data'));
    
    //
}
public function update(Request $request, $id) {

        $user = User::findOrFail($id);

        $data = $request->all();

        $validator = Validator::make($data, [
                    'firstname' => 'required|string|max:45',
                    'lastname' => 'required',
                    'email' => 'required|string|email|max:255',
                    'phone_number' => 'required',
                    'gender' => 'required|in:male,female',
                    'address' => 'required',
                    'experience' => 'required',
                    'specialization' => 'required',
                    'city' => 'required',
                    'state' => 'required',
                    'country' => 'required',
                    'postal_code' => 'required',
                    'price' => 'required'
        ]);

        if ($validator->fails()) {
			
            return redirect()->back()->withErrors($validator)->withInput();
        }
        if ($request->hasFile('image')) { //check the file present or not
            $rules = array('image' => 'mimes:jpeg,png,jpg,gif,svg||max:4096');
            $validator = Validator::make($data, $rules);
            if ($validator->fails()) {
				
                return redirect()->back()->withErrors($validator)->withInput();
            }
            $image = $request->file('image'); //get the file
            $data['image'] = md5(uniqid()) . '.' . $image->getClientOriginalExtension(); //get the  file extention
            $destinationPath = public_path('/images/avatars/'); //public path folder dir

            if (!$image->move($destinationPath, $data['image'])) {
                echo "image not uploaded correclty! Try Later";
                die;
            }
        }

        $user->update($data);

//        $geo_locations = Geocoder::getCoordinatesForAddress($data['address']);
//
//        if (isset($geo_locations) && ($geo_locations['lat'] == 0 || $geo_locations['lng'] == 0)) {
//
//            return redirect()->back()->withErrors(['msg', 'Invalid address'])->withInput();
//        }

        $userAddress = UserAddresses::where('user_id', $id)->first();
		
        if ($userAddress) {
            $userAddress->update([
                'place_id' => (isset($geo_locations['place_id']) && $geo_locations['place_id']) ? $geo_locations['place_id'] : '',
                'latitude' => (isset($geo_locations['lat']) && $geo_locations['lat']) ? $geo_locations['lat'] : '',
                'longitude' => (isset($geo_locations['lng']) && $geo_locations['lng']) ? $geo_locations['lng'] : '',
                'gender' => $data['gender'],
                'city' => $data['city'],
                'state' => $data['state'],
                'country' => $data['country'],
                'pincode' => $data['postal_code'],
                'full_address' => $data['address'],
            ]);
        }

         vendorEducation::where('user_id', $id)->delete();

        if (isset($data['degree']) && isset($data['batch']) && isset($data['edu_desc'])) {

            $edu_details = [];

            for ($i = 0; $i <= (count($data['degree']) - 1); $i++) {

                vendorEducation::create([
                    'user_id' => $user->id,
                    'degree' => $data['degree'][$i],
                    'batch' => $data['batch'][$i],
                    'edu_desc' => $data['edu_desc'][$i],
                ]);
            }
        }

        if (isset($data['specialization']) && $data['specialization']) {

            $vendor_service = VenderService::where('vender_id', $id)->firstOrFail();
            $vendor_service->update([
                'cat_id' => $data['specialization'],
                'experience' => $data['experience'],
                'price' => $data['price']
            ]);
        }

       
        return redirect('doctor/dashboard/usd')->with('success_message', trans('admin/user.user_update_message'));
    }

}
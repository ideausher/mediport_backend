<?php

namespace App\Http\Controllers\doctor;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use App\Transaction;
use App\Booking;
use DB;
use Carbon\Carbon;

class DashboardController extends Controller {

    public function __construct() {
        
    }
    
    /**
     * Admin dashboard
     *
     */

    public function index(Request $request) {
        

        $userId = auth()->guard('doctor')->user()->id;
        $curreny = $request['currency'];
        $amount = Transaction::select(
            DB::raw('sum(amount) as sums'), 
            DB::raw("DATE_FORMAT(created_at,'%m') as monthKey")
        )
        ->whereYear('created_at', date('Y'))
        ->where('vender_id',$userId)
        ->where('currency',$curreny)
        ->groupBy('monthKey')
        ->orderBy('created_at', 'ASC')
        ->get();
        
        $datagraph = [0,0,0,0,0,0,0,0,0,0,0,0];
        
        foreach($amount as $amo){
            $datagraph[$amo->monthKey-1] = $amo->sums;
        }

        $today = Booking::whereDate('booking_date', Carbon::today())
                         ->where('vender_id',$userId)
                         ->limit(10)
                         ->get();

        $tomorrow = Booking::whereDate('booking_date', Carbon::tomorrow())
                            ->where('vender_id',$userId)
                            ->limit(10)
                            ->get();

        $week = Booking::whereBetween('booking_date',[Carbon::now()->addDays(2)->format('Y-m-d'),Carbon::now()->endOfWeek()->format('Y-m-d')])
                         ->where('vender_id',$userId)
                         ->limit(10)
                         ->get();

        $monthTotal = Transaction::where('currency',$curreny)
                                  ->where('vender_id',$userId)
                                  ->whereMonth('created_at', Carbon::now()->month)
                                  ->sum('amount');

        $weekTotal = Transaction::where('currency',$curreny)
                                 ->where('vender_id',$userId)
                                 ->where('created_at', '>', Carbon::now()->startOfWeek())
                                 ->where('created_at', '<', Carbon::now()->endOfWeek())
                                 ->sum('amount');

        $dayTotal = Transaction::where('currency',$curreny)
                                ->where('vender_id',$userId)
                                ->whereMonth('created_at', Carbon::now()->today())
                                ->sum('amount');

        $dataCurreny = Transaction::where('vender_id',$userId)
                                    ->groupBy('currency')
                                    ->pluck('currency');
    
        return view('doctor/dashboard',compact('datagraph','today','tomorrow','week','curreny','monthTotal','weekTotal','dayTotal','dataCurreny'));
    }

}

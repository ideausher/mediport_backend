<?php

namespace App\Http\Controllers\doctor;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use Validator;

class UpdatePasswordController extends Controller {

    public function update(Request $request) {

        $data['reset_token'] = ($request->get('reset_token')) ? $request->get('reset_token') : '';

        $data['user_id'] = ($request->get('user_id')) ? $request->get('user_id') : '';

        $user = User::find($data['user_id']);
 
        if (($data['reset_token'] == '' || $data['user_id'] == '') || $user->password_otp != $data['reset_token']) {

            return redirect('doctor')->withErrors(['Invalid reset link']);
        }

        return view('doctor.updatePassword', compact('data'));
    }

    public function updatePassword(Request $request) {

        $data = $request->all();
        $validator = Validator::make($data, [
                    'update_password' => 'required|string|min:6|same:confirm_password',
                    'user_id' => 'required',
        ]);
        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator)->withInput();
        }
        $user = User::find($data['user_id']);
        $user->password = $data['update_password'];
        $user->password_otp = '';
        $user->save();

        return redirect('doctor')->with('success_message', trans('doctor/password.updated_password_message'));
    }

}

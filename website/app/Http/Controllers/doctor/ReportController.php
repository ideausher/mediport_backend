<?php

namespace App\Http\Controllers\doctor;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use PDF;
use Auth;
use Carbon\Carbon;
use App\Booking;
use Session;
use App\UserAddresses;
use App\Http\Requests\ReportPdf;
use App\Http\Requests\EditSaveReport;
use App\BookingDoctorReport;
use Illuminate\Support\Facades\Crypt;
use App\User;
use App\DeviceDetails;
use App\Notification;

class ReportController extends Controller
{

    public function __construct() {
        
    }

    /**
     * Function to show appointment report form
     * @param
     * @return View
     * @author Durga Parshad
     * @since 19-December-2019
     */
    public function index($id){

        $booking = Booking::find($id);
        $booking_id = $id;
        $username = $booking->bookingDetail->patient_name;
        $age = $booking->bookingDetail->age;
        $gender = $booking->bookingDetail->gender;
        $reference_id = $booking->reference_id;
        
        return view('doctor.appointmentreport',compact( 'username','reference_id', 'booking_id','age', 'gender'));
    }

    /**
     * Function to generate PDF File
     * @param 'id'
     * @return PDF File
     * @author Durga Parshad
     * @since 27-December-2019
     */
    public function view(Request $request,$id){

        try{
            
            $bookingDoctorReport = BookingDoctorReport::where('booking_id',$id)
                                            ->first();
            $booking = Booking::find($bookingDoctorReport['booking_id']);

            $address = $booking->vendor->userAddress->full_address . ' , '.$booking->vendor->userAddress->city . ' , '.$booking->vendor->userAddress->state . ' , '.$booking->vendor->userAddress->country . ' , '.
            $booking->vendor->userAddress->pincode;

            $pdfData = [
                'name' => $booking->bookingDetail->patient_name,
                'gender' => $booking->bookingDetail->gender == 1 ? 'Male' : 'Female',
                'age' => $booking->bookingDetail->age,
                'reference_id' => $booking->reference_id,
                'doctor_name' => $booking->vendor->firstname,
                'doctor_category' => $booking->service->servicecategory->cat_name,
                'issue' => $bookingDoctorReport['data']->issue,
                'report' => $bookingDoctorReport['data']->report,
                'address' => $address,
                'date' => $bookingDoctorReport->created_at->toDateTimeString()
            ];
            
            $pdf = PDF::setOptions(['isHtml5ParserEnabled' => true, 'isRemoteEnabled' => true])->loadView('doctor.reportpdf',$pdfData);
        
            $pdfName = md5(md5($pdfData['date']). md5(Carbon::now()->toString())) . '.pdf';
           
            return $pdf->stream($pdfName);
            
        }
        catch (\Exception $ex) {
            return view('errors/500');
        }

    }

    /**
     * Function to Edit pdf details edit
     * @param 'id'
     * @return View
     * @author Durga Parshad
     * @since 27-December-2019
     */
    public function edit(Request $request,$id){

        try{
            
            $bookingDoctorReport = BookingDoctorReport::where('booking_id',$id)
                                            ->first();
            $booking = Booking::find($id);
            $pdfData = [
                'name' => $booking->bookingDetail->patient_name,
                'gender' => $booking->bookingDetail->gender == 1 ? 'Male' : 'Female',
                'age' => $booking->bookingDetail->age,
                'reference_id' => $booking->reference_id,
                'issue' => $bookingDoctorReport['data']->issue,
                'report' => $bookingDoctorReport['data']->report,
                'booking_id' => $id
            ];
                                
            return view('doctor.editreport',$pdfData);
        }
        catch (\Exception $ex) {
            return view('errors/500');
        }

    }

    /**
     * Function to Edit pdf details edit
     * @param 'id'
     * @return View
     * @author Durga Parshad
     * @since 27-December-2019
     */
    public function editSave(EditSaveReport $request){

        try{
            
            $data = $request->all();
            $bookingReport = BookingDoctorReport::where('booking_id',$data['booking_id'])
                                ->first();

            $booking = Booking::find($data['booking_id']);
            $storeData = [
                'issue' => $request['issue'],
                'report' => $request['report']
            ];

            $data = [
                'name' => $booking->bookingDetail->patient_name,
                'gender' => $booking->bookingDetail->gender == 1 ? 'Male' : 'Female',
                'age' => $booking->bookingDetail->age,
                'reference_id' => $booking->reference_id,
                'doctor_name' => $booking->vendor->firstname,
                'doctor_category' => $booking->service->servicecategory->cat_name,
                'issue' => $request['issue'],
                'report' => $request['report'],
            ];

            $dataString = json_encode($storeData,true);
            $encrypted = Crypt::encryptString($dataString);

            $bookingReport->data = $encrypted;
            $bookingReport->save();
            $user = User::find($booking['user_id']);
            $title = 'Report has been edited by doctor.';
            $message = 'Report has been edited by doctor '. $user->firstname.'.';
            $data['user_name'] = $user->firstname;
            $dataPush = $data;
            $downstreamResponse = DeviceDetails::sendNotification($title ,$message,$booking['user_id'],$dataPush);

            Notification::create([
                'user_id' => $user->id, 
                'vender_id' => auth()->guard('doctor')->user()->id,
                'type' => 1,
                'title' => $title,
                'message' => $encrypted
            ]);

            Session::flash('message', trans('doctor/report.edit_success')); 
            Session::flash('alert-class', 'alert-success'); 
            return back();

        }
        catch (\Exception $ex) {
            return view('errors/500');
        }

    }

     /**
     * Function to generate PDF File
     * @param 'name','issue','report'
     * @return PDF File
     * @author Durga Parshad
     * @since 19-December-2019
     */
    public function generate(ReportPdf $request){

        try{

            $user = auth()->guard('doctor')->user()->id;
            $data = $request->all();
            $booking = Booking::find($data['booking_id']);
            $address = $booking->vendor->userAddress->full_address . ' , '.$booking->vendor->userAddress->city . ' , '.$booking->vendor->userAddress->state . ' , '.$booking->vendor->userAddress->country . ' , '.
            $booking->vendor->userAddress->pincode;
            $pdfData = [
                'name' => $booking->bookingDetail->patient_name,
                'gender' => $booking->bookingDetail->gender == 1 ? 'Male' : 'Female',
                'age' => $booking->bookingDetail->age,
                'reference_id' => $booking->reference_id,
                'doctor_name' => $booking->vendor->firstname,
                'doctor_category' => $booking->service->servicecategory->cat_name,
                'issue' => $request['issue'],
                'report' => $request['report'],
                'address' => $address
            ];

            $storeData = [
                'issue' => $request['issue'],
                'report' => $request['report']
            ];

            $dataString = json_encode($storeData,true);
            $encrypted = Crypt::encryptString($dataString);
            $bookingDoctorReport = BookingDoctorReport::create([
                'data' => $encrypted,
                'booking_id' => $data['booking_id']
            ]);

            $pdfData['date'] = $bookingDoctorReport->created_at->toDateTimeString();
            $booking->status = '3';
            $booking->save();

            $userDetails = User::find($booking['user_id']);

            $title = 'Report has been added by doctor.';
            $message = 'Report has been added by doctor '. $booking->bookingDetail->patient_name.'.';
            $data['user_name'] = $booking->bookingDetail->patient_name;
            $dataPush = $pdfData;
            $downstreamResponse = DeviceDetails::sendNotification($title ,$message,$booking['user_id'],$dataPush);

            Notification::create([
                'user_id' => $userDetails->id, 
                'vender_id' => $user,
                'type' => 2,
                'title' => $title,
                'message' => $encrypted
            ]);


            $pdf = PDF::setOptions(['isHtml5ParserEnabled' => true, 'isRemoteEnabled' => true])->loadView('doctor.reportpdf', $pdfData);
            $pdfName = md5(md5($user) . md5($pdfData['date']). md5(Carbon::now()->toString())) . '.pdf';
                
       
            return $pdf->stream($pdfName);
        }
        catch (\Exception $ex) {
            return view('errors/500');
        }

    }
    
    
}

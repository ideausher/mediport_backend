<?php

namespace App\Http\Controllers\doctor;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\venderSlot;
use App\VendorSlotDayBreak;
use App\VendorSlotDateBreak;
use Validator;
use App\Helpers\Datatable\SSP;
use App\User;
use Auth;
use Response;

class SlotController extends Controller {

    public function __construct() {
        $this->middleware('auth.doctor');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {

        $user_id = Auth::guard('doctor')->user()->id;

        $slots = venderSlot::where('vender_id', $user_id)->get();
        
        $dayBreaks = VendorSlotDayBreak::where('vendor_id', $user_id)->pluck('day')->toArray();
   
        $values = $this->getSortedValues($slots);

        return view('doctor.slotsList', compact('values', 'dayBreaks'));
    }

    public function dateBreak() {

        $user_id = Auth::guard('doctor')->user()->id;

        return view('doctor.datebreak');
    }

    public function getSlots(Request $request) {
        $data = $request->all();

        $user_id = Auth::guard('doctor')->user()->id;

        $rules = array(
            'date' => 'required',
        );

        $validator = Validator::make($data, $rules);
        if ($validator->fails()) {
            return Response::json(array(
                        'success' => false,
                        'errors' => $validator->getMessageBag()->toArray()
                            ), 400);
        }
        $specificDate = date('N', strtotime($data['date']));

        $venderSlots = venderSlot::where('vender_id', $user_id)->where('day', $specificDate)->get();

        $html = view('doctor.slotsListview', compact('venderSlots'))->render();

        return Response::json(array(
                    'success' => true,
                    'html' => $html));
    }

    public function SaveDateBreaks(Request $request) {
        $data = $request->all();

        $user_id = Auth::guard('doctor')->user()->id;

        $rules = array(
            'date' => 'required',
        );

        $validator = Validator::make($data, $rules);
        if ($validator->fails()) {
            return Response::json(array(
                        'success' => false,
                        'errors' => $validator->getMessageBag()->toArray()
                            ), 400);
        }

        $exist_array = VendorSlotDateBreak::where('vedor_id', $user_id)->where('date', date('Y-m-d', strtotime($data['date'])))->pluck('vendor_slot_id')->toArray();

        foreach ($data['slots'] as $value) {
            if (!in_array($value, $exist_array)) {
                VendorSlotDateBreak::insert([
                    'vedor_id' => $user_id,
                    'vendor_slot_id' => $value,
                    'date' => date('Y-m-d', strtotime($data['date'])),
                ]);
            }
        }
        return Response::json(array(
                    'success' => true));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
        $data = $request->all();

        $user_id = Auth::guard('doctor')->user()->id;

        $rules = array(
            'day' => 'required',
            'slot_from' => 'required',
            'slot_to' => 'required',
            'max_patient' => 'required|not_in:0'
        );

        $validator = Validator::make($data, $rules);
        if ($validator->fails()) {
            return Response::json(array(
                        'success' => false,
                        'errors' => $validator->getMessageBag()->toArray()
                            ), 400);
        }
        $slot = venderSlot::create([
                    'vender_id' => $user_id,
                    'day' => $data['day'],
                    'start_time' => $data['slot_from'],
                    'end_time' => $data['slot_to'],
                    'max_patient' => $data['max_patient'],
                    'is_active' => '1'
        ]);
        $lastInsertId = $slot->id;

        if ($lastInsertId) {
            return \Response::json(array('result' => 'success'));
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id) {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request) {

        $user_id = Auth::guard('doctor')->user()->id;

        $data = $request->all();

        $rules = array(
            'day' => 'required',
            'slot_from' => 'required',
            'slot_to' => 'required',
            'max_patient' => 'required',
        );
        $validator = Validator::make($data, $rules);
        if ($validator->fails()) {

            return Response::json(array(
                        'success' => false,
                        'errors' => $validator->getMessageBag()->toArray()
                            ), 400);
        }

        venderSlot::where('vender_id', $user_id)
                ->where('day', $data['day'])
                ->where('id', $data['slot_id'])
                ->update(['start_time' => $data['slot_from'], 'end_time' => $data['slot_to']]);
        return \Response::json(array('result' => 'success'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {
        venderSlot::destroy($id);

        $array = array();
        $array['success'] = true;
        $array['message'] = trans('doctor/slot.slot_delete_message');
        VendorSlotDateBreak::where('vendor_slot_id',$id)->delete();
        return \Response::json($array);
    }

    public function insertVenderBreak(Request $request) {
        $user_id = Auth::guard('doctor')->user()->id;
        $data = $request->all();
        if (($data['check'] == 1)) {
            VendorSlotDayBreak::where('vendor_id', $user_id)->where('day', $data['day'])->delete();
            venderSlot::where('vender_id', $user_id)->where('day', $data['day'])->update(array('is_active' => '1'));
            
            return \Response::json(array('result' => 'enabled'));
        } else {
            VendorSlotDayBreak::insert(['vendor_id' => $user_id, 'day' => $data['day']]);
            venderSlot::where('vender_id', $user_id)->where('day', $data['day'])->update(array('is_active' => '0')); 
            return \Response::json(array('result' => 'disabled'));
        }
    }

    public function checkOverlappingSlots(Request $request) {

        $user_id = Auth::guard('doctor')->user()->id;

        $data = $request->all();

        if (isset($data['slot_id'])) {
            //case 1 Example: existing slot 5:00 - 6:00, new slot 4:30 - 5:30  //Overlapping from left side of existing slot
            $overlap_slots = venderSlot::where('day', $data['day'])->where('start_time', '>', $data['slot_from'])->where('start_time', '<', $data['slot_to'])->where('end_time', '>', $data['slot_from'])->where('end_time', '>', $data['slot_to'])->where('id', '<>', $data['slot_id'])->where('vender_id', $user_id)
                    //case 2 Example: existing slot 5:00 - 6:00, new slot 5:30 - 6:30  //Overlapping from right side of existing slot
                    ->orWhere('day', $data['day'])->where('start_time', '<', $data['slot_from'])->where('start_time', '<', $data['slot_to'])->where('end_time', '>', $data['slot_from'])->where('end_time', '<', $data['slot_to'])->where('id', '<>', $data['slot_id'])->where('vender_id', $user_id)
                    //case 3 Example: existing slot 5:00 - 6:00, new slot 4:30 - 6:30  //Existing slot within new slot
                    ->orWhere('day', $data['day'])->where('start_time', '>', $data['slot_from'])->where('start_time', '<', $data['slot_to'])->where('end_time', '>', $data['slot_from'])->where('end_time', '<', $data['slot_to'])->where('id', '<>', $data['slot_id'])->where('vender_id', $user_id)
                    //case 4 Example: existing slot 5:00 - 6:00, new slot 5:15 - 5:45  //New Slot within existing slot
                    ->orWhere('day', $data['day'])->where('start_time', '<', $data['slot_from'])->where('start_time', '<', $data['slot_to'])->where('end_time', '>', $data['slot_from'])->where('end_time', '>', $data['slot_to'])->where('id', '<>', $data['slot_id'])->where('vender_id', $user_id)
                    //case 5 Example: existing slot 5:00 - 6:00, new slot 5:00 - 7:00  //Start Time same, end time of new slot greater than existing slot
                    ->orWhere('day', $data['day'])->where('start_time', $data['slot_from'])->where('start_time', '<', $data['slot_to'])->where('end_time', '>', $data['slot_from'])->where('end_time', '<', $data['slot_to'])->where('id', '<>', $data['slot_id'])->where('vender_id', $user_id)
                    //case 6 Example: existing slot 5:00 - 6:00, new slot 5:00 - 5:30  //Start time same, end time of new slot smaller than existing slot
                    ->orWhere('day', $data['day'])->where('start_time', $data['slot_from'])->where('start_time', '<', $data['slot_to'])->where('end_time', '>', $data['slot_from'])->where('end_time', '>', $data['slot_to'])->where('id', '<>', $data['slot_id'])->where('vender_id', $user_id)
                    //case 7 Example: existing slot 5:00 - 6:00, new slot 4:30 - 6:00  //End time same, start time of new slot smaller than existing slot
                    ->orWhere('day', $data['day'])->where('start_time', '>', $data['slot_from'])->where('start_time', '<', $data['slot_to'])->where('end_time', '>', $data['slot_from'])->where('end_time', $data['slot_to'])->where('id', '<>', $data['slot_id'])->where('vender_id', $user_id)
                    //case 8 Example: existing slot 5:00 - 6:00, new slot 5:30 - 6:00  //End time same, start time of new slot greater than existing slot
                    ->orWhere('day', $data['day'])->where('start_time', '<', $data['slot_from'])->where('start_time', '<', $data['slot_to'])->where('end_time', '>', $data['slot_from'])->where('end_time', $data['slot_to'])->where('id', '<>', $data['slot_id'])->where('vender_id', $user_id)
                    ->count();
        } else {
            $overlap_slots = venderSlot::where('day', $data['day'])->where('start_time', '>', $data['slot_from'])->where('start_time', '<', $data['slot_to'])->where('end_time', '>', $data['slot_from'])->where('end_time', '>', $data['slot_to'])->where('vender_id', $user_id)
                    ->orWhere('day', $data['day'])->where('start_time', '<', $data['slot_from'])->where('start_time', '<', $data['slot_to'])->where('end_time', '>', $data['slot_from'])->where('end_time', '<', $data['slot_to'])->where('vender_id', $user_id)
                    ->orWhere('day', $data['day'])->where('start_time', '>', $data['slot_from'])->where('start_time', '<', $data['slot_to'])->where('end_time', '>', $data['slot_from'])->where('end_time', '<', $data['slot_to'])->where('vender_id', $user_id)
                    ->orWhere('day', $data['day'])->where('start_time', '<', $data['slot_from'])->where('start_time', '<', $data['slot_to'])->where('end_time', '>', $data['slot_from'])->where('end_time', '>', $data['slot_to'])->where('vender_id', $user_id)
                    ->orWhere('day', $data['day'])->where('start_time', $data['slot_from'])->where('start_time', '<', $data['slot_to'])->where('end_time', '>', $data['slot_from'])->where('end_time', '<', $data['slot_to'])->where('vender_id', $user_id)
                    ->orWhere('day', $data['day'])->where('start_time', $data['slot_from'])->where('start_time', '<', $data['slot_to'])->where('end_time', '>', $data['slot_from'])->where('end_time', '>', $data['slot_to'])->where('vender_id', $user_id)
                    ->orWhere('day', $data['day'])->where('start_time', '>', $data['slot_from'])->where('start_time', '<', $data['slot_to'])->where('end_time', '>', $data['slot_from'])->where('end_time', $data['slot_to'])->where('vender_id', $user_id)
                    ->orWhere('day', $data['day'])->where('start_time', '<', $data['slot_from'])->where('start_time', '<', $data['slot_to'])->where('end_time', '>', $data['slot_from'])->where('end_time', $data['slot_to'])->where('vender_id', $user_id)
                    ->count();
        }

        if ($overlap_slots) {
            $array['overlap_slots_exists'] = true;
        } else {
            $array['overlap_slots_exists'] = false;
        }

        $same_slot = venderSlot::where('day', $data['day'])->where('start_time', $data['slot_from'])->where('end_time', $data['slot_to'])->where('vender_id',$user_id)->count();
        if ($same_slot) {
            $array['same_slot_exists'] = true;
        } else {
            $array['same_slot_exists'] = false;
        }

        echo json_encode($array);
    }

    private function getSortedValues($slots) {


        $sorted_data = [];

        $days = ['Monday' => '1', 'Tuesday' => '2', 'Wednesday' => '3', 'Thursday' => '4', 'Friday' => '5', 'Saturday' => '6', 'Sunday' => '7'];

        if (!empty($slots) && $slots) {

            foreach ($slots as $key => $slot) {

                $sorted_data[array_search($slot->day, $days)]['data'][] = $slot;
            }
        }

        return array_merge($days, $sorted_data);
    }

    public function getSlotsData() {

        $user_id = Auth::guard('doctor')->user()->id;

        $table = 'vendor_slots_date_breaks';

        $primaryKey = 'id';

        $columns = array(
            array('db' => 'date', 'dt' => 0, 'field' => 'date'),
            array('db' => 'date', 'dt' => 1, 'field' => 'date', 'formatter' => function( $d, $row ) {
                    return date('l', strtotime($d));
                }),
            array('db' => 'GROUP_CONCAT(CONCAT(start_time,"-",end_time)) as slots', 'dt' => 2, 'formatter' => function( $d, $row ) {

                    $slots = explode(",", $d);
                    $html = '';
                    foreach ($slots as $slot) {
                        $time = explode("-", $slot);
                        $start_time = date('H:i', strtotime($time[0]));
                        $end_time = date('H:i', strtotime($time[1]));
                        $html.= '<span class="span-tab"  data-id="" data-target="#exampleModalCenter"><span id="start_time">' . $start_time . '</span>-<span id="end_time">' . $end_time . '</span></span>';
                    }
                    return $html;
                }, 'field' => 'slots'),
            array('db' => 'date', 'dt' => 3, 'formatter' => function( $d, $row ) {

                    return $operation = '<a href="javascript:;" data-date="' . $d . '" class="btn btn-danger delete-btn" title="' . trans('admin/common.delete') . '" data-toggle="tooltip"><i class="fa fa-times"></i></a>';
                }, 'field' => 'date')
        );

        $sql_details = array(
            'user' => config('database.connections.mysql.username'),
            'pass' => config('database.connections.mysql.password'),
            'db' => config('database.connections.mysql.database'),
            'host' => config('database.connections.mysql.host')
        );

        $joinQuery = '';
        $joinQuery .= " LEFT JOIN vender_slots vs ON vs.id = vendor_slots_date_breaks.vendor_slot_id";
        $extraWhere = "vs.vender_id=" . $user_id;
        $groupBy = "vendor_slots_date_breaks.date";



        echo json_encode(
                SSP::simple($_GET, $sql_details, $table, $primaryKey, $columns, $joinQuery, $extraWhere, $groupBy)
        );
    }

    public function deleteBreak(Request $request) {
    
        
        $data = $request->all();

        $user_id = Auth::guard('doctor')->user()->id;

        $rules = array(
            'date' => 'required',
        );

        $validator = Validator::make($data, $rules);
        if ($validator->fails()) {
            return Response::json(array(
                        'success' => false,
                        'errors' => $validator->getMessageBag()->toArray()
                            ), 400);
            
         
        }

        VendorSlotDateBreak::where('vedor_id', $user_id)->where('date', $data['date'])->delete();
        
        
        return \Response::json(array('success' => true,'message'=>trans('doctor/slot.break_delete_message')));

        


    }

}

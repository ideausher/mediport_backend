<?php

namespace App\Http\Controllers\Frontend;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\User;
use Validator;
use App\Models\Notification;
use App\Enquiry as Enquiry;
use Illuminate\Support\Facades\Mail;
use App\Mail\Frontend\EnquiryMail;
use App\Mail\Admin\NotificationEmail;
use App\Jobs\SendEmailJob;

class HomeController extends Controller {
    
    public function __construct() {
        $this->middleware('guest.user');
    }

    public function index() {
        if(config('app.locale')=='en'){
            return view('frontend.index');
        }elseif (config('app.locale')=='es') {
            return view('frontend.index_es');
        }else{
            return view('frontend.index');
        }
    }
    
    public function submitEnquiry(Request $request) {
        $data = $request->all();
        
        $rules = array(
            'fullname' => 'required',
            'email' => 'required|email',
            'subject' => 'required',
            'message' => 'required'
        );
       
        $validator = Validator::make($data, $rules);
        if ($validator->fails()) {
            return response()->json($validator->errors()->all());
        }
        
        $enquiry = new Enquiry;
        $enquiry->fullname = $data['fullname'];
        $enquiry->email = $data['email'];
        $enquiry->subject = $data['subject'];
        $enquiry->message = $data['message'];
        $enquiry->save();

        //send enquiry mail to admin
        Mail::to(config('settings.admin.email'))->send(new EnquiryMail($enquiry));

        $array = array();
        $array['success'] = true;
        if(config('app.locale')=='en'){
            $array['message'] = 'Your message has been submitted successfully!';
        }elseif (config('app.locale')=='es') {
            $array['message'] = 'Su mensaje ha sido enviado exitosamente!';
        }else{
            $array['message'] = 'Your message has been submitted successfully!';
        }
        return response()->json($array);
    }
    
    public function submitNotification(Request $request) {
        
        echo 'Hello world';
        exit;
        $data = $request->all();
        
       $rules = array(
           'fullname' => 'required',
           'email' => 'required|email',
            'subject' => 'required',
            'message' => 'required'
        );
        $user=User::find($data['id']);
        foreach($user as $user1)
        {
           
            $data['email']= $user1->email;
            $data['fullname']=$user1->firstname;
            $data['user_id']=$user1->id;

        $validator = Validator::make($data, $rules);
        if ($validator->fails()) {
            return response()->json($validator->errors()->all());
        }
        
        $enquiry = new Notification;
        $enquiry->user_id = $data['user_id'];
        $enquiry->vender_id = $data['user_id'];
        $enquiry->title = $data['subject'];
        $enquiry->message = $data['message'];
        $enquiry->save();

        //send enquiry mail to admin
        
        Mail::to('sandeep.intersoft1@gmail.com')->send(new NotificationEmail($enquiry));
        

         session()->flash('success_message', trans('admin/booking.booking_status_message'));
        $array = array();
        $array['success'] = true;
        $array['message'] = trans('admin/booking.booking_status_message');
        echo json_encode($array);
       /* $array = array();
        $array['success'] = true;
        if(config('app.locale')=='en'){
            $array['message'] = 'Your message has been submitted successfully!';
        }elseif (config('app.locale')=='es') {
            $array['message'] = 'Su mensaje ha sido enviado exitosamente!';
        }else{
            $array['message'] = 'Your message has been submitted successfully!';
        }
        */
        
    }
   
       
        return response()->json($array);
    }
}

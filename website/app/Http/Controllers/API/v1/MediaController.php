<?php

namespace App\Http\Controllers\API\v1;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;
use Storage;
use App\Media;
use App\Http\Requests\Media as MediaRequest;

class MediaController extends Controller
{

    private $image_ext = ['jpg', 'jpeg', 'png', 'gif'];
    //private $audio_ext = ['mp3', 'ogg', 'mpga'];
    //private $video_ext = ['mp4', 'mpeg'];
    private $document_ext = ['doc', 'docx', 'pdf', 'odt'];

    protected $response = [
        'status' => 0,
        'message' => '',
    ];

    public function __construct() {
        $this->response['data'] = new \stdClass();
    }

    protected function setData($key, $value)
    {
        array_walk_recursive($value, function (&$item, $key) {
            $item = null === $item ? '' : $item;
        });
        $this->response['data'] = $value;
        return $this->response['data'];
    }

    /**
     * @SWG\Post(
     *     path="/upload",
     *     tags={"Media"},
     *     summary="Upload Media file",
     *     description="Upload media file",
     *     operationId="uploadMedia",
     *     @SWG\Parameter(
     *         name="file",
     *         in="formData",
     *         type="file",
     *         description="Upload file"    
     *     ),
     *     @SWG\Parameter(
     *         name="Authorization",
     *         required = true,
     *         in="header",
     *         description="Authorization Token",
     *         type="string"
     *     ),
     *     @SWG\Response(response=200, description="Successful operation"),
     *     @SWG\Response(response=422, description="Validation Error and  Unprocessable Entity")*      ,
     *     @SWG\Response(response=401, description="Invalid Token"),
     *     @SWG\Response(response=500, description="Internal serve error")
     * )
     */

    public function uploadMedia(MediaRequest $request){

        try{
            
            $model = new Media();

            $file = $request->file('file');
            $ext = $file->getClientOriginalExtension();
            $type = $this->getType($ext);
            $name = md5(rand(1,rand(1,9999999))."uploadMedia");
            $directoryName = '/public/media/' . $this->getUserDir() . '/' . $type;
            $newImageName = $name . '.' . $ext;
           
            if (Storage::putFileAs($directoryName, $file, $newImageName)) {
                $data = $model::create([
                    'name' => $name,
                    'type' => $type,
                    'extension' => $ext,
                    'user_id' => Auth::id()
                ]);

                $this->response['status'] = 1;
                $this->response['message'] = trans('api/service.total_record');
                $this->setData("data", $data);
                return response()->json($this->response, 200);
                
            }

        } catch (\Exception $ex) {
            $this->response['message'] = trans('api/user.something_wrong');
            return response($this->response, 500);
        }
    }

    /**
     * Get directory for the specific user
     * @return string Specific user directory
     */
    private function getUserDir()
    {
        return Auth::id();
    }

    /**
     * Get type by extension
     * @param  string $ext Specific extension
     * @return string      Type
     */
    private function getType($ext)
    {
        if (in_array($ext, $this->image_ext)) {
            return 'image';
        }

        // if (in_array($ext, $this->audio_ext)) {
        //     return 'audio';
        // }

        // if (in_array($ext, $this->video_ext)) {
        //     return 'video';
        // }

        if (in_array($ext, $this->document_ext)) {
            return 'document';
        }
    }

}

<?php

namespace App\Http\Controllers\API\v1;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Service;
use App\ServiceCategory;
use App\slot;
use App\ExtraHour;
use App\User;
use App\BookingStatusHistory;
use App\venderSlot;
use App\UserAddresses;
use App\Booking;
use App\BookingDetail;
use App\Notification;
use App\Services\PushNotification;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;
use App\Http\Resources\Booking as BookingResource;
use App\Http\Resources\venderBookingList;
use App\Http\Resources\venderBookingListCollection;
use App\Http\Resources\BookingCollection;
use App\Http\Resources\Invoice as InvoiceResource;
use App\Http\Resources\NotificationCollection;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Carbon;
use App\Http\Resources\OrderCollection;
use App\Enquiry;
use App\Mail\EnquiryEmail;
use Illuminate\Support\Facades\Mail;

class NotificationController extends Controller {

    protected $response = [
        'status' => 0,
        'message' => '',
    ];

    public function __construct() {
        
        $this->response['data'] = new \stdClass();
    }
    
    protected function setData($complexObject)
    {
        
        $json = json_encode($complexObject);
        $encodedString = preg_replace('/null/', '" "' , $json);
        $this->response['data'] = json_decode($encodedString);
        return $this->response['data'];
    }

    /**
     * @SWG\Get(
     *     path="/notification",
     *     tags={"Notification"},
     *     summary="Get user notification list",
     *     description="Get user notification detail list",
     *     operationId="getNotification",
     *     @SWG\Parameter(
     *         name="Authorization",
     *         in="header",
     *         required = true,
     *         description="Authorization Token",
     *         type="string"
     *     ),
     *     @SWG\Parameter(
     *         name="page",
     *         required = true,
     *         in="query",
     *         description="pagination page number",
     *         type="integer"
     *     ),
     *     @SWG\Parameter(
     *         name="limit",
     *         required = true,
     *         in="query",
     *         description="Pagination number of records in list",
     *         type="integer"
     *     ),
     *     @SWG\Parameter(
     *         name="is_read",
     *         required = true,
     *         in="query",
     *         description="Message read or not read by user(1 = Read, 2 = Unread, 3 = all)",
     *         type="integer"
     *     ),
     *     @SWG\Response(response=200, description="Successful operation"),
     *     @SWG\Response(response=422, description="Validation Error and  Unprocessable Entity")*      ,
     *     @SWG\Response(response=401, description="Invalid Token"),
     *     @SWG\Response(response=500, description="Internal serve error")
     * )
     */
    public function getNotifications(Request $request) {
        try{
           
            $user = Auth::User();
            $query = Notification::with([
                                    'vendor' => function($query){
                                        $query->select(['id','image','firstname']);
                                    }
                                  ])
                                  ->where('user_id',$user->id);
                                        
            if($request['is_read'] != 3){
                $query->where('is_read',$request['is_read']);
            }
            
            $notifications = $query->offset(10*$request['page'])
                                    ->limit($request['limit'])
                                    ->orderBy('created_at', 'DESC')
                                    ->get();

            if (!$notifications) {
                $this->response['message'] = trans('api/service.no_notification');
                $this->response['data'] = array();
                $this->response['status'] = 0;
                return response()->json($this->response, 200);
            }
        
            $this->response['message'] = trans('api/service.notifications_found');
            $this->response['data'] = $notifications;
            $this->response['status'] = 1;
            return response()->json($this->response, 200);
        } catch (\Exception $ex) {
            $this->response['message'] = trans('api/user.something_wrong');
            return response($this->response, 500);
        }

        
        
    }

    /**
     * @SWG\Patch(
     *     path="/notification",
     *     tags={"Notification"},
     *     summary="Update User Notification Setting",
     *     description="Update User Notification Setting details using API's",
     *     operationId="userNotificationSetting",
     *     @SWG\Parameter(
     *         name="Authorization",
     *         required = true,
     *         in="header",
     *         description="Authorization Token",
     *         type="string"
     *     ),
     *     @SWG\Response(response=200, description="Successful operation"),
     *     @SWG\Response(response=422, description="Validation Error and  Unprocessable Entity")*      ,
     *     @SWG\Response(response=401, description="Invalid Token And Unauthenticated"),
     *     @SWG\Response(response=500, description="Internal serve error")
     * )
     */
    public function updateStatus(){
        
        try{
            $user = User::find(Auth::user()->id);
            if($user->is_notification == 1 ) {
                $user->is_notification = 2;
            }else{
                $user->is_notification = 1;
            }
            $user->save();
            $this->response['message'] = trans('api/message.notification_update');
            $this->setData($user);
            $this->response['status'] = 1;
            return response()->json($this->response, 200);
        }
        catch (\Exception $ex) {
            $this->response['message'] = trans('api/user.something_wrong');
            return response($this->response, 500);
        }
    }

}

<?php

namespace App\Http\Controllers\API\v1;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Setting;
use App\Http\Requests\TermsandCondition;

class SettingController extends Controller
{
    protected $response = [
        'status' => 0,
        'message' => '',
    ];

    public function __construct() {
        $this->response['data'] = new \stdClass();
    }

    protected function setData($data)
    {
        array_walk_recursive($value, function (&$item, $key) {
            $item = null === $item ? '' : $item;
        });
        $this->response['data'] = $value;
        return $this->response['data'];
    }

    /**
     * @SWG\Get(
     *     path="/term_condition",
     *     tags={"Terms And Condition"},
     *     summary="Get term and condition text",
     *     description="Get term and condition using API's",
     *     operationId="getTermCondition",
     *     @SWG\Parameter(
     *         name="Language",
     *         in="header",
     *         description="Language",
     *         type="string"
     *     ),
     *     @SWG\Response(response=200, description="Successful operation"),
     *     @SWG\Response(response=422, description="Validation Error and  Unprocessable Entity")*      ,
     *     @SWG\Response(response=401, description="Invalid Token And Unauthenticated"),
     *     @SWG\Response(response=500, description="Internal serve error")
     * )
     */
    public function termsandcondition(TermsandCondition $request){

        try {

            $setting = Setting::where('language',$request->header('language'))
                               ->pluck('term_condition');
                      
            $returnData = $setting[0] ? $setting[0] : '';

            if (!$returnData) {
                $this->response['status'] = 0;
                $this->response['message'] = trans('api/service.no_record_found');
                $this->response['data'] = $returnData;
                return response()->json($this->response, 200);
            }
            
            $this->response['status'] = 1;
            $this->response['message'] = trans('api/service.total_record');
            $this->response['data'] = $returnData;
            return response()->json($this->response, 200);
        } catch (\Exception $ex) {
            $this->response['message'] = trans('api/user.something_wrong');
            return response($this->response, 500);
        }
    }   

}

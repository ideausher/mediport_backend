<?php

namespace App\Http\Controllers\API\v1;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Service;
use App\slot;
use App\User;
use App\venderSlot;
use App\UserAddresses;
use App\ServiceCategory;
use App\Booking;
use App\Coupon;
use App\Review;
use App\Banner;
use App\VenderService;
use App\BookingDetail;
use App\Http\Resources\CategoryCollection;
use App\Http\Resources\ServiceCollection;
use App\Http\Resources\VenderServiceCollection;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;
use App\Http\Resources\Coupon as CouponResource;
use App\Http\Resources\BookingCollection;
use App\Http\Resources\Banner as BannerResource;
use App\Http\Resources\BannerCollection;
use App\Http\Requests\AllServices;
use App\Http\Requests\DocterProfile;
use Illuminate\Support\Facades\DB;

class ServicesController extends Controller {

    protected $response = [
        'status' => 0,
        'message' => '',
    ];

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    protected $service;
    protected $pageLimit;

    public function __construct(Service $service) {
        $this->service = $service;
        $this->pageLimit = config('settings.pageLimit');
        $this->response['data'] = new \stdClass();
    }

    protected function setData($complexObject)
    {
        $json = json_encode($complexObject);
        $encodedString = preg_replace('/null/', '" "' , $json);
        $this->response['data'] = json_decode($encodedString);
        return $this->response['data'];
    }

    /**
     * @SWG\Get(
     *     path="/category",
     *     tags={"Doctor"},
     *     summary="Get Category List",
     *     description="Get category list using API's",
     *     operationId="getCategory",
     *     @SWG\Parameter(
     *         name="Authorization",
     *         required = true,
     *         in="header",
     *         description="Authorization Token",
     *         type="string"
     *     ),
     *     @SWG\Response(response=200, description="Successful operation"),
     *     @SWG\Response(response=422, description="Validation Error and  Unprocessable Entity")*      ,
     *     @SWG\Response(response=401, description="Invalid Token And Unauthenticated"),
     *     @SWG\Response(response=500, description="Internal serve error")
     * )
     */

    public function index(Request $request) {

        $services = ServiceCategory::where(['status' => '1'])
                                    ->get();
        return (new CategoryCollection($services))->additional([
                    'status' => 1,
                    'message' => trans('api/service.total_record')
        ]);
    }

    /**
     * @SWG\Get(
     *     path="/getServices",
     *     tags={"Doctor"},
     *     summary="Get Docters list",
     *     description="Get Docter list using API's",
     *     operationId="getDocter",
     *     @SWG\Parameter(
     *         name="Authorization",
     *         required = true,
     *         in="header",
     *         description="Authorization Token",
     *         type="string"
     *     ),
     *     @SWG\Parameter(
     *         name="category",
     *         in="query",
     *         description="Category Id",
     *         type="integer"
     *     ),
     *     @SWG\Parameter(
     *         name="search",
     *         in="query",
     *         description="Search",
     *         type="string"
     *     ),
     *     @SWG\Parameter(
     *         name="min_price",
     *         in="query",
     *         description="Minimum Price",
     *         type="integer"
     *     ),
     *     @SWG\Parameter(
     *         name="max_price",
     *         in="query",
     *         description="Maximum Price",
     *         type="integer"
     *     ),
     *     @SWG\Parameter(
     *         name="min_experience",
     *         in="query",
     *         description="Minimum Experience",
     *         type="integer"
     *     ),
     *     @SWG\Parameter(
     *         name="max_experience",
     *         in="query",
     *         description="Maximum Experience",
     *         type="integer"
     *     ),
     *     @SWG\Parameter(
     *         name="avail_min",
     *         in="query",
     *         description="Minimum Availibity",
     *         type="string"
     *     ),
     *     @SWG\Parameter(
     *         name="avail_max",
     *         in="query",
     *         description="Maximum Availibity",
     *         type="string"
     *     ),
     *     @SWG\Parameter(
     *         name="min_review",
     *         in="query",
     *         description="Minimum Review",
     *         type="integer"
     *     ),
     *     @SWG\Parameter(
     *         name="max_review",
     *         in="query",
     *         description="Maximum Review",
     *         type="integer"
     *     ),
     *     @SWG\Parameter(
     *         name="page",
     *         required = true,
     *         in="query",
     *         description="Page",
     *         type="integer"
     *     ),
     *     @SWG\Parameter(
     *         name="limit",
     *         required = true,
     *         in="query",
     *         description="Limit",
     *         type="integer"
     *     ),
     *     @SWG\Parameter(
     *         name="latitude",
     *         required = true,
     *         in="query",
     *         description="Latitude",
     *         type="string"
     *     ),
     *     @SWG\Parameter(
     *         name="longitude",
     *         required = true,
     *         in="query",
     *         description="Longitude",
     *         type="string"
     *     ),
     *     @SWG\Parameter(
     *         name="distance",
     *         required = true,
     *         in="query",
     *         description="Distance",
     *         type="integer"
     *     ),
     *       @SWG\Parameter(
     *         name="language",
     *         required = true,
     *         in="query",
     *         description="1 => en, 2 => ar, 3 => es",
     *         type="integer"
     *     ),
     *     @SWG\Response(response=200, description="Successful operation"),
     *     @SWG\Response(response=422, description="Validation Error and  Unprocessable Entity")*      ,
     *     @SWG\Response(response=401, description="Invalid Token And Unauthenticated"),
     *     @SWG\Response(response=500, description="Internal serve error")
     * )
     */
    public function get_all_services(AllServices $request) {

        try {

            $language = $request->language;
            if($language == 'en') {
                $langInt = 1;
            }elseif($language == 'ar'){
                $langInt = 2;
            }else{
                $langInt = 3;
            }

            $this->response['data'] = array();
            $resultQuery = VenderService::select(['cat_id','vender_id','price'])
                                ->has('venderSlot')
                                ->whereHas('venderaddress' , function($query) use($request){
                                    $lat = $request['latitude'];
                                    $long = $request['longitude'];
                                    $query->select('full_address','pincode',DB::raw(" user_addresses.user_id, ( 6371 * acos( cos( radians($lat) ) * cos( radians( latitude ) ) * cos( radians( longitude ) - radians($long) ) + sin( radians($lat) ) * sin( radians( latitude ) ) ) ) AS distance "))
                                    ->havingRaw('distance <= '.$request['distance']);
                                })
                                ->whereHas('users' , function($query) use($request,$language){
                                    if($request['search']){
                                        $query->where('firstname', 'LIKE', '%'.$request['search'].'%');
                                    }
                                    $query->where('lang_type', $language);
                                    $query->where('status','=','1');
                                })
                                ->whereHas('users.userRole' , function($query) use($request){
                                    
                                    $query->where('role_id', 2);
                                    
                                })
                                ->with([
                                        'users' => function($query) use($request) {
                                            $query->select('id','firstname','image');
                                            
                                        },
                                        'reviews' => function($query) use($request) {
                                            $query->select('vender_id', DB::Raw('AVG(rating) as avg_rating'));
                                            if($request['min_review']){
                                                $query->havingRaw('AVG(rating) >= ?', [sprintf("%.2f", $request['min_review'])]);
                                            }

                                            if($request['max_review']){
                                                $query->havingRaw('AVG(rating) <= ?', [sprintf("%.2f", $request['min_review'])]);
                                            }

                                            $query->groupBy('vender_id');
                                        },
                                        'servicecategory' => function($query) use($request){
                                            $query->select('id','cat_name','cat_name_es','cat_name_ar','image');
                                            if($request['search']){
                                                $query->orWhere('cat_name', 'LIKE', '%'.$request['search'].'%');
                                            }
                                        },
                                        'venderaddress' => function($query) use($request){
                                            $lat = $request['latitude'];
                                            $long = $request['longitude'];
                                            $query->select('full_address','pincode',DB::raw(" user_addresses.user_id, ( 6371 * acos( cos( radians($lat) ) * cos( radians( latitude ) ) * cos( radians( longitude ) - radians($long) ) + sin( radians($lat) ) * sin( radians( latitude ) ) ) ) AS distance "));
                                        },
                                        'venderSlot' => function($query) use($request){
                                            
                                            $query->select(['id','vender_id','is_active']);

                                            if($request['avail_min']){
                                                $query->where('start_time','>=',$request['avail_min']);
                                            }

                                            if($request['avail_min']){
                                                $query->where('end_time','<=',$request['avail_max']);
                                            }
                                            $query->where('is_active','=','1');
                                        }
                                ]);
                                
            if($request['category']){
                $resultQuery->where('vender_services.cat_id', '=', $request['category']);
            }

            if($request['min_price']){
                $resultQuery->where('vender_services.price', '>=', $request['min_price']);
            }

            if($request['max_price']){
                $resultQuery->where('vender_services.price', '<=', $request['max_price']);
            }

            if($request['min_experience']){
                $resultQuery->where('vender_services.experience', '>=', $request['min_experience']);
            }  
            
            if($request['max_experience']){
                $resultQuery->where('vender_services.experience', '<=', $request['max_experience']);
            }  

            if($request['page'] ){
                $resultQuery->offset($request['limit']*$request['page']);
            }

            if($request['limit']){
                $resultQuery->limit($request['limit']);
            }
            
        
            $data = $resultQuery->get()
                                ->toArray(); 

            
                            
            $returnData = [];
            foreach($data as $key => $value){
                
                if($value['vender_slot'] != null){

                    if($value['servicecategory'] && $request->language == 2){
                        $categoryName = $value['servicecategory']['cat_name_ar'];
                    }
                    elseif($value['servicecategory'] && $request->language == 3){
                        $categoryName = $value['servicecategory']['cat_name_es'];
                    }else{
                        $categoryName = $value['servicecategory']['cat_name'];
                    }

                    $returnData[$key]['cat_id'] = $value['cat_id'];
                    $returnData[$key]['vender_id'] = $value['vender_id'];
                    $returnData[$key]['price'] = $value['price'];
                    $returnData[$key]['name'] = $value['users']['firstname'];
                    $returnData[$key]['profile_image'] = $value['users']['image'] ? $value['users']['image'] : '';
                    $returnData[$key]['avg_rating'] = $value['reviews'] ? $value['reviews'][0]['avg_rating'] : 0;
                    $returnData[$key]['cat_name'] = $categoryName;
                    $returnData[$key]['cat_image'] = $value['servicecategory'] ? $value['servicecategory']['image'] : '';
                    $returnData[$key]['venderaddress'] = $value['venderaddress'] ? round($value['venderaddress']['distance'],2) : 0;
                    $returnData[$key]['full_address'] = $value['venderaddress'] ? $value['venderaddress']['full_address'] : '';
                    $returnData[$key]['pincode'] = $value['venderaddress'] ? $value['venderaddress']['pincode'] : '';
                }
                
            }                    
            
            if (!$returnData) {
                $this->response['status'] = 0;
                $this->response['message'] = trans('api/service.no_record_found');
                $this->response['data'] = $returnData;
                return response()->json($this->response, 200);
            }
            
            $this->response['status'] = 1;
            $this->response['message'] = trans('api/service.total_record');
            $this->response['data'] = $returnData;
            return response()->json($this->response, 200);
        } catch (\Exception $ex) {
            return $ex;
            $this->response['message'] = trans('api/user.something_wrong');
            return response($this->response, 500);
        }
    }

    /**
     * @SWG\Get(
     *     path="/mydoctors",
     *     tags={"Doctor"},
     *     summary="Get My Docters list",
     *     description="Get My Docter list using API's",
     *     operationId="getMyDocter",
     *     @SWG\Parameter(
     *         name="Authorization",
     *         required = true,
     *         in="header",
     *         description="Authorization Token",
     *         type="string"
     *     ),
     *     @SWG\Parameter(
     *         name="category",
     *         in="query",
     *         description="Category Id",
     *         type="integer"
     *     ),
     *     @SWG\Parameter(
     *         name="search",
     *         in="query",
     *         description="Search",
     *         type="string"
     *     ),
     *     @SWG\Parameter(
     *         name="min_experience",
     *         in="query",
     *         description="Minimum Experience",
     *         type="integer"
     *     ),
     *     @SWG\Parameter(
     *         name="max_experience",
     *         in="query",
     *         description="Maximum Experience",
     *         type="integer"
     *     ),
     *     @SWG\Parameter(
     *         name="min_review",
     *         in="query",
     *         description="Minimum Review",
     *         type="integer"
     *     ),
     *     @SWG\Parameter(
     *         name="max_review",
     *         in="query",
     *         description="Maximum Review",
     *         type="integer"
     *     ),
     *     @SWG\Parameter(
     *         name="page",
     *         required = true,
     *         in="query",
     *         description="Page",
     *         type="integer"
     *     ),
     *     @SWG\Parameter(
     *         name="limit",
     *         required = true,
     *         in="query",
     *         description="Limit",
     *         type="integer"
     *     ),
     *     @SWG\Parameter(
     *         name="latitude",
     *         required = true,
     *         in="query",
     *         description="Latitude",
     *         type="string"
     *     ),
     *     @SWG\Parameter(
     *         name="longitude",
     *         required = true,
     *         in="query",
     *         description="Longitude",
     *         type="string"
     *     ),
     *     @SWG\Parameter(
     *         name="distance",
     *         required = true,
     *         in="query",
     *         description="Distance",
     *         type="integer"
     *     ),
     *     @SWG\Response(response=200, description="Successful operation"),
     *     @SWG\Response(response=422, description="Validation Error and  Unprocessable Entity")*      ,
     *     @SWG\Response(response=401, description="Invalid Token And Unauthenticated"),
     *     @SWG\Response(response=500, description="Internal serve error")
     * )
     */
    public function mydoctors(AllServices $request) {

        try {
            //DB::connection()->enableQueryLog();
            $user = Auth::user();
            $this->response['data'] = array();
            $resultQuery = Booking::select(['user_id','vender_id'])
                                    ->whereHas('vendor', function($query) use($request){
                                        if($request['search']){
                                            $query->where('firstname','like', '%'.$request['search'].'%');
                                        }

                                    })
                                    ->with([
                                        'vendor' => function($query) use($request){
                                            $query->select(['firstname','id','image']);
                                            $query->with([
                                                'userAddress' => function($query) use($request){
                                                    $query->select(['country','city','pincode','user_id']);
                                                    $lat = $request['latitude'];
                                                    $long = $request['longitude'];
                                                    $query->select('full_address','pincode',DB::raw(" user_addresses.user_id, ROUND( 6371 * acos( cos( radians($lat) ) * cos( radians( latitude ) ) * cos( radians( longitude ) - radians($long) ) + sin( radians($lat) ) * sin( radians( latitude ) ) ),2) AS distance"))
                                                    ->havingRaw('distance <= '.$request['distance']);
                                                },
                                                'venderServices' => function($query) use($request){
                                                    $query->select(['cat_id','id','vender_id']);
                                                    $query->with([
                                                        'servicecategory' => function($query){
                                                           $query->select(['cat_name','id']);
                                                        }
                                                    ]);

                                                    if($request['category']){
                                                        $query->where('cat_id', '=', $request['category']);
                                                    }
                                                    
                                                    if($request['min_experience']){
                                                        $query->where('experience', '>=', $request['min_experience']);
                                                    }  
                                                    
                                                    if($request['max_experience']){
                                                        $query->where('experience', '<=', $request['max_experience']);
                                                    }  
                                                },
                                                'vendorReviews' => function($query){
                                                    $query->select('vender_id', DB::Raw('AVG(rating) as avg_rating'));
                                                }
                                            ]);
                                        }
                                    ]);


            $resultQuery->where('bookings.user_id', '=', $user->id);
            $resultQuery->groupBy('bookings.vender_id');
           
            if($request['page'] ){
                $resultQuery->offset(10*$request['page']);
            }

            if($request['limit']){
                $resultQuery->limit($request['limit']);
            }
            
            $data = $resultQuery->get()
                                ->toArray(); 
            
            //print_r($data); exit;
            $returnData = [];

            foreach($data as $key => $value){
                
                $returnData[$key]['cat_id'] = '';
                $returnData[$key]['vender_id'] = $value['vender_id'];
                $returnData[$key]['price'] = '';
                $returnData[$key]['name'] = $value['vendor']['firstname'];
                $returnData[$key]['profile_image'] = $value['vendor']['image'] ? $value['vendor']['image'] : '';
                $returnData[$key]['avg_rating'] = $value['vendor']['vendor_reviews'] ? round($value['vendor']['vendor_reviews'][0]['avg_rating'],2) : 0;
                $returnData[$key]['cat_name'] = '';
                $returnData[$key]['cat_image'] = '';

                $returnData[$key]['venderaddress'] = $value['vendor'] ? round($value['vendor']['user_address']['distance'],2) : 0;

                ///$returnData[$key]['distance'] = $value['venderaddress'] ? $value['venderaddress']['distance'] : 0;
                $returnData[$key]['full_address'] = $value['vendor'] ? $value['vendor']['user_address']['full_address'] : '';
                $returnData[$key]['pincode'] = $value['vendor'] ? $value['vendor']['user_address']['pincode'] : '';
                
                
            }     

            //print_r(DB::getQueryLog()); exit;
            if (!$data) {
                $this->response['status'] = 0;
                $this->response['message'] = trans('api/service.no_record_found');
                $this->response['data'] = $returnData;
                return response()->json($this->response, 200);
            }
            
            $this->response['status'] = 1;
            $this->response['message'] = trans('api/service.total_record');
            $this->response['data'] = $returnData;
            return response()->json($this->response, 200);
        } catch (\Exception $ex) {
            $this->response['message'] = trans('api/user.something_wrong');
            return response($this->response, 500);

        }
    }

    /**
     * @SWG\Get(
     *     path="/doctor/{id}",
     *     tags={"Doctor"},
     *     summary="Get Docter Full Profile",
     *     description="Get Docter Full Profile using API's",
     *     operationId="getDocterProfile",
     *     @SWG\Parameter(
     *         name="Authorization",
     *         required = true,
     *         in="header",
     *         description="Authorization Token",
     *         type="string"
     *     ),
     *     @SWG\Parameter(
     *         name="id",
     *         required = true,
     *         in="path",
     *         description="User id",
     *         type="integer"
     *     ),
     *     @SWG\Response(response=200, description="Successful operation"),
     *     @SWG\Response(response=422, description="Validation Error and  Unprocessable Entity")*      ,
     *     @SWG\Response(response=401, description="Invalid Token And Unauthenticated"),
     *     @SWG\Response(response=500, description="Internal serve error")
     * )
     */
    public function getUserById(DocterProfile $request , $id) {
        try{
            $data = User::select(['id','firstname','image']) 
                        ->with([
                            'userManyAddress' => function($query) {
                                $query->select(['full_address','country','user_id','city','pincode','state']);
                            },
                            'vendorEducation' => function($query){
                                $query->select(['degree','user_id','edu_desc','batch']);
                            },
                            'vendorReviews' => function($query){
                                $query->select(['rating','feedback_message','vender_id','user_id','created_at']);
                                $query->with(['user' => function($query){
                                        $query->select(['id','firstname']);
                                    }
                                ]);
                                $query->latest();
                                $query->limit(5);
                            },
                            'vendorReviewsAvg' => function($query) use($request) {
                                $query->select('vender_id', DB::Raw('AVG(rating) as avg_rating'));
                                if($request['review']){
                                    $query->havingRaw('AVG(rating) >= ?', [sprintf("%.2f", $request['review'])]);
                                }
                                $query->groupBy('vender_id');
                            },
                            'venderServices' => function($query) {
                                $query->select(['id','price','experience','vender_id','description','cat_id']);
                                $query->with(['servicecategory' => function($query){
                                        $query->select(['cat_name','id']);
                                    }
                                ]);
                            }
                        ])
                        ->withCount(['vendorReviews','venderBooking'])
                        ->find($id);
            $data['vender_langauages'] = ['Hindi','English'];
            $data['suggested_by'] = Review::where(['vender_id' => $id , 'is_like' => '1'])->count();
            $this->response['status'] = 1;
            $this->response['message'] = trans('api/service.total_record');
            $this->setData($data);
            return response()->json($this->response, 200);

        } catch (\Exception $ex) {
            $this->response['message'] = trans('api/user.something_wrong');
            return response($this->response, 500);
        }
    }

    public function listBanners() {
        $banners = Banner::orderBy('id', 'desc')->get();
        return (new BannerCollection($banners))->additional([
                    'status' => 1,
                    'message' => trans('api/service.all_banners')
        ]);
    }

    public function checkCouponCode(Request $request) {
        $validator = Validator::make($request->all(), [
                    'coupon_code' => ['required'],
        ]);
        if ($validator->fails()) {
            $this->response['message'] = $validator->errors()->first();
            return response()->json($this->response, 401);
        }
        $coupon = Coupon::where(['code' => $request['coupon_code']])->first();
        if (!$coupon) {
            $this->response['status'] = 0;
            $this->response['message'] = trans('api/service.invalid_coupon');
            return response()->json($this->response, 404);
        }
        if (!$coupon->status) {
            $this->response['status'] = 0;
            $this->response['message'] = trans('api/service.coupon_is_not_active');
            return response()->json($this->response, 401);
        }
        $date = strtotime(date('Y-m-d'));
        $start_date = strtotime(date($coupon->startDateTime));
        $end_date = strtotime(date($coupon->endDateTime));
        if ($date < $start_date || $date > $end_date) {
            $this->response['status'] = 0;
            $this->response['message'] = trans('api/service.coupon_is_expired');
            return response()->json($this->response, 401);
        }
        if ($request['order_amount'] < $coupon->minAmount) {
            $this->response['status'] = 0;
            $this->response['message'] = trans('api/service.order_amount_is_less_than_required') . $coupon->minAmount;
            return response()->json($this->response, 401);
        }
        return (new CouponResource($coupon))->additional([
                    'status' => 1,
                    'message' => trans('api/service.valid_coupon')
        ]);
    }

}

<?php

namespace App\Http\Controllers\API\v1;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;
use App\venderSlot;
use Illuminate\Support\Facades\Auth;
use App\Http\Resources\VenderSlots as VenderSlotsResource;
use App\Http\Resources\SlotCollection;
use App\Http\Resources\VenderSlotsCollection;
use App\slot;
use App\User;
use App\Booking;
use App\BookedSlot;
use App\VenderService;
use App\VendorSlotDayBreak;
use Carbon;
use App\Http\Requests\GetSlots;

class venderSlotController extends Controller {

    protected $response = [
        'status' => 0,
        'message' => '',
    ];

    public function __construct() {
        $this->response['data'] = new \stdClass();
    }

    public function addVenderSlot(Request $request) {

        /* $validator = Validator::make($request->all(), [
          'slot_id' => ['required']
          ]);
          if ($validator->fails()) {
          $this->response['message'] = $validator->errors()->first();
          return response()->json($this->response, 401);
          } */
        $user = Auth::User();
        if (!$user->hasRole(['vendor'])) {

            $this->response['message'] = trans('api/user.user_is_not_vender');
            return response()->json($this->response, 401);
        }
        $count = 0;
        if ($request['slot_id']) {

            $check_slot_already_assigned = venderSlot::where('vender_id', '=', $user->id)->get();
            $assigned_array = array();
            foreach ($check_slot_already_assigned as $check_slot_already_assin) {
                $assigned_array[] = $check_slot_already_assin['slot_id'];
            }
            $array1 = array_diff($assigned_array, $request['slot_id']);
            $array2 = array_diff($request['slot_id'], $assigned_array);
            $array3 = array_merge($array1, $array2);
            $allSlots = venderSlot::where(['vender_id' => $user->id])->get();
            if (!count($array3)) {
                return (new VenderSlotsCollection($allSlots))->additional([
                            'status' => 1,
                            'message' => trans('api/service.no_change_in_slot')
                ]);
            }
            foreach ($array3 as $slot_id) {

                $if_alreay_assigned_then_remove = venderSlot::where(['slot_id' => $slot_id, 'vender_id' => $user->id])->first();
                if (isset($if_alreay_assigned_then_remove->id)) {
                    $if_alreay_assigned_then_remove->delete();
                } else {
                    $addVenderSlot = venderSlot::create(['vender_id' => $user->id, 'slot_id' => $slot_id]);
                    $count++;
                }
            }
            $allSlots = venderSlot::where(['vender_id' => $user->id])->get();
            return (new VenderSlotsCollection($allSlots))->additional([
                        'status' => 1,
                        'message' => trans('api/service.slots_added_successfully')
            ]);
        } else {

            $if_alreay_assigned_then_remove = venderSlot::where(['vender_id' => $user->id])->first();
            if ($if_alreay_assigned_then_remove) {
                $if_alreay_assigned_then_remove->delete();
            }
            $this->response['message'] = trans('api/service.no_new_slot_added');
            return response()->json($this->response, 200);
        }
    }

    /**
     * @SWG\Get(
     *     path="/slots",
     *     tags={"Doctor"},
     *     summary="Get list of doctor slots",
     *     description="Get list of docter slots using API's",
     *     operationId="getUserDetails",
     *      @SWG\Parameter(
     *         name="Authorization",
     *         in="header",
     *         description="Authorization Token",
     *         type="string"
     *     ),
     *     @SWG\Parameter(
     *         name="language",
     *         in="header",
     *         description="Language",
     *         type="string"
     *     ),
     *     @SWG\Parameter(
     *         name="vender_id",
     *         in="query",
     *         description="Vender Id",
     *         type="string"
     *     ),
     *     @SWG\Parameter(
     *         name="date",
     *         in="query",
     *         description="date",
     *         type="string"
     *     ),
     *     @SWG\Response(response=200, description="Successful operation"),
     *     @SWG\Response(response=422, description="Validation Error and  Unprocessable Entity")*      ,
     *     @SWG\Response(response=401, description="Invalid Token And Unauthenticated"),
     *     @SWG\Response(response=500, description="Internal serve error")
     * )
     */

    public function getSlots(GetSlots $request) {
        try{
            $dt = Carbon\Carbon::parse($request->date);
            $daysBreak = VendorSlotDayBreak::select('day')
                                            ->where([
                                                'vendor_id' => $request['vender_id'],
                                                'day' => $dt->dayOfWeekIso
                                            ])
                                            ->count();
            
            $return_slots = venderSlot::doesnthave('slotDateOff')
                                        ->withCount([
                                            'bookings' => function($query) use($request) {
                                                $query->where('booking_date',$request->date);
                                            }
                                        ])
                                        ->where([
                                            'vender_id' => $request['vender_id'],
                                            'day' => $dt->dayOfWeekIso
                                        ])
                                        ->havingRaw('max_patient > bookings_count'
                                        );
            
            

            if($daysBreak > 0){
                $return_slots = $return_slots->whereNotIn('day',[$dt->day]);
            }
            
            $return_slots_array = $return_slots->get();
            if($return_slots_array->isEmpty()){
                $this->response['status'] = 0;
                $this->response['message'] = trans('api/service.no_record_found');
                $this->response['data'] = $return_slots_array;
                return response()->json($this->response, 200);
            }
            $this->response['status'] = 1;
            $this->response['message'] = trans('api/service.total_record');
            $this->response['data'] = $return_slots_array;
            return response()->json($this->response, 200);
            
        }
        catch (\Exception $ex) {
            
            $this->response['message'] = trans('api/user.something_wrong');
            return response($this->response, 500);
        }
    }

    public function getBookedSlots(Request $request) {

        $service_id = $request['service_id'];
        $bookingDates = $request['dates'];
        $getVender = array();
        if ($request['checkSql']) {
            $getVender = VenderService::select('vender_id')
                            ->join('users', 'vender_services.vender_id', '=', 'users.id')
                            ->where('users.online', '=', '1')
                            ->where('users.status', '=', '1')
                            ->where('service_id', '=', $service_id)
                            ->get()->toArray();
        } else {

            //$getVender = VenderService::select('vender_id')->where('service_id', '=', $service_id)->get()->toArray();
            $getVender = VenderService::select('vender_id')
                            ->join('users', 'vender_services.vender_id', '=', 'users.id')
                            ->where('users.online', '=', '1')
                            ->where('users.status', '=', '1')
                            ->where('service_id', '=', $service_id)
                            ->get()->toArray();
        }
        $venders = array_column($getVender, 'vender_id');
        $dates = array();
        $final_array = array();
        $dates = array();

        if (empty($venders)) {

            foreach ($bookingDates as $bookingD) {
                $day = date('N', ($bookingD));
                $getSlotsOfDay = slot::select('id')->where('day', '=', $day)->get()->toArray();
                $allDays = array_column($getSlotsOfDay, 'id');
                $final_array[date('Y-m-d', $bookingD)] = $allDays;
            }


            $this->response['status'] = 1;
            $this->response['message'] = trans('api/user.booked_slots');
            $this->response['data'] = $final_array;
            return response()->json($this->response, 200);
        }

        $get_avail_slots = venderSlot::select('slot_id')->whereIn('vender_id', $venders)->get()->toArray();

        $get_avail_slots = array_column($get_avail_slots, 'slot_id');

        if (empty($get_avail_slots)) {

            foreach ($bookingDates as $bookingD) {
                $day = date('N', ($bookingD));
                $getSlotsOfDay = slot::select('id')->where('day', '=', $day)->get()->toArray();
                $allDays = array_column($getSlotsOfDay, 'id');
                $final_array[date('Y-m-d', $bookingD)] = $allDays;
            }


            $this->response['status'] = 1;
            $this->response['message'] = trans('api/user.booked_slots');
            $this->response['data'] = $final_array;
            return response()->json($this->response, 200);
        }

        $allslots = slot::all();
        $allDates = array();
        $fully_booked_slot = array();

        foreach ($bookingDates as $bookingDate) {
            $allDates[] = date('Y-m-d H:i:s', $bookingDate);
        }
        $bookedSlots = array();
        foreach ($allDates as $allDate) {
            $status_id = array(Booking::venderAssigned, Booking::venderOnTheWay, Booking::orderInProgres, Booking::venderArived);
            $bs = BookedSlot::select('slot_id')->whereIn('vender_id', $venders)->whereDate('booking_date', '=', date('Y-m-d 00:00:00', strtotime($allDate)))->groupby('slot_id')->get();
            $i = 0;

            foreach ($bs as $b) {

                $bookedSlots[$allDate][$i]['slot_id'] = $b->slot_id;
                //$bookedSlots[$allDate][$i]['user_id'] = $b->user_id;
                $i++;
            }
        }

        foreach ($bookedSlots as $date => $bookedSlot) {
            foreach ($bookedSlot as $bkSlt) {
                $status_id = array(Booking::venderAssigned, Booking::venderOnTheWay, Booking::orderInProgres, Booking::venderArived);
                $booked_slot_count = BookedSlot::whereDate('booking_date', '=', date('Y-m-d 00:00:00', strtotime($date)))->where(['slot_id' => $bkSlt['slot_id']])->whereIn('status_id', $status_id)->whereIn('vender_id', $venders)->distinct('slot_id')->get();
                $bkSlot = $booked_slot_count->count();

                $get_available_venders = DB::table('vender_services')->select('vender_services.vender_id')->distinct()
                                ->join('vender_slots', 'vender_services.vender_id', '=', 'vender_slots.vender_id')
                                ->where('vender_services.service_id', '=', $request['service_id'])->get();



                //$get_available_venders = venderSlot::where(['slot_id' => $bkSlt['slot_id']]);
                $avail_vender = $get_available_venders->count();

                if ($bkSlot >= $avail_vender) {
                    $fully_booked_slot[date('Y-m-d', strtotime($date))][] = $bkSlt['slot_id'];
                }
            }
        }

        $dates_array = array();
        foreach ($bookingDates as $key => $bookingDate) {
            $dates_array[date('Y-m-d', $bookingDate)] = array();
        }
        $final_array = array_merge($dates_array, $fully_booked_slot);
        $this->response['status'] = 1;
        $this->response['message'] = trans('api/user.booked_slots');
        $this->response['data'] = $final_array;
        return response()->json($this->response, 200);
    }

}

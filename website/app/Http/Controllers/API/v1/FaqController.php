<?php

namespace App\Http\Controllers\API\v1;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Faq;

class FaqController extends Controller
{
    protected $response = [
        'status' => 0,
        'message' => '',
    ];

    public function __construct() {
        
        $this->response['data'] = new \stdClass();
    }

    /**
     * @SWG\Get(
     *     path="/faq",
     *     tags={"Faq"},
     *     summary="Get faq list",
     *     description="Get faq detail for faq page",
     *     operationId="getFaq",
     *     @SWG\Parameter(
     *         name="Authorization",
     *         in="header",
     *         description="Authorization Token",
     *         type="string"
     *     ),
     *     @SWG\Parameter(
     *         name="language",
     *         in="header",
     *         description="Language symbols",
     *         type="string"
     *     ),
     *     @SWG\Response(response=200, description="Successful operation"),
     *     @SWG\Response(response=422, description="Validation Error and  Unprocessable Entity")*      ,
     *     @SWG\Response(response=401, description="Invalid Token"),
     *     @SWG\Response(response=500, description="Internal serve error")
     * )
     */
    public function getFaq(Request $request){
        try{
            $card = Faq::where('language',$request->header('language'))
                        ->get();

            if($card){
                $this->response['status'] = 1;
                $this->response['message'] = trans('api/service.total_record');
                $this->response['data'] = $card;
                return response()->json($this->response, 200);
            }

            $this->response['status'] = 0;
            $this->response['message'] = trans('api/service.no_record_found');
            $this->response['data'] = $card;
            return response()->json($this->response, 200);
            
        }
        catch (\Exception $ex) {
            
            $this->response['message'] = trans('api/user.something_wrong');
            return response($this->response, 500);
        }
    }

}

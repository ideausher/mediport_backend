<?php

namespace App\Http\Controllers\Admin;

use App\ServiceCategory;
use App\Service;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Validator;
use Illuminate\Validation\Rule;
use DB;

class ServiceCategoryController extends Controller {

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {


        $categories = ServiceCategory::orderBy('created_at', 'desc')->get();
        return view('admin/servicesCategoryList', compact('categories'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        return view('admin/serviceCategory');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
        //
    }

    public function saveCategory(Request $request) {
       
        $data = $request->all();
        
        $rules = array(
            'cat_name' => 'unique:service_categories',
            'cat_name_ar' => 'required|unique:service_categories,cat_name_ar',
            'cat_name_es' => 'required|unique:service_categories,cat_name_es',
            'image' => 'required||mimes:jpeg,png,jpg,gif,svg||max:4096',
        );
        $validator = Validator::make($data, $rules);
        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator)->withInput();
        }
        if ($request->hasFile('image')) { //check the file present or not
            $image = $request->file('image'); //get the file
            $data['image'] = uniqid() . '.' . $image->getClientOriginalExtension(); //get the  file extention
            $destinationPath = public_path('/images'); //public path folder dir
            if (!$image->move($destinationPath, $data['image'])) {
                echo "image not uploaded correclty! Try Later";
                die;
            } //mve to destination you mentioned
        }
        $data['status'] = '1';
        
        $cat_service = ServiceCategory::create($data);
        return redirect()->route('listCategory')->with('success_message', trans('admin/servicecategory.service_added_successfully'));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\ServiceCategory  $serviceCategory
     * @return \Illuminate\Http\Response
     */
    public function show(ServiceCategory $serviceCategory) {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\ServiceCategory  $serviceCategory
     * @return \Illuminate\Http\Response
     */
    public function edit(ServiceCategory $serviceCategory) {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\ServiceCategory  $serviceCategory
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ServiceCategory $serviceCategory) {
        //
    }

    public function editCategory($id) {
        $category = ServiceCategory::where(['id' => $id])->first();
        if ($category) {
           
            return view('admin/serviceCategory',compact('category'));
            
        } else {
            return redirect('admin/addCategory')->with('error_message', trans('admin/service.service_invalid_message'));
        }
    }

    public function updateCategory(Request $request) {
        
        
        $data = $request->all();

        $rules = array(
            'cat_name' => 'required|unique:service_categories,cat_name',
            'cat_name_ar' => 'required|unique:service_categories,cat_name_ar',
            'cat_name_es' => 'required|unique:service_categories,cat_name_es',
        );
        $validator = Validator::make($data, [
                    'cat_name' => [
                        'required',
                        Rule::unique('service_categories')->ignore($data['id']),
                    ],
        ]);
        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator)->withInput();
        }
        if ($request->hasFile('image')) {   //check the file present or not
            $image = $request->file('image'); //get the file
            $data['image'] = uniqid() . '.' . $image->getClientOriginalExtension(); //get the  file extention
            $destinationPath = public_path('/images'); //public path folder dir
            if (!$image->move($destinationPath, $data['image'])) {
                echo "image not uploaded correclty! Try Later";
                die;
            } //mve to destination you mentioned
        }

        $cat_service = ServiceCategory::where('id', $data['id'])->first();
        
        $cat_service->cat_name_ar = $data['cat_name_ar'];
        $cat_service->cat_name_es = $data['cat_name_es'];
        $cat_service->cat_name = $data['cat_name'];
        if ($request->hasFile('image')) {
            $cat_service->image = $data['image'];
        }
        $cat_service->save();
        return redirect()->route('listCategory')->with('success_message', trans('admin/servicecategory.service_updated_successfully'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\ServiceCategory  $serviceCategory
     * @return \Illuminate\Http\Response
     */
    public function deleteCategory($id) {
        
        $specification = ServiceCategory::findOrFail($id)->delete();
        //DB::table('services')->where('cat_id', $id)->update(['cat_id' => 0]);
        if($specification) {
            session()->flash('flash_message', 'Category deleted successfully!');
            return redirect()->back();
        }
    }
    
    public function changeServiceStatus(Request $request)
    {
        $data = $request->all();

        $category = ServiceCategory::find($data['id']);

        if ($category->status) {
            $category->status = '0';
        } else {
            $category->status = '1';
        }
        $category->save();
        $array = array();
        $array['success'] = true;
        $array['message'] = trans('admin/service.service_status_message');
        echo json_encode($array);
    }

    public function destroy(ServiceCategory $serviceCategory) {
        //
    }

}

<?php

namespace App\Http\Controllers\Admin;

use Auth;
use App\Models\Notification;
use App\Http\Controllers\Controller;
use App\User;
use App\Helpers\Datatable\SSP;
use App\Mail\Admin\NotificationEmail;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Jobs\SendEmailJob;
use App\Admin as Admin;
use App\DeviceDetails;
use App\Services\PushNotification;



class NotificationController extends Controller {

    public function getNotifications($type = null) {
      
        $user_id = Auth::guard('admin')->user()->id;
        if ($type == 'all') {
            $notifications = Notification::with('getUser')->orderBy('id', 'DESC')->where('vender_id', $user_id)->paginate(10);
        } else {
            
            $notifications = Notification::where('is_read', 0)->with('getUser')->orderBy('id', 'DESC')->where('vender_id', $user_id)->get();
            foreach ($notifications as $notification) {
                $notification->is_read = 1;
                $notification->save();
            }
        }
        $patient = User::select("users.*", "role_user.user_id", "roles.id")
                        ->join("role_user", "role_user.user_id", '=', "users.id")
                        ->join("roles", "roles.id", "role_user.role_id")
                        ->where('roles.id', 1)->get();
        $doctor = User::select("users.*", "role_user.user_id", "roles.id")
                        ->join("role_user", "role_user.user_id", '=', "users.id")
                        ->join("roles", "roles.id", "role_user.role_id")
                        ->where('roles.id', 2)->get();

        return view('admin/notifications', compact('notifications', 'type', 'patient', 'doctor'));
    }
  
    public function NotificationEmail(Request $request) {

        
        $data = $request->all();
        
        $profile = auth()->guard('admin');
        $admin = Admin::findOrFail($profile->user()->id);
        $users_detail=User::find($data['id']);
        $users_id =$users_detail->pluck('id');
        $emailJob = new SendEmailJob($users_detail,$data,$admin);
    
        // Inserting data into table Notification
        Notification::insert([
                                'vender_id'=> $admin->id,
                                'user_id' => $request->id[0],
                                'type'=> '3', 
                                'title'=>$request->subject,
                                'message'=>$request->message,
                                'is_read' => '2',
                            ]);
        
        
        DeviceDetails::sendBulkNotification($data['subject'],$data['message'],$users_id);
        session()->flash('success_message', "Notification SuccessFully Sent");
        return redirect()->back();
  
    }
    

}

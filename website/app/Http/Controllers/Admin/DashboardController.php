<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use App\Transaction;
use Carbon\Carbon;
use DB;

class DashboardController extends Controller {
    
    public function __construct() {
        
    }

    /**
     * Admin dashboard
     *
     */
    public function index() {
       
        try{
            
            $users = \App\RoleUser::where('role_id', '1')->count();
            //$onlineUsers = \App\User::active()->online()->count();
            $totalVendors = \App\RoleUser::where('role_id', '2')->count();
            //$services = \App\Service::count();
            $bookings = \App\Booking::count();
            $transactions = \App\Transaction::count();
            $enquiries = \App\Enquiry::count();
    
            $user = auth()->guard('admin')->user();
            return view('admin/dashboard',  compact('users','bookings','transactions','enquiries', 'totalVendors'));

        }
        catch (\Exception $ex) {
            return view('errors/500');
        }

    }
}
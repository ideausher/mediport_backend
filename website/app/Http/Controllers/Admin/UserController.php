<?php

namespace App\Http\Controllers\Admin;

use App\Helpers\Datatable\SSP;
use App\Http\Controllers\Controller;
use App\User;
use App\phoneOtp;
use App\UserAddresses;
use File;
use Validator;
use Illuminate\Http\Request;

class UserController extends Controller {

    /**
     * User Model
     * @var User
     */
    protected $user;
    protected $pageLimit;

    /**
     * Inject the models.
     * @param User $user
     */
    public function __construct(User $user) {
        
        $this->user = $user;
       
        $this->pageLimit = config('settings.pageLimit');
         
    }

    /**
     * Display a listing of user
     *
     * @return Response
     */
    public function index() {
        
        // Show the page
        return view('admin/userList');
    }
    

    /**
     * Display the specified user.
     *
     * @param  int  $id
     * @return Response
     */
    
    public function show($id) {

        $user = User::findOrFail($id);
       
        return view('admin/userDetails', compact('user'));
    }
    public function showuser($id) {

        $user['user'] = User::findOrFail($id);
        
       //$user['userAddress']=UserAddresses::where(['user_id'=> $id])->get();
        return view('admin/patientView',$user);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id) {
        //$user = User::with('userAddress')->find($id);
        $user = User::find($id);
        $home_addresses = $user->userAddress()->where('address_type', 'LIKE', '%home%')->get();
        $office_addresses = $user->userAddress()->where('address_type', 'LIKE',  '%office%')->get();
        $other_addresses = $user->userAddress()->where('address_type', 'LIKE',  '%other%')->get();
        $work_addresses = $user->userAddress()->where('address_type', 'LIKE',  '%work%')->get();

        if ($user) {
            return view('admin/edituser', compact('user', 'home_addresses', 'office_addresses', 'other_addresses', 'work_addresses'));
        }
    }
    
    public function Add() {
        
        $users = User::orderBy('created_at', 'Asc')->get();
        return view('admin/addUser', compact('users'));
    }
    public function AddUser(Request $request)
    {
       
              $data = $request->all();
             
              
             $validator = Validator::make($request->all(), [
                    'first_name' => 'required|string|max:45',
                    'last_name' => 'required_if:role_id,==,2',
                    'email' => 'required|string|email|max:255',
                    'password' => 'required|string|min:6|same:confirm_password',
                    'phone' => 'required',
                    'gender' => 'required|in:male,female',
                    'image' => 'required||mimes:jpeg,png,jpg,gif,svg||max:4096',
                    'blood_group'=>'required',
                    'country_code'=>'required',
        ],[
            
        ]);
             if ($validator->fails()) {
            return redirect()->back()->withErrors($validator)->withInput();
        }
                 if ($request->hasFile('image')) { //check the file present or not
            $rules = array('image' => 'mimes:jpeg,png,jpg,gif,svg||max:4096');
            $validator = Validator::make($data, $rules);
            if ($validator->fails()) {
                return redirect()->back()->withErrors($validator)->withInput();
            }
            $image = $request->file('image'); //get the file
            $data['image'] = uniqid() . '.' . $image->getClientOriginalExtension(); //get the  file extention
            $destinationPath = public_path('images/avatars/'); //public path folder dir
            if (!$image->move($destinationPath, $data['image'])) {
                echo "image not uploaded correclty! Try Later";
                die;
            } //mve to destination you mentioned
        }
        
         $user_check = User::where('email', $data['email'])->first();
         if(!$user_check)
         {
             
       $user = User::create([
                        'firstname' => $data['first_name'],
                        'lastname' => $data['last_name'],
                        'email' => $data['email'],
                        'password' => $data['password'],
                        'phone_number' => $data['phone'],
                        'gender' => $data['gender'],
                        'status' => 1,
                        'image'=> $data['image'],
                        'blood_group'=> $data['blood_group'],
                        'phone_country_code'=> $data['country_code'],
            ]);
            
        $user->attachRole($data['role_id']);
        
        return redirect('admin/patients')->with('success_message', trans('admin/user.user_update_message'));

         }
         else{
            return redirect()->back()->withErrors("Patient with same E-Mail Id already exist")->withInput();
         }
         
                
    }

    //save edit data
    public function update(Request $request, $id) {
        $user = User::findOrFail($id);
        $data = $request->all();

        $rules = array(
            'firstname' => 'required',
            'lastname' => 'required',
            'gender' => 'required',
            //'phone_number' => 'required|numeric|digits_between:10,15'
        );
        $validator = Validator::make($data, $rules);
        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator)->withInput();
        }
        if ($request->hasFile('image')) { //check the file present or not
            $rules = array('image' => 'mimes:jpeg,png,jpg,gif,svg||max:4096');
            $validator = Validator::make($data, $rules);
            if ($validator->fails()) {
                return redirect()->back()->withErrors($validator)->withInput();
            }
            $image = $request->file('image'); //get the file
            $data['image'] = uniqid() . '.' . $image->getClientOriginalExtension(); //get the  file extention
            $destinationPath = public_path('images/avatars/'); //public path folder dir
            if (!$image->move($destinationPath, $data['image'])) {
                echo "image not uploaded correclty! Try Later";
                die;
            } //mve to destination you mentioned
        }
        $user->update($data);

        $rules = array(
            'firstname' => 'required',
            'lastname' => 'required',
            'gender' => 'required',
            'phone_number' => 'required|numeric',
        );


        // update user data

        if (isset($data['add']['home'])) {
            $homeAdd = UserAddresses::findOrFail($data['add']['home']['id']);
            if ($homeAdd) {
                $addData = $data['add']['home'];
                $homeAdd->update($addData);
            }
        }
        if (isset($data['add']['office'])) {
            $homeAdd = UserAddresses::findOrFail($data['add']['office']['id']);
            if ($homeAdd) {
                $addData = $data['add']['office'];
                $homeAdd->update($addData);
            }
        }
        if (isset($data['add']['other'])) {
            $homeAdd = UserAddresses::findOrFail($data['add']['other']['id']);
            if ($homeAdd) {
                $addData = $data['add']['other'];
                $homeAdd->update($addData);
            }
        }
        return redirect('admin/patients')->with('success_message', trans('admin/user.user_update_message'));
    }

    public function destroy($id) {
        $user = User::findOrFail($id);
//        if ($user->phone_number) {
//            $existInOtpTable = phoneOtp::where(['phone_no' => $user->phone_number])->first();
//            phoneOtp::destroy($existInOtpTable->id);
//        }
        $oldFile = \Config::get('constants.USER_IMAGE_PATH') . $user->image;
        if (File::exists($oldFile)) {
            File::delete($oldFile);
        }
        User::destroy($id);

        $array = array();
        $array['success'] = true;
        $array['message'] = trans('admin/user.user_delete_message');
        echo json_encode($array);
    }

    public function changeUserStatus(Request $request) {
        $data = $request->all();
        $user = User::find($data['id']);

        if ($user->status == User::pending || $user->status == User::inActive || $user->status == User::rejected) {
            $user->status = '1';
        } else {
            $user->status = '0';
        }
        $user->save();

        $array = array();
        $array['status'] = $user->status;
        $array['success'] = true;
        $array['message'] = trans('admin/user.user_status_message');
        echo json_encode($array);
    }

    /**
     * Change user credit of the specified user.
     *
     * @return \Illuminate\Http\Response
     */
    public function updateCredit(Request $request) {

        $data = $request->all();
        $data['credit'] = $data['value'];
        $user = User::find($data['userId']);

        $user->update($data);
        $array = array();
        $array['success'] = true;
        session()->flash('success_message', trans('admin/user.credit_update_message'));
        echo json_encode($array);
    }

    public function getUserData() {

        
        $table = 'users';

        $primaryKey = 'id';

        $columns = array(
            array('db' => 'users.firstname', 'dt' => 0, 'field' => 'firstname'),
            array('db' => 'users.email', 'dt' => 1, 'field' => 'email'),
            array('db' => 'users.phone_number', 'dt' => 2, 'field' => 'phone_number'),
            array('db' => 'users.gender', 'dt' => 3, 'field' => 'gender'),
            array('db' => 'users.blood_group', 'dt' => 5, 'field' => 'blood_group'),
            array('db' => 'users.image', 'dt' => 4, 'formatter' => function( $d, $row ) {
                    if(isset($row) && !is_null($row['image'])){
                        return '<img id="uploaded-image" src="'.url('/images/avatars/'.$row['image']).'"  height="35" width="35">';
                    }else{
                        return '<img id="uploaded-image" src="'.url('uploads/user/default.png').'"  height="35" width="35">';
                    }
            }, 'field' => 'image'),
            array('db' => 'users.id', 'dt' => 6, 'formatter' => function ($d, $row) {
                    $operation = '<a href="javascript:;" id="' . $d . '" class="btn btn-danger d-inline-block delete-btn" title="' . trans('admin/common.delete') . '" data-toggle="tooltip"><i class="fa fa-times"></i></a>'
                            .'&nbsp &nbsp<a href="patient/edit/' . $d . '" class="btn btn-primary d-inline-block" title="' . trans('admin/common.edit') . '" data-toggle="tooltip"><i class="fa fa-pencil"></i></a>';
                            //.' <a href="'.url("admin/users/showuser/$d").'" id="' . $d . '" class="btn btn-primary d-inline-block  view-btn" title="' . trans('admin/common.view') . '" data-toggle="tooltip"><i class="fa fa-eye"></i></a>';
                    return $operation;
                }, 'field' => 'id'),
        );

        $sql_details = array(
            'user' => config('database.connections.mysql.username'),
            'pass' => config('database.connections.mysql.password'),
            'db' => config('database.connections.mysql.database'),
            'host' => config('database.connections.mysql.host'),
        );
          $joinQuery = '';
//        $joinQuery = "LEFT JOIN (SELECT COUNT(*) AS total_bookings, user_id FROM bookings GROUP BY user_id ) as bk ON bk.user_id = users.id";
//        $joinQuery .= " LEFT JOIN (SELECT COUNT(*) AS total_transactions, user_id FROM transactions GROUP BY user_id ) as trans ON trans.user_id = users.id";
        $joinQuery .= " LEFT JOIN role_user ru ON ru.user_id = users.id";
        $joinQuery .= " LEFT JOIN roles r ON r.id = ru.role_id";
        $joinQuery .= " LEFT JOIN user_addresses ur ON ur.user_id = users.id";
       $extraWhere = " r.name='User'";
        $groupBy = "";
        echo json_encode(
                SSP::simple($_GET, $sql_details, $table, $primaryKey, $columns, $joinQuery, $extraWhere, $groupBy)
        );
    }

}

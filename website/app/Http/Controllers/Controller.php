<?php
/**
 * @SWG\Swagger(
 *     basePath="/api/v1",
 *     schemes={"http", "https"},
 *     host=L5_SWAGGER_CONST_HOST,
 *     @SWG\Info(
 *         version="1.0.0",
 *         title="Mediport Api's",
 *         description="These api's are created to share data with multiplatforms.",
 *         @SWG\Contact(
 *             email="support@ashriya.com"
 *         ),
 *     )
 * )
 */
namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

class Controller extends BaseController {

    use AuthorizesRequests,
        DispatchesJobs,
        ValidatesRequests;

    public static function getSettings() {
        $setting = \App\Setting::first();
        if ($setting) {
            \Config::set('app.locale', $setting->language);
            \Config::set('settings.title', $setting->site_title);
            \Config::set('settings.logo', $setting->logo);
            \Config::set('settings.email', $setting->email);
            \Config::set('settings.phone', $setting->phone);
            \Config::set('settings.map', $setting->map);
            \Config::set('settings.address', $setting->address);
            \Config::set('settings.facebook', $setting->facebook);
            \Config::set('settings.twitter', $setting->twitter);
            \Config::set('settings.linkedin', $setting->linkedin);
            \Config::set('settings.googleplus', $setting->googleplus);
        }
        return $setting;
    }

    public static function getAdminSettings() {
        $admin = \App\Admin::first();
        $appName = \Config::get('app.name');
        if ($admin) {
            \Config::set('mail.from.address', $admin->email);
            \Config::set('mail.from.name', $appName);

            \Config::set('settings.admin.email', $admin->email);
            \Config::set('settings.admin.name', $admin->firstname . ' ' . $admin->lastname);
            \Config::set('settings.admin.image', $admin->image);
        }
        return $admin;
    }

    public static function getMetadata() {
        \Config::set('settings.title', config('settings.title'));
        \Config::set('settings.metaTitle', config('settings.metaTitle'));
        \Config::set('settings.metaKeywords', config('settings.metaKeywords'));
        \Config::set('settings.metaDescription', config('settings.metaDescription'));
    }

}

Controller::getSettings();
Controller::getAdminSettings();
Controller::getMetadata();

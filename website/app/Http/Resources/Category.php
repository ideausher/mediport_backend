<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\Resource;

class Category extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        if($request->header('language') == 'ar'){
            $returnValue = $this->cat_name_ar;
        }
        elseif($request->header('language') == 'es'){
            $returnValue = $this->cat_name_es;
        }else{
            $returnValue = $this->cat_name;
        }

        return [
            'id' => $this->id,
            'cat_name' => $returnValue,
            'image' => $this->image ? $this->image : '',
        ];
    }
}

<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\Resource;

class Review extends Resource {

    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request) {


        $submittedBy = '';
        $user_id = '';
        $vender_id = '';
        if ($this->user_id == $this->review_submitted_by) {

            $submittedBy = $this->user->firstname . ' ' . $this->user->lastname;
            $user_id = $this->user_id;
            $vender_id = $this->vender_id;
        } else {
            $user_id = $this->user_id;
            $vender_id = $this->vender_id;
            $submittedBy = $this->vender->firstname . ' ' . $this->vender->lastname;
        }

        return [
            'id' => $this->id,
            'userId' => $user_id,
            'venderId' => $vender_id,
            'reviewSubmittedBy' => $submittedBy,
            'rating' => number_format($this->rating, 2),
            'like' => (int) $this->is_like
        ];
    }

}

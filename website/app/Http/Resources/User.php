<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\Resource;
use App\UserAddresses;
use App\Http\Resources\AddressCollection;
use App\Http\Resources\VenderServiceCollection;
use Illuminate\Support\Facades\Auth;

class User extends Resource {

    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    private $token;

    public function __construct($resource, $token = "") {
        parent::__construct($resource);
        $this->token = $token;
    }

    public function toArray($request) {

        return [
            'id' => $this->id,
            'firstname' => $this->firstname ? $this->firstname : '',
            'email' => $this->email ? $this->email : '',
            'phone_number' => $this->phone_number ? $this->phone_number : '',
            'image' => $this->image ? $this->image : '',
            'phone_country_code' => $this->phone_country_code ? $this->phone_country_code : '',
            'blood_group' => $this->blood_group ? $this->blood_group : '',
            'gender' => $this->gender ? $this->gender : '',
            'token' => $this->token,
            'is_notification' => $this->is_notification
        ];

       
    }

}

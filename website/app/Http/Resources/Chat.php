<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\Resource;

class Chat extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        
        return [
            'id' => $this->id,
            'userId' => $this->user_id,
            'body' => $this->body,
            'senderName' => $this->sender->firstname.' '.$this->sender->lastname,
            'image' => $this->sender->image ? url('images/avatars/' . $this->sender->image) : '',
            //'userImage' => $this->user->image ? url('images/avatars/' . $this->user->image) : '',
            //'venderImage' => $this->vender->image ? url('images/avatars/' . $this->vender->image) : '',
            'createdAt' => $this->created_at->format('Y-m-d H:i:s'),
        ];
    }
}

<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class UserProfile extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'firstname' => $this->firstname ? $this->firstname : '',
            'email' => $this->email ? $this->email : '',
            'phone_number' => $this->phone_number,
            'phone_country_code' => $this->phone_country_code ? $this->phone_country_code : '',
            'gender' => $this->gender ? $this->gender : '' ,
            'blood_group' => $this->blood_group ? $this->blood_group : '',
            'is_notification' => $this->is_notification,
            'image' => $this->image ? $this->image : '',

        ];
    }
}
